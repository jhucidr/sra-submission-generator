/*Copyright (c) 2015 Johns Hopkins Center for Inherited Disease Research

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.jhmi.cidr.SRA_Submission.view;

import edu.jhmi.cidr.SRA_Submission.controller.SRAController;
import edu.jhmi.cidr.SRA_Submission.model.models.DefaultPropertiesModel;
import edu.jhmi.cidr.SRA_Submission.model.models.SRASubmissionModel;
import edu.jhmi.cidr.SRA_Submission.view.wizardpages.WizardPageViewController;
import edu.jhmi.cidr.SRA_Submission.view.wizardpages.SRAWizard;
import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.stage.FileChooser;
import javafx.stage.Screen;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import org.controlsfx.dialog.Wizard;

/**
 *
 * @author asanch15
 */
public class main extends Application {

    @Override
    public void start(Stage stage) throws Exception {
        SRASubmissionModel model = new SRASubmissionModel();
        Wizard wizard = new SRAWizard(model);
//        Wizard.WizardPane page1 = getWizardPane(wizard, "wizardpages/ProjectInfoPage.fxml", model);
//        
//        wizard.setFlow(new Wizard.LinearFlow(page1, new Wizard.WizardPane()));

//        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("SRAView.fxml"));
//        Parent root = (Parent) fxmlLoader.load();
//        SRAViewController viewController = (SRAViewController) fxmlLoader.getController();
////        SRAPropertiesModel propertiesModel = new SRAPropertiesModel(new File("defaults.props"));
//        SRAController controller = new SRAController(viewController);
//        Scene scene = new Scene(root);
//
//        viewController.setController(controller);
////        propertiesModel.addObserver(viewController);
////        propertiesModel.init();
//        
//        stage.setTitle("SRA Submission Generator");
//        stage.setScene(scene);
//        stage.setResizable(false);
//        viewController.setStage(stage);
//        stage.show();
//        centerStage(stage);
        model.init();
        stage.setOnCloseRequest((WindowEvent t) -> {
            System.exit(-1);
        });

        wizard.setTitle("SRA Submission Generator");
        showWizard(wizard, model);
    }

    private void showWizard(Wizard wizard, SRASubmissionModel model) {

        wizard.showAndWait().ifPresent(result -> {
            if (result == ButtonType.FINISH) {
                FileChooser fileChooser = new FileChooser();
                File selectedFile;

                fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("tar", "*.tar"));
                selectedFile = fileChooser.showSaveDialog(null);

                try {
                    model.generateSubmission(selectedFile);

                    Alert alert = new Alert(Alert.AlertType.INFORMATION);

                    alert.setTitle("SRA Submission");
                    alert.setHeaderText("SRA Submission");
                    alert.setContentText("SRA Submission Created: " + selectedFile.getAbsolutePath());
                    alert.show();
                } catch (Exception ex) {
                    Alert alert = new Alert(Alert.AlertType.ERROR);

                    alert.setContentText(ex.getMessage());
                    ex.printStackTrace();

                    alert.showAndWait();
                    showWizard(wizard, model);
                }

            }
        });
    }

    //center window on screen, has to be done after it's shown
    private void centerStage(Stage stage) {
        Screen screen = Screen.getPrimary();
        double screenWidth = screen.getVisualBounds().getWidth();
        double screenHeight = screen.getVisualBounds().getHeight();
        stage.setX(screenWidth / 2 - stage.getWidth() / 2);
        stage.setY(screenHeight / 2 - stage.getHeight() / 2);
    }

    /**
     * The main() method is ignored in correctly deployed JavaFX application.
     * main() serves only as fallback in case the application can not be
     * launched through deployment artifacts, e.g., in IDEs with limited FX
     * support. NetBeans ignores main().
     *
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }
}
