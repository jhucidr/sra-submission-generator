/*Copyright (c) 2015 Johns Hopkins Center for Inherited Disease Research

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/

package edu.jhmi.cidr.SRA_Submission.view;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.ProgressIndicator;
import javafx.stage.Stage;

public class ProgressBarViewController {

    private long maxProgress = 1;
    private long currentProgress = 0;
    private Stage stage;

    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private Label infoLabel;

    @FXML
    private Label percentLabel;

    @FXML
    private ProgressBar progressBar;

    @FXML
    private ProgressIndicator progressIndicator;

    public void setMaxProgress(long maxProgress) {
        if (maxProgress < 1) {
            throw new IllegalArgumentException("Max Progress can not be less than 1");
        }

        percentLabel.setText("0%");

        this.maxProgress = maxProgress;
        this.currentProgress = 1;
    }

    public void setCurrentProgress(long currentProgress) {
        double currentProgressValue;

        if (currentProgress > maxProgress) {
            throw new IllegalArgumentException("Current Progress can not be less than max progress");
        }

        this.currentProgress = currentProgress;
        currentProgressValue = currentProgress / (double) maxProgress;

        progressBar.setProgress(currentProgressValue);
        setPercentLabel(currentProgressValue);
    }

    public void setStage(Stage stage) {
        this.stage = stage;
    }

    public void incrementProgress(long amountToIncrementBy) {
        double currentProgressValue;

        currentProgress += amountToIncrementBy;

        if (currentProgress > maxProgress) {
            currentProgress = maxProgress;
        }

        System.out.println("currentProgress = " + currentProgress);
        System.out.println("maxProgress = " + maxProgress);

        currentProgressValue = currentProgress / (double) maxProgress;

        progressBar.setProgress(currentProgressValue);
        setPercentLabel(currentProgressValue);
    }

    public void setStatusLabelText(String text) {
        infoLabel.setText(text);
    }

    private void setPercentLabel(double currentProgressValue) {
        String formattedValue = String.format("%.0f", currentProgressValue * 100);
        System.out.println("formattedValue = " + formattedValue);
        percentLabel.setText(formattedValue + "%");
    }

    public void closeProgressBarWindow() {
        stage.close();
    }

    @FXML
    void initialize() {
        assert infoLabel != null : "fx:id=\"infoLabel\" was not injected: check your FXML file 'ProgressView.fxml'.";
        assert progressBar != null : "fx:id=\"progressBar\" was not injected: check your FXML file 'ProgressView.fxml'.";
    }

}
