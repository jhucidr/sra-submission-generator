/*Copyright (c) 2015 Johns Hopkins Center for Inherited Disease Research

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.jhmi.cidr.SRA_Submission.view.util;

import com.google.common.base.Joiner;
import java.util.List;
import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.beans.property.Property;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.TextArea;
import javafx.util.Duration;

/**
 *
 * @author btibbil1
 */
public class GuiUtils {

    private static final Duration DEFAULT_DURATION = Duration.millis(150);

    public static void animateProperty(Property property, Double finalValue, Duration duration) {
        animateProperty(property, finalValue, duration, null);
    }

    public static void animateProperty(Property property, Double finalValue, Duration duration, Runnable callback) {
        Timeline timeline = new Timeline();
        final Runnable runnable = callback;
        KeyValue kv = new KeyValue(property, finalValue);
        KeyFrame kf = new KeyFrame(duration, kv);

        timeline.setCycleCount(1);
        timeline.getKeyFrames().add(kf);
        if (callback != null) {
            timeline.setOnFinished(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent t) {
                    runnable.run();
                }
            });
        }
        timeline.play();
    }

    public static void fadeIn(Node control) {
        animateProperty(control.opacityProperty(), 1.0, DEFAULT_DURATION);
    }

    public static void fadeIn(Node control, long durationMilliseconds) {
        Duration duration = Duration.millis(durationMilliseconds);
        animateProperty(control.opacityProperty(), 1.0, duration);
    }

    public static void fadeOut(Node control) {
        animateProperty(control.opacityProperty(), 0d, DEFAULT_DURATION);
    }

    public static void fadeOut(Node control, long durationMilliseconds) {
        Duration duration = Duration.millis(durationMilliseconds);
        animateProperty(control.opacityProperty(), 0d, duration);
    }

    public static void fadeOut(Node control, long durationMilliseconds, Runnable callback) {
        Duration duration = Duration.millis(durationMilliseconds);
        animateProperty(control.opacityProperty(), 0d, duration, callback);
    }

    public static void fadeOutTo(Node control, double finalOpacity) {
        animateProperty(control.opacityProperty(), finalOpacity, DEFAULT_DURATION);
    }

    public static void showDialog(String title, String mainMessage, List<String> messages) {
        Alert alert = new Alert(Alert.AlertType.ERROR);
        TextArea textArea = new TextArea(Joiner.on("\n").join(messages));

        textArea.setEditable(false);
        textArea.setWrapText(true);
        textArea.setMaxWidth(Double.MAX_VALUE);
        textArea.setMaxHeight(Double.MAX_VALUE);
        
        alert.setTitle(title);
        alert.setContentText(mainMessage);
        alert.getDialogPane().setExpandableContent(textArea);
//        alert.setContentText(Joiner.on("\n").join(parser.getErrors()));
        alert.show();
    }
}
