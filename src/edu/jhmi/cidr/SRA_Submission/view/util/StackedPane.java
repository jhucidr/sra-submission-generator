/*Copyright (c) 2015 Johns Hopkins Center for Inherited Disease Research

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.jhmi.cidr.SRA_Submission.view.util;

import java.util.HashMap;
import java.util.Map;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.util.Duration;

/**
 *
 * @author asanch15
 */
public class StackedPane extends Pane {

    private static final double BAR_HEIGHT = 32;

    private final ObservableStack<Pane> stack = new ObservableStack<>();
    private final VBox enclosure = new VBox();
    private final HBox sliderBox = new HBox();
    private final Map<PanePosition, AnchorPane> panes = new HashMap<>(3);
    private final Button nextButton = new Button("Next");
    private final Button backButton = new Button("Back");
    private final double stageWidth;

//    private Node backButton = null;
    public StackedPane(double width, double height) {
        super();

        this.stageWidth = width;
        this.setPrefSize(width, height);

        setUpSlider(width);
        setUpEnclosure(width, height);
        setUpStackListener();
        stack.push(new Pane());
    }

//    public StackedPane(Pane basePane) {
//        super();
//        setUpStackListener();
//        stack.push(basePane);
//    }
    private void setUpSlider(double width) {
        sliderBox.setPrefWidth(width * 3);

        for (PanePosition panePosition : PanePosition.values()) {
            AnchorPane pane = new AnchorPane();

            pane.setPrefWidth(width);

            panes.put(panePosition, pane);
            sliderBox.getChildren().add(pane);
        }
    }

    private void setUpEnclosure(double width, double height) {
        enclosure.setPrefSize(width, height);

        enclosure.getChildren().addAll(getTopBar(), sliderBox, getButtonBar());
        
        HBox.setHgrow(sliderBox, Priority.ALWAYS);
        
        this.getChildren().add(enclosure);
    }

    private Pane getTopBar() {
        HBox bar = getBar();

        bar.setAlignment(Pos.CENTER_LEFT);
        bar.getChildren().add(backButton);
        
        return bar;
    }

    private Pane getButtonBar() {
        HBox bar = getBar();
        
        bar.setAlignment(Pos.CENTER_RIGHT);
        bar.getChildren().add(nextButton);
        
        return bar;
    }

    private HBox getBar() {
        HBox bar = new HBox();

        bar.setPrefSize(stageWidth, BAR_HEIGHT);

        return bar;
    }

    public void pushPane(Pane pane) {
        stack.push(pane);

        if (stack.size() > 1) {
            backButton.setVisible(true);
        }
    }

    public void popPane() {
        if (stack.size() > 0) {
            stack.pop();

            if (stack.size() == 1) {
                backButton.setVisible(false);
            }
        }
    }

    public int getNumPanes() {
        return stack.size();
    }

    private void setUpStackListener() {
        stack.addListener(new StackChangeListener<Pane>() {

            @Override
            public void onPush(Pane value) {
                showPane(value);
            }

            @Override
            public void onPop(Pane value) {
                removePane(value);
            }
        });
    }

    private void setPane(PanePosition panePosition, Pane pane) {
        AnchorPane anchorPane = panes.get(panePosition);

        anchorPane.getChildren().clear();
        anchorPane.getChildren().add(pane);

        AnchorPane.setTopAnchor(pane, 0.0);
        AnchorPane.setRightAnchor(pane, 0.0);
        AnchorPane.setBottomAnchor(pane, 0.0);
        AnchorPane.setLeftAnchor(pane, 0.0);
    }

    private void showPane(Pane pane) {
        final Pane prevPane = stack.peek(1);

        if (prevPane != null) {
            final Pane animationPane = panes.get(PanePosition.LEFT);

            setPane(PanePosition.RIGHT, pane);

            GuiUtils.animateProperty(animationPane.prefWidthProperty(),
                    0.0, Duration.millis(150));
        } else {
            setPane(PanePosition.CENTER, pane);
        }
    }

    private void removePane(final Pane pane) {
        Pane animationPane = panes.get(PanePosition.LEFT);

        GuiUtils.animateProperty(animationPane.prefWidthProperty(), stageWidth, Duration.millis(150));
    }

    private enum PanePosition {

        LEFT, CENTER, RIGHT;
    }

}
