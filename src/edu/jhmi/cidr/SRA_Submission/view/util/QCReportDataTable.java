/*Copyright (c) 2015 Johns Hopkins Center for Inherited Disease Research

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.jhmi.cidr.SRA_Submission.view.util;

import edu.jhmi.cidr.SRA_Submission.util.parsers.QCReportParser;
import edu.jhmi.cidr.lib.conveniencestore.guiutils.jfxtable.JFXTableColumn;
import edu.jhmi.cidr.lib.conveniencestore.guiutils.jfxtable.JFXTableView;
import edu.jhmi.cidr.lib.conveniencestore.guiutils.jfxtable.cells.JFXTableCellType;
import java.util.Arrays;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;

/**
 *
 * @author Alice Sanchez
 */
public class QCReportDataTable extends JFXTableView<QCReportParser.QCReportRecord> {

    public QCReportDataTable(TableView<QCReportParser.QCReportRecord> tableView) {
        super(tableView, Arrays.asList(Column.values()));
    }

    private enum Column implements JFXTableColumn<QCReportParser.QCReportRecord> {

        LOCAL_ID("Local ID", "localID"),
        EXPERIMENT_NAME("Experiment Name", "experimentName"),
        PROJECT("Project", "project"),
        RUN_NAME("Run Group ID", "runName"),
        LIBRARY_NAME("Library Name", "libraryName"), 
        LIBRARY_PREP_PROTOCOL("Library Prep Protocol", "libraryPrepProtocol"), 
        LIBRARY_PREP_DESC("Library Prep Description", "libraryPrepDescription"),
        READ_1_LENGTH("Mean Read Length R1", "readOneLength"),
        READ_2_LENGTH("Mean Read Length R2", "readTwoLength"),
        INSERT_SIZE("Mean Insert Size", "insertSize"),
        INSERT_SDEV("Standard Deviation", "insertSDev"),
        ; 

        private final String columnName;
        private final String fieldName;

        private Column(String columnName, String fieldName) {
            this.columnName = columnName;
            this.fieldName = fieldName;
        }

        @Override
        public String getColumnName() {
            return columnName;
        }

        @Override
        public String getFieldName() {
            return fieldName;
        }

        @Override
        public JFXTableCellType getTableCellType() {
            return JFXTableCellType.STRING;
        }

        @Override
        public TableColumn<QCReportParser.QCReportRecord, ?> getTableColumn() {
            return JFXTableCellType.STRING.getTableColumn(columnName, fieldName);
        }

    }
}
