/*Copyright (c) 2015 Johns Hopkins Center for Inherited Disease Research

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.jhmi.cidr.SRA_Submission.view.util;

import java.util.ArrayList;
import java.util.List;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Rectangle;
import javafx.stage.Stage;

/**
 *
 * @author btibbil1
 */
public class GuiDialog {

    private final String BUTTON_STYLE;
    private final String LABEL_STYLE;
    private static final int PADDING = 30;
    private final long FADE_DURATION = 300;
    private final Stage stage;
    private final Pane dialogPane = new Pane();
    private final Rectangle background = new Rectangle();
    private final VBox dialogBox = new VBox(); // holds text and buttonBox
    private final HBox buttonBox = new HBox();
    private final Button okButton = new Button("OK");
    private final Button cancelButton = new Button("Cancel");

    public GuiDialog(Stage stage) {
        this.stage = stage;

        BUTTON_STYLE = "-fx-text-fill: white;"
                + "-fx-font-weight: bold;"
                + "-fx-border-color: white;"
                + "-fx-border-style: solid;"
                + "-fx-border-width: 2px;"
                + "-fx-background-color: rgba(0,0,0,0);"
                + "-fx-cursor: hand;";
        LABEL_STYLE = "-fx-text-fill: white;"
                + "-fx-font-weight: bold;";

        dialogPane.setStyle("-fx-background-color: rgba(173, 173, 173, 0.75);");
        //add rounded corners
        background.setStyle("-fx-arc-width: 25px;"
                + "-fx-arc-height: 25px;"
                + "-fx-effect: dropshadow(three-pass-box, #0066FF, 15, 0.01, 0, 0)");
        background.setFill(Paint.valueOf("#0066FF")); //cannot be set with setstyle
        background.setOpacity(0.75);
        okButton.setStyle(BUTTON_STYLE);
        cancelButton.setStyle(BUTTON_STYLE);

        dialogPane.getChildren().add(background);
        dialogPane.getChildren().add(dialogBox);
        
        setupCloseDialogListener();
    }
    
    private void closeDialog(){
        GuiUtils.fadeOut(dialogPane, FADE_DURATION, new Runnable() {
            @Override
            public void run() {
                dialogPane.setVisible(false);
            }
        });
    }

    private void setupCloseDialogListener(){
        EventHandler<ActionEvent> listener = new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent t) {
                closeDialog();
            }
        };
        
        okButton.addEventHandler(ActionEvent.ACTION, listener);
        cancelButton.addEventHandler(ActionEvent.ACTION, listener);
    }
    
    public void setCallBackOnResponse(Response response, Runnable callBack) {
        final Runnable callBackRunnable = callBack;

        EventHandler<ActionEvent> listener = new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                Object source = e.getSource();

                if (source instanceof Button) {
                    callBackRunnable.run();
                }

            }
        };
        
        switch (response) {
            case OK:
                okButton.addEventHandler(ActionEvent.ACTION, listener);
                break;
            case CANCEL:
                cancelButton.addEventHandler(ActionEvent.ACTION, listener);
                break;
        }
    }

    public void showDialog(List<String> dialogText, GuiDialogButton buttonChoices) {
        Pane root = (Pane) stage.getScene().getRoot();
        double centerX = root.getWidth() / 2;
        double centerY = root.getHeight() / 2;
        
        refreshDialogBox(dialogText);
        refreshButtonBox(buttonChoices);
        
        dialogPane.setPrefSize(stage.getWidth(), stage.getHeight());
        root.getChildren().add(dialogPane);
        stage.sizeToScene();

        background.setWidth(dialogBox.getWidth() + PADDING);
        background.setHeight(dialogBox.getHeight() + PADDING);

        centerGuiObjectAt(centerX, centerY, dialogBox);
        centerGuiObjectAt(centerX, centerY, background);

        showDialogPane();
    }

    private void refreshDialogBox(List<String> dialogText) {
        Pane vSeparator = new Pane();
        vSeparator.setPrefSize(0, PADDING / 2);
        dialogBox.getChildren().clear();
        dialogBox.getChildren().addAll(createLabels(dialogText));
        dialogBox.getChildren().add(vSeparator);
        dialogBox.getChildren().add(buttonBox);
    }
    
    private void refreshButtonBox(GuiDialogButton buttonChoices) {
        Pane leftSeparator = new Pane();
        Pane rightSeparator = new Pane();
        
        buttonBox.getChildren().clear();
        buttonBox.getChildren().add(leftSeparator);
        buttonBox.getChildren().addAll(createButtons(buttonChoices));
        buttonBox.getChildren().add(rightSeparator);
        
        HBox.setHgrow(leftSeparator, Priority.SOMETIMES);
        HBox.setHgrow(rightSeparator, Priority.SOMETIMES);
    }
    
    private void showDialogPane() {
        dialogPane.setOpacity(0);
        dialogPane.setVisible(true);
        GuiUtils.fadeIn(dialogPane, FADE_DURATION);
    }
    
    private List<Node> createButtons(GuiDialogButton buttonChoices) {
        List<Node> nodes = new ArrayList<>();

        nodes.add(okButton);

        if (buttonChoices == GuiDialogButton.OK_CANCEL) {
            Pane hSeparator = new Pane();
            hSeparator.setPrefSize(PADDING / 2, 0);
            nodes.add(hSeparator);
            nodes.add(cancelButton);
        }

        return nodes;
    }

    private List<Label> createLabels(List<String> dialogText) {
        List<Label> labels = new ArrayList<>();

        for (String text : dialogText) {
            Label label = new Label(text);
            label.setStyle(LABEL_STYLE);
            label.setMaxWidth(stage.getWidth() - PADDING * 4);
            label.wrapTextProperty().setValue(true);
            labels.add(label);
        }

        return labels;
    }

    private void centerGuiObjectAt(double x, double y, Node node) {
        node.setLayoutX(x - node.getLayoutBounds().getWidth() / 2);
        node.setLayoutY(y - node.getLayoutBounds().getHeight() / 2);
    }

    public enum GuiDialogButton {

        OK, OK_CANCEL;
    }

    public enum Response {

        OK, CANCEL;
    }
}
