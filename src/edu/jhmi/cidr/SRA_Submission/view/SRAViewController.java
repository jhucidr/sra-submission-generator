/*Copyright (c) 2015 Johns Hopkins Center for Inherited Disease Research

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/

package edu.jhmi.cidr.SRA_Submission.view;

import com.google.common.io.Files;
import edu.jhmi.cidr.SRA_Submission.view.util.GuiDialog;
import edu.jhmi.cidr.SRA_Submission.controller.SRAController;
import edu.jhmi.cidr.SRA_Submission.model.ModelMessage;
import edu.jhmi.cidr.SRA_Submission.model.models.DefaultPropertiesModel;
import edu.jhmi.cidr.SRA_Submission.model.models.SRASubmissionModel;
import edu.jhmi.cidr.SRA_Submission.model.models.elements.SubmissionElement;
import edu.jhmi.cidr.SRA_Submission.objects.Action;
import edu.jhmi.cidr.SRA_Submission.objects.Contact;
import edu.jhmi.cidr.SRA_Submission.objects.LibraryProtocol;
import edu.jhmi.cidr.SRA_Submission.objects.SRAFreeFormData;
import edu.jhmi.cidr.SRA_Submission.view.util.FileListParser;
import edu.jhmi.cidr.lib.conveniencestore.guiutils.GuiLoadScreen;
import java.io.File;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Observable;
import java.util.Observer;
import java.util.ResourceBundle;
import javafx.application.Platform;
import javafx.beans.binding.Bindings;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.Control;
import javafx.scene.control.ListView;
import javafx.scene.control.MenuItem;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.stage.DirectoryChooser;
import javafx.stage.DirectoryChooserBuilder;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;
import javafx.stage.FileChooserBuilder;
import javafx.stage.Stage;

public class SRAViewController implements Observer {

    private final DefaultPropertiesModel propertiesModel = new DefaultPropertiesModel(new File("defaults.props"));
    private final SRASubmissionModel submissionModel = new SRASubmissionModel();
    private final SRAFreeFormData freeFormData = new SRAFreeFormData();
    private final List<String> defaultSampleIDColumns = Arrays.asList("Local_Id", "SM_tag");
    private final List<String> defaultRunGroupIDColumns = Arrays.asList("RG_ID", "RG_ID_PU");
//    final StringProperty currentLibProtocol = new SimpleStringProperty("");

    private GuiLoadScreen loadScreen = null;
    private ContextMenu contactsTableMenu = null;
    private ContextMenu actionTableMenu = null;
    private Control firstAttributeControl = null;
    private Control secondAttributeControl = null;
    private Stage stage = null;
//    private SRAController controller = null;
//    private ProgressBarViewController progressBarViewController = null;

    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private TableView<Action> actionsTable;

    @FXML
    private TextField centerNameField;

    @FXML
    private TableView<Contact> contactsTable;

    @FXML
    private TableColumn<Contact, String> contactInformOnErrorColumn;

    @FXML
    private TableColumn<Contact, String> contactInformOnStatusColumn;

    @FXML
    private TableColumn<Contact, String> contactNameColumn;

    @FXML
    private ComboBox<LibraryProtocol> designDescriptionComboBox;

    @FXML
    private ComboBox<String> libraryLayoutComboBox;

    @FXML
    private TextArea libraryProtocolTextArea;

    @FXML
    private ComboBox<String> librarySelectionComboBox;

    @FXML
    private ComboBox<String> librarySourceComboBox;

    @FXML
    private ComboBox<String> libraryStrategyComboBox;

    @FXML
    private TextField projectAliasField;

    @FXML
    private TextField projectTitleField;

    @FXML
    private TextField outputNameField;

    @FXML
    private TextField referenceGenomeField;

    @FXML
    private TextField contactsNameField;

    @FXML
    private TextField informOnErrorField;

    @FXML
    private TextField informOnStatusField;

    @FXML
    private ComboBox<Action.ActionType> actionComboBox;

    @FXML
    private TableColumn<Action, String> actionColumn;

    @FXML
    private TableColumn<Action, String> firstAttributeColumn;

    @FXML
    private TableColumn<Action, String> secondAttributeColumn;

    @FXML
    private TextField outputFolderField;

    @FXML
    private HBox actionHBox;

    @FXML
    private Button addContactButton;

    @FXML
    private TextField checksumFileField;

    @FXML
    private TextField bamFolderField;

    @FXML
    private ListView<File> bamFolderListView;

    @FXML
    private ContextMenu bamFolderListViewMenu;

    @FXML
    private TextField qcReportField;

    @FXML
    private ListView<File> qcReportsListView;

    @FXML
    private ContextMenu qcReportListViewMenu;

    @FXML
    private Button submitButton;

    @FXML
    private ComboBox<String> runGroupIDColumnComboBox;

    @FXML
    private ComboBox<String> sampleIDColumnComboBox;

    @FXML
    void initialize() {
        bindUIControls();
        setUpContactsTable();
        setUpActionTable();
        setUpDefaultActions();
        bamFolderListView.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
        bamFolderListView.setOnMouseClicked(handleListViewMenu(bamFolderListView, bamFolderListViewMenu));
        qcReportsListView.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
        qcReportsListView.setOnMouseClicked(handleListViewMenu(qcReportsListView, qcReportListViewMenu));

        try {
            propertiesModel.init();
            populateUIFromDefaults(propertiesModel.getDefaultProperties());
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void populateUIFromDefaults(DefaultPropertiesModel.SRADefaultProperties defaultProperties) {
        freeFormData.getCenterName().set(defaultProperties.getCenterName());
        freeFormData.getLibraryLayout().set(defaultProperties.getLibraryLayout());
        freeFormData.getLibrarySelection().set(defaultProperties.getLibrarySelection());
        freeFormData.getLibrarySource().set(defaultProperties.getLibrarySource());
        freeFormData.getLibraryStrategy().set(defaultProperties.getLibraryStrategy());
        freeFormData.getReferencGenome().set(defaultProperties.getRefGenome());

        designDescriptionComboBox.getItems().setAll(defaultProperties.getLibraryProtocols());
        sampleIDColumnComboBox.getItems().setAll(defaultSampleIDColumns);
        runGroupIDColumnComboBox.getItems().setAll(defaultRunGroupIDColumns);
    }

    @FXML
    void onAddBAMFolderList(ActionEvent event) {
        FileChooser fileChooser = FileChooserBuilder.create()
                .title("Choose BAM Folder List")
                .extensionFilters(new ExtensionFilter("csv", "*.csv"))
                .build();
        File bamList = fileChooser.showOpenDialog(null);

        if (bamList != null) {
            FileListParser fileListParser = new FileListParser(bamList);

            try {
                for (File file : fileListParser.parse()) {
                    if (file.isDirectory() && bamFolderListView.getItems().contains(file) == false) {
                        bamFolderListView.getItems().add(file);
                    }
                }
            } catch (Exception ex) {
                ex.printStackTrace();
                new GuiDialog(stage).showDialog(Arrays.asList("Error reading BAM Folder List", ex.getMessage()),
                        GuiDialog.GuiDialogButton.OK);
            }
        }
    }

    @FXML
    void onAddQCReportList(ActionEvent event) {
        FileChooser fileChooser = FileChooserBuilder.create()
                .title("Choose QC Report File List")
                .extensionFilters(new ExtensionFilter("csv", "*.csv"))
                .build();
        File bamList = fileChooser.showOpenDialog(null);

        if (bamList != null) {
            FileListParser fileListParser = new FileListParser(bamList);

            try {
                for (File file : fileListParser.parse()) {
                    if (Files.getFileExtension(file.getName()).equals("csv")
                            && qcReportsListView.getItems().contains(file) == false) {
                        qcReportsListView.getItems().add(file);
                    }
                }
            } catch (Exception ex) {
                ex.printStackTrace();
                new GuiDialog(stage).showDialog(Arrays.asList("Error reading QC Report File List", ex.getMessage()),
                        GuiDialog.GuiDialogButton.OK);
            }
        }
    }

    @FXML
    void onBrowseForChecksumFile(ActionEvent event) {
        FileChooser chooser = FileChooserBuilder.create()
                .title("Select Checksum File")
                .extensionFilters(new ExtensionFilter("MD5 file", "*.md5"))
                .build();
        File file = chooser.showOpenDialog(null);

        if (file != null) {
            checksumFileField.setText(file.getAbsolutePath());
        }
    }

    @FXML
    void submitButtonPressed(ActionEvent event) {
        List<String> errors;

        errors = freeFormData.validate();

        if (errors.isEmpty()) {
            handleSRAGeneration(false);
        } else {
            (new GuiDialog(stage)).showDialog(errors, GuiDialog.GuiDialogButton.OK);
        }
    }

    @FXML
    void bamFolderBrowseButtonClicked(ActionEvent event) {
        DirectoryChooser dirChooser = DirectoryChooserBuilder.create()
                .title("BAM Folder")
                .build();
        File dir = dirChooser.showDialog(null);

        if (dir != null) {
            bamFolderField.setText(dir.getAbsolutePath());
        }
    }

    @FXML
    void addToBamFolderList(ActionEvent event) {
        String dirPath = bamFolderField.getText();

        if (dirPath.isEmpty() == false) {
            File dir = new File(dirPath);

            if (dir.isDirectory()) {
                bamFolderListView.getItems().add(dir);
                bamFolderField.setText("");
            }
        }
    }

    @FXML
    void onRemoveBamDir(ActionEvent event) {
        List<File> selectedFiles = bamFolderListView.getSelectionModel().getSelectedItems();
        
        if(selectedFiles.isEmpty() == false){
            List<File> items = new ArrayList<>(bamFolderListView.getItems());
            
            items.removeAll(selectedFiles);
            bamFolderListView.getItems().setAll(items);
        }
    }

    @FXML
    void qcReportBrowseButtonClicked(ActionEvent event) {
        ExtensionFilter extensionFilter = new ExtensionFilter("CSV File", "*.csv");
        FileChooser fileChooser = FileChooserBuilder.create()
                .title("QC Report")
                .extensionFilters(extensionFilter)
                .build();
        File qcReportFile = fileChooser.showOpenDialog(null);

        if (qcReportFile != null) {
            qcReportField.setText(qcReportFile.getAbsolutePath());
        }
    }

    @FXML
    void addToQCReportList(ActionEvent event) {
        String qcReportFilePath = qcReportField.getText();

        if (qcReportFilePath.isEmpty() == false) {
            File qcReportFile = new File(qcReportFilePath);

            if (qcReportFile.isFile() && qcReportFile.getName().endsWith(".csv")) {
                qcReportsListView.getItems().add(qcReportFile);
                qcReportField.setText("");
            }
        }
    }

    @FXML
    void onRemoveQCReport(ActionEvent event) {
        List<File> selectedItems = qcReportsListView.getSelectionModel().getSelectedItems();

        if (selectedItems.isEmpty() == false) {
            List<File> items = new ArrayList<>(qcReportsListView.getItems());
            
            items.removeAll(selectedItems);
            qcReportsListView.getItems().setAll(items);
        }
    }

    @FXML
    void outputFolderBrowseButtonClicked(ActionEvent event) {
        DirectoryChooser dirChooser = DirectoryChooserBuilder.create()
                .title("Output Folder")
                .build();
        File dir = dirChooser.showDialog(null);

        if (dir != null) {
            freeFormData.getOutputFolderPath().set(dir.getPath());
        }
    }

    @FXML
    void addToActionTableButtonClicked(ActionEvent event) {
        Action.ActionType actionType = actionComboBox.getSelectionModel().getSelectedItem();
        List<String> errors;
        Action action;

        if (actionType != null) {
            action = new Action(actionType);
            String firstAttribute = getControlText(firstAttributeControl);
            String secondAttribute = getControlText(secondAttributeControl);

            if (firstAttribute != null) {
                action.firstAttributeProperty().set(firstAttribute);

                if (secondAttribute != null) {
                    action.secondAttributeProperty().set(secondAttribute);
                }
            }

            errors = action.validate();

            if (errors.isEmpty()) {
                if (!actionsTable.getItems().contains(action)) {
                    actionsTable.getItems().add(action);

                    Platform.runLater(new Runnable() {

                        @Override
                        public void run() {
                            actionComboBox.getSelectionModel().clearSelection();
                        }
                    });
                } else {
                    new GuiDialog(stage).showDialog(Arrays.asList("Duplicate Entry"), GuiDialog.GuiDialogButton.OK);
                }
            } else {
                new GuiDialog(stage).showDialog(errors, GuiDialog.GuiDialogButton.OK);
            }
        }
    }

    @FXML
    void onActionSelected(ActionEvent event) {
        Action.ActionType actionType = actionComboBox.getSelectionModel().getSelectedItem();

        actionHBox.getChildren().clear();

        actionHBox.getChildren().add(actionComboBox);

        if (actionType != null) {
            firstAttributeControl = getActionAttributeGUIElement(actionType.getFirstAttributeType());
            secondAttributeControl = getActionAttributeGUIElement(actionType.getSecondAttributeType());
            Button addButton = new Button("Add Action");

            if (firstAttributeControl != null) {
                actionHBox.getChildren().add(firstAttributeControl);

                if (secondAttributeControl != null) {
                    actionHBox.getChildren().add(secondAttributeControl);
                    HBox.setHgrow(firstAttributeControl, Priority.SOMETIMES);
                    HBox.setHgrow(secondAttributeControl, Priority.SOMETIMES);
                } else {
                    HBox.setHgrow(firstAttributeControl, Priority.ALWAYS);
                }
            }

            addButton.setOnAction(new EventHandler<ActionEvent>() {

                @Override
                public void handle(ActionEvent event) {
                    addToActionTableButtonClicked(event);
                }
            });

            actionHBox.getChildren().add(addButton);
        }
    }

    @FXML
    void onDesignDescriptionSelected(ActionEvent event) {
        LibraryProtocol protocol = designDescriptionComboBox.getSelectionModel().getSelectedItem();

        if (protocol != null) {
            libraryProtocolTextArea.setText(protocol.getDescription());
        }
    }

    @FXML
    void addToContactsTableButtonClicked(ActionEvent event) {
        Contact contact = new Contact(contactsNameField.getText(), informOnStatusField.getText(), informOnErrorField.getText());
        List<String> errors;
//
//        contact.nameProperty().set(contactsNameField.getText());
//        contact.informOnStatusProperty().set(informOnStatusField.getText());
//
//        if (informOnErrorField.getText().isEmpty()) {
//            contact.informOnErrorProperty().set(informOnStatusField.getText());
//        } else {
//            contact.informOnErrorProperty().set(informOnErrorField.getText());
//        }

        errors = contact.validate();

        if (errors.isEmpty()) {
            if (!contactsTable.getItems().contains(contact)) {
                contactsTable.getItems().add(contact);

                Platform.runLater(new Runnable() {

                    @Override
                    public void run() {
                        contactsNameField.setText("");
                        informOnStatusField.setText("");
                        informOnErrorField.setText("");
                    }
                });

            } else {
                new GuiDialog(stage).showDialog(Arrays.asList("Duplicate Entry"), GuiDialog.GuiDialogButton.OK);
            }
        } else {
            new GuiDialog(stage).showDialog(errors, GuiDialog.GuiDialogButton.OK);
        }
    }

    private void setUpDefaultActions() {
        Action experiment = new Action(Action.ActionType.SOURCE, false);
        Action protect = new Action(Action.ActionType.PROTECT, false);
        Action run = new Action(Action.ActionType.SOURCE, false);

        experiment.firstAttributeProperty().set(SubmissionElement.SchemaType.EXPERIMENT.getName());
        run.firstAttributeProperty().set(SubmissionElement.SchemaType.RUN.getName());

        freeFormData.getActions().addAll(experiment, run, protect);
    }

    private String getControlText(Control control) {
        String text;

        if (control == null) {
            text = null;
        } else if (control instanceof TextField) {
            text = ((TextField) control).getText();
        } else if (control instanceof ComboBox) {
            Object selectedObj = ((ComboBox) control).getSelectionModel().getSelectedItem();

            if (selectedObj != null) {
                text = selectedObj.toString();
            } else {
                text = null;
            }
        } else {
            text = null;
        }

        return text;
    }

    private Control getActionAttributeGUIElement(Action.ActionAttributeType attributeType) {
        Control control;

        if (!attributeType.getChoices().isEmpty()) {
            ComboBox<String> attributeComboBox = new ComboBox<>();

            attributeComboBox.getItems().addAll(attributeType.getChoices());
            attributeComboBox.setPromptText(attributeType.getPromptText());

            control = attributeComboBox;
        } else if (attributeType != Action.ActionAttributeType.NONE) {
            TextField attributeTextField = new TextField();

            attributeTextField.setPromptText(attributeType.getPromptText());

            control = attributeTextField;
        } else {
            control = null;
        }

        return control;
    }

    private void bindUIControls() {
        freeFormData.getAlias().bindBidirectional(projectAliasField.textProperty());
        freeFormData.getCenterName().bindBidirectional(centerNameField.textProperty());
        freeFormData.getDesignDescription().bindBidirectional(designDescriptionComboBox.valueProperty(), designDescriptionComboBox.getConverter());
        freeFormData.getLibraryProtocol().bindBidirectional(libraryProtocolTextArea.textProperty());
        freeFormData.getLibraryLayout().bindBidirectional(libraryLayoutComboBox.valueProperty());
        freeFormData.getLibrarySelection().bindBidirectional(librarySelectionComboBox.valueProperty());
        freeFormData.getLibrarySource().bindBidirectional(librarySourceComboBox.valueProperty());
        freeFormData.getLibraryStrategy().bindBidirectional(libraryStrategyComboBox.valueProperty());
        freeFormData.getReferencGenome().bindBidirectional(referenceGenomeField.textProperty());
        freeFormData.getTitle().bindBidirectional(projectTitleField.textProperty());
        freeFormData.getOutputFolderPath().bindBidirectional(outputFolderField.textProperty());
        freeFormData.getOutputFileName().bindBidirectional(outputNameField.textProperty());
        freeFormData.getChecksumFilePath().bindBidirectional(checksumFileField.textProperty());
        freeFormData.getSampleIDColumn().bindBidirectional(sampleIDColumnComboBox.valueProperty());
        freeFormData.getRunGroupIDColumn().bindBidirectional(runGroupIDColumnComboBox.valueProperty());

        Bindings.bindContentBidirectional(freeFormData.getBamFolders(), bamFolderListView.getItems());
        Bindings.bindContentBidirectional(freeFormData.getQcReportFiles(), qcReportsListView.getItems());
    }

    private ContextMenu createRemoveItemContextMenu(final TableView table) {
        return createRemoveItemContextMenu(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
                int selectedIndex = table.getSelectionModel().getSelectedIndex();

                if (selectedIndex != -1) {
                    table.getItems().remove(selectedIndex);
                }
            }
        });
    }

    private ContextMenu createRemoveItemContextMenu(EventHandler<ActionEvent> handleItemRemoval) {
        ContextMenu menu = new ContextMenu();
        MenuItem removeContact = new MenuItem("Remove contact");

        menu.setAutoHide(true);

        removeContact.setOnAction(handleItemRemoval);

        menu.getItems().add(removeContact);

        return menu;
    }

    private void handleTableMenu(MouseEvent event, TableView table, ContextMenu menu) {
        if (event.getButton() == MouseButton.SECONDARY) {
            boolean itemSelected = (table.getSelectionModel().getSelectedItem() != null);

            if (itemSelected) {
                menu.show(table, event.getScreenX(), event.getScreenY());
            }

        } else {
            menu.hide();
        }
    }

    private EventHandler<MouseEvent> handleListViewMenu(final ListView listView, final ContextMenu menu) {
        return new EventHandler<MouseEvent>() {

            @Override
            public void handle(MouseEvent event) {
                if (event.getButton() == MouseButton.SECONDARY) {
                    boolean itemSelected = (listView.getSelectionModel().getSelectedItem() != null);

                    if (itemSelected) {
                        menu.show(listView, event.getScreenX(), event.getScreenY());
                    }

                } else {
                    menu.hide();
                }
            }
        };
    }

    private void setUpContactsTable() {
        contactsTableMenu = createRemoveItemContextMenu(contactsTable);

        contactNameColumn.setCellValueFactory(new PropertyValueFactory<Contact, String>("name"));
        contactInformOnStatusColumn.setCellValueFactory(new PropertyValueFactory<Contact, String>("informOnStatus"));
        contactInformOnErrorColumn.setCellValueFactory(new PropertyValueFactory<Contact, String>("informOnError"));

        contactsTable.setOnMouseClicked(new EventHandler<MouseEvent>() {

            @Override
            public void handle(MouseEvent event) {
                handleTableMenu(event, contactsTable, contactsTableMenu);
            }
        });

        Bindings.bindContentBidirectional(freeFormData.getContacts(), contactsTable.getItems());
    }

    private void setUpActionTable() {
        actionTableMenu = createRemoveItemContextMenu(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent t) {
                int selectedIndex = actionsTable.getSelectionModel().getSelectedIndex();

                if (selectedIndex != -1) {
                    Action selectedAction = actionsTable.getItems().get(selectedIndex);

                    if (selectedAction.isRemovable()) {
                        actionsTable.getItems().remove(selectedIndex);
                    } else {
                        GuiDialog dialog = new GuiDialog(stage);

                        dialog.showDialog(Arrays.asList("Can not remove a default action"), GuiDialog.GuiDialogButton.OK);
                    }
                }
            }
        });

        actionComboBox.getItems().addAll(Action.ActionType.values());

        actionColumn.setCellValueFactory(new PropertyValueFactory<Action, String>("typeName"));
        firstAttributeColumn.setCellValueFactory(new PropertyValueFactory<Action, String>("firstAttribute"));
        secondAttributeColumn.setCellValueFactory(new PropertyValueFactory<Action, String>("secondAttribute"));

        actionsTable.setOnMouseClicked(new EventHandler<MouseEvent>() {

            @Override
            public void handle(MouseEvent event) {
                handleTableMenu(event, actionsTable, actionTableMenu);
            }
        });

        Bindings.bindContentBidirectional(freeFormData.getActions(), actionsTable.getItems());
    }

    public void setStage(Stage stage) {
        this.stage = stage;
        this.loadScreen = new GuiLoadScreen(stage);
    }

    public void setController(SRAController controller) {
//        this.controller = controller;
    }

    private void handleSRAGeneration(final boolean runWithWarnings) {
        loadScreen.show("Processing Submisison");
        new Thread(new Runnable() {

            @Override
            public void run() {
                try {
                    SRASubmissionModel.SubmissionReport report;

                    submissionModel.setQCReportFiles(freeFormData.getQcReportFiles());
                    submissionModel.setBAMFolders(freeFormData.getBamFolders());

                    if (freeFormData.getChecksumFilePath().get().isEmpty() == false) {
                        submissionModel.setChecksumFile(new File(freeFormData.getChecksumFilePath().get()));
                    }

                    report = submissionModel.generateSubmission(freeFormData, runWithWarnings);

                    if (report.isSuccessful() == false) {
                        if (report.getErrors().isEmpty() == false) {
                            reportMessage(report.getErrors());
                        } else if (report.getWarnings().isEmpty() == false) {
                            final List<String> warnings = report.getWarnings();

                            Platform.runLater(new Runnable() {

                                @Override
                                public void run() {
                                    GuiDialog dialog = new GuiDialog(stage);
                                    List<String> dialogText = new ArrayList<>();

                                    dialogText.add("Warning");
                                    dialogText.addAll(warnings);
                                    dialogText.add("");
                                    dialogText.add("Press OK to continue processing despite the warnings");

                                    dialog.setCallBackOnResponse(GuiDialog.Response.OK, new Runnable() {

                                        @Override
                                        public void run() {
                                            handleSRAGeneration(true);
                                        }
                                    });

                                    dialog.showDialog(dialogText, GuiDialog.GuiDialogButton.OK_CANCEL);
                                }
                            });
                        }
                    } else {
                        reportMessage(Arrays.asList("Report Successfully Generated"));
                    }

                } catch (Exception ex) {
                    ex.printStackTrace();
                    reportMessage(Arrays.asList("Error generating submission"));
                } finally {
                    Platform.runLater(new Runnable() {

                        @Override
                        public void run() {
                            loadScreen.hide();
                        }
                    });
                }
            }
        }).start();
    }

    private void reportMessage(final List<String> messages) {
        Platform.runLater(new Runnable() {

            @Override
            public void run() {
                (new GuiDialog(stage)).showDialog(messages, GuiDialog.GuiDialogButton.OK);
//                disableGuiWithRunLater(true);
            }
        });
    }

    @Override
    public void update(Observable oberservable, Object update) {

        if (update instanceof ModelMessage) {
            final ModelMessage msg = (ModelMessage) update;

            Platform.runLater(new Runnable() {

                @Override
                public void run() {
                    switch (msg) {
                        case FINSHED_PROCESSING_DEFAULT_PROPERTIES:
                            break;
                    }
                }
            });
        }
    }
}
