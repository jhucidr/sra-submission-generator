/*Copyright (c) 2015 Johns Hopkins Center for Inherited Disease Research

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.jhmi.cidr.SRA_Submission.view.wizardpages;

import edu.jhmi.cidr.SRA_Submission.model.objects.BAMData;
import edu.jhmi.cidr.SRA_Submission.objects.Contact;
import edu.jhmi.cidr.SRA_Submission.objects.SRAFreeFormData;
import edu.jhmi.cidr.SRA_Submission.util.parsers.BAMHeaderParser;
import edu.jhmi.cidr.SRA_Submission.util.parsers.QCReportParser;
import static edu.jhmi.cidr.lib.Constants.NEW_LINE;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.text.Text;
import javafx.scene.text.TextFlow;
import org.controlsfx.dialog.Wizard;

/**
 * FXML Controller class
 *
 * @author Alice Sanchez
 */
public class ReviewPageController extends WizardPageViewController implements Initializable {

    @FXML
    private TextFlow reviewTextFlow;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
    }

    @Override
    public void onEnteringPage(Wizard wizard) {
        setReviewText();
    }

    @Override
    public SRAWizard.WizardPageInfo getNextPage() {
        return null;
    }

    private void setReviewText() {
        SRAFreeFormData sraFreeFormData = getModel().getSraFreeFormData();
        List<BAMData> bamDataList = getModel().getCombinedQCBAMData();
        StringBuilder sb = new StringBuilder();
        Text text = new Text();
        
        sb.append("Project Title: ").append(sraFreeFormData.getTitle().get()).append("\n")
                .append("dbGaP Alias: ").append(sraFreeFormData.getAlias().get()).append("\n")
                .append("Center Name: ").append(sraFreeFormData.getCenterName().get()).append("\n")
                .append("Reference Genome: ").append(sraFreeFormData.getReferencGenome().get()).append("\n")
                .append("Library Info:\n")
                .append("\tStrategy: ").append(sraFreeFormData.getLibraryStrategy().get()).append("\n")
                .append("\tSource: ").append(sraFreeFormData.getLibrarySource().get()).append("\n")
                .append("\tSelection: ").append(sraFreeFormData.getLibrarySelection().get()).append("\n")
                .append("\tLayout: ").append(sraFreeFormData.getLibraryLayout().get()).append("\n")
                .append("Contacts\n");

        for (Contact contact : sraFreeFormData.getContacts()) {
            sb.append("\t").append(contact.getName()).append("\n")
                    .append("\t\tInform on Status: ").append(contact.getInformOnStatus()).append("\n")
                    .append("\t\tInform on Error: ").append(contact.getInformOnError()).append("\n");
        }

        if (bamDataList.isEmpty() == false) {
            sb.append(bamDataList.size()).append(" BAM Files:\n");

            for (BAMData bamData : bamDataList) {
                BAMHeaderParser.BAMHeaderData headerData = bamData.getHeaderData();
                QCReportParser.QCReportRecord qcRecord = bamData.getQcReportData();

                sb.append("\tFilename: ").append(headerData.getFilename()).append(NEW_LINE)
                        .append("\t\tChecksum: ").append(headerData.getChecksum()).append(NEW_LINE)
                        .append("\t\tLocal ID: ").append(qcRecord.getLocalID()).append(NEW_LINE)
                        .append("\t\tExperiment Name: ").append(qcRecord.getExperimentName()).append(NEW_LINE)
                        .append("\t\tRun Name: ").append(qcRecord.getRunName()).append(NEW_LINE)
                        .append("\t\tLibrary Name: ").append(qcRecord.getLibraryName()).append(NEW_LINE)
                        .append("\t\tLibrary Prep Protocol: ").append(qcRecord.getLibraryPrepProtocol()).append(NEW_LINE)
                        .append("\t\tLibrary Prep Description: ").append(qcRecord.getLibraryPrepDescription()).append(NEW_LINE)
                        .append("\t\tRead One Length: ").append(qcRecord.getReadOneLength()).append(NEW_LINE)
                        .append("\t\tRead Two Length: ").append(qcRecord.getReadTwoLength()).append(NEW_LINE)
                        .append("\t\tMean Insert Size: ").append(qcRecord.getInsertSize()).append(NEW_LINE)
                        .append("\t\tInsert Size SDev: ").append(qcRecord.getInsertSDev()).append(NEW_LINE)
                        .append("\t\tRuns: ").append(NEW_LINE);

                for (BAMHeaderParser.BAMRunData runData : headerData.getRuns()) {
                    sb.append("\t\t\tID: ").append(runData.getId()).append(NEW_LINE)
                            .append("\t\t\t\tIPlatform: ").append(runData.getPlatform()).append(NEW_LINE)
                            .append("\t\t\t\tInstrument: ").append(runData.getInstrument()).append(NEW_LINE)
//                            .append("\t\t\t\tRun Name: ").append(runData.getRunName()).append(NEW_LINE)
                            .append("\t\t\t\tLibrary Name: ").append(runData.getLibraryName()).append(NEW_LINE)
                            .append("\t\t\t\tExperiment Name: ").append(runData.getExperimentName()).append(NEW_LINE)
                            .append("\t\t\t\tCenter Name: ").append(runData.getCenterName()).append(NEW_LINE);
                }

                sb.append("\t\tSoftware:").append(NEW_LINE);

                for (BAMHeaderParser.BAMSoftwareData softwareData : headerData.getSoftware()) {
                    sb.append("\t\t\t").append(softwareData.getName()).append(" ").append(softwareData.getVersion()).append(NEW_LINE);
                }

            }
        } else {
            sb.append("No BAM information available");
        }

        text.setText(sb.toString());
//        text.setWrappingWidth(reviewTextFlow.getWidth() - 10.0);
        reviewTextFlow.getChildren().setAll(text);
    }

}
