/*Copyright (c) 2015 Johns Hopkins Center for Inherited Disease Research

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.jhmi.cidr.SRA_Submission.view.wizardpages;

import com.google.common.base.Joiner;
import edu.jhmi.cidr.SRA_Submission.objects.Contact;
import edu.jhmi.cidr.SRA_Submission.objects.SRAFreeFormData;
import edu.jhmi.cidr.SRA_Submission.view.util.EmailContactTable;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;
import javafx.collections.ListChangeListener;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import org.controlsfx.dialog.Wizard;
import org.controlsfx.validation.Validator;

/**
 * FXML Controller class
 *
 * @author Alice Sanchez
 */
public class SetEmailsPageController extends WizardPageViewController implements Initializable {

    private final Contact placeHolderContact = new Contact("", "", "");
    private EmailContactTable emailContactTable;

    @FXML
    private TextField nameField;

    @FXML
    private TextField informOnErrorField;

    @FXML
    private TableView<Contact> contactTable;

    @FXML
    private TextField informOnStatusField;

    @FXML
    private TextField validationField;

    @FXML
    void onAddContact(ActionEvent event) {
        Contact contact = new Contact(nameField.getText(), informOnStatusField.getText(), informOnErrorField.getText());
        List<String> errors = contact.validate();

        if (errors.isEmpty()) {
            if (contactTable.getItems().contains(placeHolderContact)){
                contactTable.getItems().clear();
            }

            emailContactTable.addItem(contact);
            emailContactTable.refreshTable(false);

            nameField.setText("");
            informOnStatusField.setText("");
            informOnErrorField.setText("");
            contactTable.requestFocus();
        } else {
            Alert alert = new Alert(Alert.AlertType.ERROR);

            alert.setContentText(Joiner.on("\n").join(errors));
            alert.show();
        }
    }

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        emailContactTable = new EmailContactTable(contactTable);
        contactTable.getItems().add(placeHolderContact);
//        contactTable.getItems().add(new Contact("", "", ""));

        nameField.setText("");
        informOnStatusField.setText("");
        informOnErrorField.setText("");

        contactTable.getItems().addListener((ListChangeListener.Change<? extends Contact> c) -> {
            List<Contact> items = contactTable.getItems();

            if (items.contains(placeHolderContact)) {
                validationField.setText("");
            } else {
                validationField.setText("Valid");
            }
        });
    }

    @Override
    public void onEnteringPage(Wizard wizard) {
        SRAFreeFormData data = getModel().getSraFreeFormData();

        if (data.getContacts().isEmpty() == false) {
            contactTable.getItems().setAll(data.getContacts());
        }

        wizard.getValidationSupport().registerValidator(validationField, Validator.createEmptyValidator("Requires atleast one contact"));
//                Validator.combine(Validator.createEmptyValidator("M"),
//                Validator.createPredicateValidator((t) -> {
//                    System.out.println((contactTable.getItems().isEmpty() == false
//                            && contactTable.getItems().get(0).getName().isEmpty() == false));
//                    return (contactTable.getItems().isEmpty() == false
//                    && contactTable.getItems().get(0).getName().isEmpty() == false);
//                }, "Yup")));
    }

    @Override
    public void onExitingPage(Wizard wizard) {
        if (contactTable.getItems().contains(placeHolderContact) == false) {
            getModel().getSraFreeFormData().getContacts().setAll(contactTable.getItems());
        }
    }

    @Override
    public SRAWizard.WizardPageInfo getNextPage() {
        contactTable.requestFocus();

        if (contactTable.getItems().contains(placeHolderContact)) {
            return null;
        }

        return SRAWizard.WizardPageInfo.REVIEW;
    }

}
