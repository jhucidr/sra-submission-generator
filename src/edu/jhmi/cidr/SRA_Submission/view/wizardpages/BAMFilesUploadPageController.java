/*Copyright (c) 2015 Johns Hopkins Center for Inherited Disease Research

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.jhmi.cidr.SRA_Submission.view.wizardpages;

import com.google.common.base.Joiner;
import com.google.common.collect.HashMultimap;
import com.google.common.collect.Multimap;
import com.google.common.io.Files;
import edu.jhmi.cidr.SRA_Submission.util.parsers.BAMHeaderParser;
import edu.jhmi.cidr.SRA_Submission.util.parsers.BAMHeaderParser.BAMHeaderData;
import edu.jhmi.cidr.SRA_Submission.util.parsers.MD5ChecksumFileParser;
import edu.jhmi.cidr.SRA_Submission.view.util.BAMHeaderDataTable;
import edu.jhmi.cidr.SRA_Submission.view.util.FileListParser;
import edu.jhmi.cidr.SRA_Submission.view.util.GuiUtils;
import edu.jhmi.cidr.lib.conveniencestore.guiutils.GuiLoadScreen;
import java.io.File;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.Set;
import javafx.application.Platform;
import javafx.collections.ListChangeListener;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.input.DragEvent;
import javafx.scene.input.Dragboard;
import javafx.scene.input.TransferMode;
import javafx.scene.layout.StackPane;
import javafx.stage.FileChooser;
import javafx.util.converter.NumberStringConverter;
import org.controlsfx.dialog.Wizard;
import org.controlsfx.validation.Validator;

/**
 * FXML Controller class
 *
 * @author Alice Sanchez
 */
public class BAMFilesUploadPageController extends WizardPageViewController implements Initializable {

    private final Multimap<String, BAMHeaderData> bamDataMap = HashMultimap.create();
    private final Map<String, String> checksumMap = new HashMap<>();
    private BAMHeaderDataTable bamHeaderDataTable;
    private GuiLoadScreen loadScreen;
    private File lastDir = null;

    @FXML
    private ListView<String> missingBAMFileListView;

    @FXML
    private Menu removeBamFolderMenu;

    @FXML
    private ListView<String> unknownBAMFilesListView;

    @FXML
    private ListView<String> missingChecksumsListView;

    @FXML
    private TableView<BAMHeaderData> bamDataTable;

    @FXML
    private StackPane tableStackPane;

    @FXML
    private Label bamFileCount;

    @FXML
    private Label unknownBamFileCount;

    @FXML
    private Label missingBamFileCount;

    @FXML
    private Label missingChecksumCount;

    @FXML
    private TextField validationField;

    @FXML
    void onDragDroppedOnBAMTable(DragEvent event) {
        Dragboard dragBoard = event.getDragboard();

        if (dragBoard.hasFiles()) {
            parseFiles(dragBoard.getFiles());
        }
    }

    @FXML
    void onDragOverOnBAMTable(DragEvent event) {
        if (event.getGestureSource() != bamDataTable && event.getDragboard().hasFiles()) {
            event.acceptTransferModes(TransferMode.ANY);
        }
    }

//    @FXML
//    void onAddChecksumFile(ActionEvent event) {
//        FileChooser fileChooser = new FileChooser();
//        List<String> errors = new ArrayList<>();
//        List<File> files;
//
//        fileChooser.setTitle("Select checksum file(s)");
//        fileChooser.getExtensionFilters().addAll(new FileChooser.ExtensionFilter("MD5", "*.md5"), new FileChooser.ExtensionFilter("txt", "*.txt"));
//
//        if (lastDir != null) {
//            fileChooser.setInitialDirectory(lastDir);
//        }
//
//        files = fileChooser.showOpenMultipleDialog(null);
//
//        if (files.isEmpty() == false) {
//            lastDir = files.get(0).getParentFile();
//
//            for (File file : files) {
//                try {
//                    parseChecksumFile(file);
//                } catch (Exception ex) {
//                    errors.add(file.getName() + ": " + ex.getMessage());
//                    ex.printStackTrace();
//                }
//            }
//
//            if (errors.isEmpty() == false) {
//                showErrorDialog(errors);
//            }
//
//            refreshDataView();
//        }
//    }
    @FXML
    void onAddListofBAMFolders(ActionEvent event) {
        FileChooser fileChooser = new FileChooser();
        File selected;

        fileChooser.setTitle("Select BAM Folder List File");
        fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("csv", "*.csv"));
        selected = fileChooser.showOpenDialog(null);

        if (selected != null) {
            FileListParser parser = new FileListParser(selected);

            try {
                parseFiles(parser.parse());
            } catch (Exception ex) {
                Alert alert = new Alert(Alert.AlertType.ERROR);

                alert.setContentText("Error processing files: " + ex.getMessage());
                alert.show();

                ex.printStackTrace();
            }

        }
    }

    private void parseFiles(List<File> files) {
        new Thread(() -> {

            try {
                List<String> errors = new ArrayList<>();

                Platform.runLater(() -> {
                    loadScreen.show();
                });
                for (File file : files) {
                    if (file.isFile()) {
                        String ext = Files.getFileExtension(file.getName());

                        if (ext.equalsIgnoreCase("bam")) {
                            errors.addAll(parseBamFile(file));
                        } else if (ext.equalsIgnoreCase("md5") || ext.equalsIgnoreCase("txt")) {
                            parseChecksumFile(file);
                        }
                    } else {
                        errors.addAll(parseBamFolder(file));
                    }
                }

                if (errors.isEmpty() == false) {
                    Platform.runLater(() -> {
                        GuiUtils.showDialog("Error", "Error processing file(s)", errors);
                    });
                }

            } catch (Exception ex) {
                Platform.runLater(() -> {
                    Alert alert = new Alert(Alert.AlertType.ERROR);

                    alert.setContentText("Error processing: " + ex.getMessage());
                    alert.show();
                });

                ex.printStackTrace();
            } finally {
                Platform.runLater(() -> {
                    refreshDataView();
                    loadScreen.hide();
                });
            }
        }).start();
    }

//    private void showErrorDialog(List<String> errors) {
//        Alert alert = new Alert(Alert.AlertType.ERROR);
//
//        alert.setTitle("Error");
//        alert.setContentText(Joiner.on("\n").join(errors));
//
//        alert.showAndWait();
//    }
    private Set<String> getBamFilesExperimentNames() {
        Set<String> experimentNames = new HashSet<>();

        for (BAMHeaderData bamHeaderData : bamDataMap.values()) {
            experimentNames.add(bamHeaderData.getRuns().get(0).getExperimentName());
        }

        return experimentNames;
    }

    private void refreshMissingUnknownListViews() {
        Set<String> qcReportExperimentNames = getModel().getQCReportExperimentalNames();

        if (qcReportExperimentNames != null) {
            Set<String> experimentNames = getBamFilesExperimentNames();
            Set<String> intersection = new HashSet<>(qcReportExperimentNames);
            Set<String> missingBams = new HashSet<>(qcReportExperimentNames);
            Set<String> unknownBams = new HashSet<>(experimentNames);

            intersection.retainAll(experimentNames);
            missingBams.removeAll(intersection);
            unknownBams.removeAll(intersection);

            missingBAMFileListView.getItems().setAll(missingBams);
            missingBamFileCount.setText(String.valueOf(missingBams.size()));

            unknownBAMFilesListView.getItems().setAll(unknownBams);
            unknownBamFileCount.setText(String.valueOf(unknownBams.size()));
        } else {
            unknownBAMFilesListView.getItems().setAll(getBamFilesExperimentNames());
            unknownBamFileCount.setText(String.valueOf(unknownBAMFilesListView.getItems().size()));
        }

    }

    private void refreshMissingChecksumListView() {
        missingChecksumsListView.getItems().clear();

        for (BAMHeaderData bamHeaderData : bamDataMap.values()) {
            if (bamHeaderData.getChecksum() == null
                    || bamHeaderData.getChecksum().isEmpty()) {
                missingChecksumsListView.getItems().add(bamHeaderData.getRuns().get(0).getExperimentName());
            }
        }

        missingChecksumCount.setText(String.valueOf(missingChecksumsListView.getItems().size()));
    }

    private void refreshRemoveMenu() {
        removeBamFolderMenu.getItems().clear();

        for (String folderName : bamDataMap.keySet()) {
            MenuItem menuItem = new MenuItem(folderName);

            menuItem.setOnAction((ActionEvent t) -> {
                bamDataTable.getContextMenu().hide();

                bamDataMap.removeAll(folderName);
                refreshDataView();
            });

            removeBamFolderMenu.getItems().add(menuItem);
        }
    }

    private void refreshDataView() {
        bamHeaderDataTable.setItems(bamDataMap.values());

        refreshMissingUnknownListViews();
        refreshMissingChecksumListView();
        refreshRemoveMenu();
    }

    private List<String> parseBamFolder(File bamFolder) {
        List<String> errors = new ArrayList<>();
        File[] bamFiles = bamFolder.listFiles((File pathname) -> Files.getFileExtension(pathname.getName()).equalsIgnoreCase("bam"));

        for (File file : bamFiles) {
            errors.addAll(parseBamFile(file));
        }

        return errors;
    }

    private List<String> parseBamFile(File bamFile) {
        BAMHeaderParser parser = new BAMHeaderParser(bamFile);
        BAMHeaderData bamHeaderData = parser.parse();

        if (parser.getErrors().isEmpty()) {
            String checksum = checksumMap.get(bamHeaderData.getFilename());

            if (checksum != null) {
                bamHeaderData.setChecksum(checksum);
            }

            bamDataMap.put(bamFile.getParentFile().getAbsolutePath(), bamHeaderData);
        }

        return parser.getErrors();
    }

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        final BAMHeaderData placeholder = new BAMHeaderData();
        bamHeaderDataTable = new BAMHeaderDataTable(bamDataTable);
        loadScreen = new GuiLoadScreen(tableStackPane);
        bamFileCount.textProperty().bindBidirectional(bamHeaderDataTable.numberRowsProperty(), new NumberStringConverter());

        validationField.setText("");
        bamDataTable.getItems().add(placeholder);
        missingChecksumsListView.getItems().add("");
        unknownBAMFilesListView.getItems().add("");
        missingBAMFileListView.getItems().add("");

        bamDataTable.getItems().addListener(new ListChangeListener<BAMHeaderData>() {

            @Override
            public void onChanged(ListChangeListener.Change<? extends BAMHeaderData> c) {
                if (bamDataTable.getItems().isEmpty() || bamDataTable.getItems().contains(placeholder)) {
                    validationField.setText("");
                } else {
                    validationField.setText("Valid");
                }
            }
        });
    }

    @Override
    public SRAWizard.WizardPageInfo getNextPage() {
        missingChecksumsListView.getItems().add("");
        missingChecksumsListView.requestFocus();
        unknownBAMFilesListView.getItems().add("");
        unknownBAMFilesListView.requestFocus();
        missingBAMFileListView.getItems().add("");
        missingBAMFileListView.requestFocus();;
        bamDataTable.requestFocus();

        return SRAWizard.WizardPageInfo.SET_EMAILS;
    }

    @Override
    public void onEnteringPage(Wizard wizard) {
        wizard.getValidationSupport().registerValidator(validationField, Validator.createEmptyValidator("Requires at;easy one BAM file"));
    }

    @Override
    public void onExitingPage(Wizard wizard) {
        getModel().setBamHeaderRecords(bamDataMap.values());
    }

    private void parseChecksumFile(File file) throws Exception {
        MD5ChecksumFileParser parser = new MD5ChecksumFileParser(file);

        parser.parse();
        checksumMap.putAll(parser.getChecksumMap());

        for (BAMHeaderData bamHeaderData : bamDataMap.values()) {
            String checksum = checksumMap.get(bamHeaderData.getFilename());

            if (checksum != null) {
                bamHeaderData.setChecksum(checksum);
            }
        }
    }

}
