/*Copyright (c) 2015 Johns Hopkins Center for Inherited Disease Research

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.jhmi.cidr.SRA_Submission.view.wizardpages;

import edu.jhmi.cidr.SRA_Submission.model.models.SRASubmissionModel;
import edu.jhmi.cidr.SRA_Submission.objects.SRAFreeFormData;
import java.io.File;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TextField;
import javafx.stage.FileChooser;
import org.controlsfx.dialog.Wizard;
import org.controlsfx.validation.ValidationSupport;
import org.controlsfx.validation.Validator;

/**
 * FXML Controller class
 *
 * @author Alice Sanchez
 */
public class ProjectInfoPageController extends WizardPageViewController implements Initializable {

    @FXML
    private TextField librarySourceField;

    @FXML
    private TextField libraryLayoutField;

    @FXML
    private TextField dbGapAliasField;

    @FXML
    private TextField projectTitleField;

    @FXML
    private TextField centerNameField;

    @FXML
    private TextField libraryStrategyField;

    @FXML
    private TextField librarySelectionField;

    @FXML
    private TextField referenceGenomeField;

//    @FXML
//    void onBrowseForLibraryProtocolFile(ActionEvent event) {
//        FileChooser fileChooser = new FileChooser();
//        File workingDir = getModel().getWorkingDirectory();
//        File selectedFile;
//        
//        fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("csv", "csv"));
//       
//        if(workingDir != null){
//            fileChooser.setInitialDirectory(workingDir);
//        }
//        
//        selectedFile = fileChooser.showOpenDialog(null);
//        
//        if(selectedFile != null){
//            libraryProtocolFileField.setText(selectedFile.getAbsolutePath());
//        }
//    }

    public void setupValidation(ValidationSupport validationSupport) {
        validationSupport.registerValidator(projectTitleField, true, Validator.createEmptyValidator("Project title is required"));
        validationSupport.registerValidator(dbGapAliasField, true, Validator.createEmptyValidator("dbGap alias is required"));
        validationSupport.registerValidator(centerNameField, true, Validator.createEmptyValidator("Center name is required"));
        validationSupport.registerValidator(referenceGenomeField, true, Validator.createEmptyValidator("Reference genome is required"));
        validationSupport.registerValidator(libraryStrategyField, true, Validator.createEmptyValidator("Library Strategy is required"));
        validationSupport.registerValidator(librarySourceField, true, Validator.createEmptyValidator("Library Source is required"));
        validationSupport.registerValidator(librarySelectionField, true, Validator.createEmptyValidator("Library Selection is required"));
        validationSupport.registerValidator(libraryLayoutField, true, Validator.createEmptyValidator("Library Layout is required"));
    }

    @Override
    public SRAWizard.WizardPageInfo getNextPage() {
        return SRAWizard.WizardPageInfo.LIBRARY_PREP;
    }

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
//        dataEntryModeGroup.selectedToggleProperty().addListener((ObservableValue<? extends Toggle> observable, Toggle oldValue, Toggle newValue) -> {
//            if (dataEntryAutoRadio.isSelected()) {
//                masterDataFileField.setDisable(false);
//                masterDataFileBrowseButton.setDisable(false);
//            } else {
//                masterDataFileField.setDisable(true);
//                masterDataFileBrowseButton.setDisable(true);
//
//            }
//        });
    }

    @Override
    public void onEnteringPage(Wizard wizard) {
        SRASubmissionModel model = getModel();

        if (model != null) {
            SRAFreeFormData freeFormData = model.getSraFreeFormData();
            projectTitleField.textProperty().bindBidirectional(freeFormData.getTitle());
            dbGapAliasField.textProperty().bindBidirectional(freeFormData.getAlias());
            centerNameField.textProperty().bindBidirectional(freeFormData.getCenterName());
            referenceGenomeField.textProperty().bindBidirectional(freeFormData.getReferencGenome());
            libraryStrategyField.textProperty().bindBidirectional(freeFormData.getLibraryStrategy());
            librarySourceField.textProperty().bindBidirectional(freeFormData.getLibrarySource());
            librarySelectionField.textProperty().bindBidirectional(freeFormData.getLibrarySelection());
            libraryLayoutField.textProperty().bindBidirectional(freeFormData.getLibraryLayout());
        }

        setupValidation(wizard.getValidationSupport());
    }

    @Override
    public void onExitingPage(Wizard wizard) {
        super.onExitingPage(wizard); 
    }
}
