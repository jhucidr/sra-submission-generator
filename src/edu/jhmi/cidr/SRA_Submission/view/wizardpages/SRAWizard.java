/*Copyright (c) 2015 Johns Hopkins Center for Inherited Disease Research

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.jhmi.cidr.SRA_Submission.view.wizardpages;

import edu.jhmi.cidr.SRA_Submission.model.models.SRASubmissionModel;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import org.controlsfx.dialog.Wizard;

/**
 *
 * @author Alice Sanchez
 */
public class SRAWizard extends Wizard {

    private final SRASubmissionModel model;
    private final Map<WizardPageInfo, WizardPane> wizardPageMap = new HashMap<>();

    public SRAWizard(SRASubmissionModel model) {
        this.model = model;
        setupWizard();
    }

    private void setupWizard() {
        this.setFlow(new Flow() {

            @Override
            public Optional<WizardPane> advance(WizardPane wp) {
                Optional<WizardPane> nextPage = Optional.empty();

                try {
                    if (wp == null) {
                        nextPage = Optional.of(getWizardPane(WizardPageInfo.PROJECT_INFO));
                    }
                    if (wp instanceof WizardPageViewController) {
                        WizardPageViewController currentController = (WizardPageViewController) wp;
                        WizardPageInfo page = currentController.getNextPage();

                        if (page != null) {
                            WizardPageViewController newController = (WizardPageViewController) getWizardPane(page);

                            nextPage = Optional.of(newController);
                        }
                    }
                } catch (IOException ex) {
                    ex.printStackTrace();
                }

                return nextPage;
            }

            @Override
            public boolean canAdvance(WizardPane wp) {
                if (wp == null) {
                    return true;
                } else {
                    return ((WizardPageViewController) wp).getWizardPage() != WizardPageInfo.REVIEW;
                }
            }
        });
    }

    private Wizard.WizardPane getWizardPane(WizardPageInfo page) throws IOException {
        Wizard.WizardPane wizardPane = wizardPageMap.get(page);

        if (wizardPane == null) {
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource(page.getfXMLPath()));
            Parent root = (Parent) fxmlLoader.load();
            WizardPageViewController viewController = (WizardPageViewController) fxmlLoader.getController();

            wizardPane = viewController;
            viewController.setModel(model);
            viewController.setWizardPage(page);
            wizardPane.setContent(root);
            wizardPageMap.put(page, wizardPane);
        }

        return wizardPane;
    }

    public enum WizardPageInfo implements WizardPage {

        PROJECT_INFO("ProjectInfoPage.fxml"),
        LIBRARY_PREP("LibraryPrepPage.fxml"),
        QC_REPORT_UPLOAD("QCReportUploadPage.fxml"),
        BAM_FILES_UPLOAD("BAMFilesUploadPage.fxml"),
        SET_EMAILS("SetEmailsPage.fxml"),
        REVIEW("ReviewPage.fxml")
        ;
        private final String fXMLPath;

        private WizardPageInfo(String fXMLPath) {
            this.fXMLPath = fXMLPath;
        }

        public String getfXMLPath() {
            return fXMLPath;
        }

    }

    public interface WizardPage {

        public String getfXMLPath();
    }
}
