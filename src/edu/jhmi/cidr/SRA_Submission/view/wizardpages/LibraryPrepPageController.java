/*Copyright (c) 2015 Johns Hopkins Center for Inherited Disease Research

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.jhmi.cidr.SRA_Submission.view.wizardpages;

import com.google.common.base.Joiner;
import edu.jhmi.cidr.SRA_Submission.objects.LibraryProtocol;
import edu.jhmi.cidr.SRA_Submission.util.parsers.LibraryPrepFileParser;
import java.io.File;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Control;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.Toggle;
import javafx.scene.control.ToggleGroup;
import javafx.stage.FileChooser;
import org.controlsfx.dialog.Wizard;
import org.controlsfx.validation.Validator;

/**
 * FXML Controller class
 *
 * @author Alice Sanchez
 */
public class LibraryPrepPageController extends WizardPageViewController implements Initializable {

    private Mode currentMode = Mode.DISABLED;
    private Map<Control, Validator> validatorMap = new HashMap<>();
    private Map<String, String> libPrepMap = new HashMap<>();

    @FXML
    private TextField libraryPrepFileField;

    @FXML
    private Button browseLibraryPrepButton;

    @FXML
    private TextArea libraryPrepDescTextArea;

    @FXML
    private RadioButton fileModeRadio;

    @FXML
    private RadioButton qcReportModeRadio;

    @FXML
    private ComboBox<String> libraryPrepProtocolComboBox;

    @FXML
    private ToggleGroup libraryPrepMode;

    @FXML
    private RadioButton manualModeRadio;

    @FXML
    private TextField validationField;

    @FXML
    void onBrowseLibraryPrepFile(ActionEvent event) {
        FileChooser fileChooser = new FileChooser();
        File selected;

        fileChooser.setTitle("Select Library Prep Protocol File");
        fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("csv", "*.csv"));
        selected = fileChooser.showOpenDialog(null);

        if (selected != null) {
            libraryPrepFileField.setText(selected.getAbsolutePath());
        }
    }

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
//        libraryPrepFileField.setText("C:\\Users\\asanch15\\Desktop\\LibraryPrep_versions_Mendel_SRAsubRef.csv");
        libraryPrepFileField.setText("");
        libraryPrepDescTextArea.setText("");
        libraryPrepProtocolComboBox.setValue("");
        validationField.setText("");

        libraryPrepMode.selectedToggleProperty().addListener((ObservableValue<? extends Toggle> observable, Toggle oldValue, Toggle newValue) -> {
            if (fileModeRadio.isSelected()) {
                setMode(Mode.FILE);
            } else if (manualModeRadio.isSelected()) {
                setMode(Mode.MANUAL);
            } else if (qcReportModeRadio.isSelected()) {
//                setMode(Mode.QC_REPORT);
            }
        });
        qcReportModeRadio.setDisable(true);
        setMode(Mode.DISABLED);

        libraryPrepFileField.textProperty().addListener((ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
            setValid(currentMode == Mode.FILE
                    && validString(libraryPrepFileField.getText()));
        });
        libraryPrepProtocolComboBox.valueProperty().addListener((ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
            setValid(currentMode == Mode.MANUAL
                    && validString(newValue)
                    && validString(libraryPrepDescTextArea.getText()));
        });
        libraryPrepDescTextArea.textProperty().addListener((ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
            setValid(currentMode == Mode.MANUAL
                    && validString(libraryPrepProtocolComboBox.getValue())
                    && validString(newValue));
        });
        libraryPrepProtocolComboBox.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<String>() {

            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                String libDesc = libPrepMap.get(newValue);

                if (libDesc != null) {
                    libraryPrepDescTextArea.setText(libDesc);
                }
            }
        });
    }

    private void setValid(boolean valid) {
        validationField.setText(valid ? "Valid" : "");
    }

    private void setMode(Mode mode) {
        boolean fileModeDisable;
        boolean manualModelDisable;

        this.currentMode = mode;
        switch (mode) {
            case FILE:
                fileModeDisable = false;
                manualModelDisable = true;

                setValid(validString(libraryPrepFileField.getText()));
                break;
            case MANUAL:
                fileModeDisable = true;
                manualModelDisable = false;

                setValid(validString(libraryPrepProtocolComboBox.getValue())
                        && validString(libraryPrepDescTextArea.getText()));
                break;
            case DISABLED:
                fileModeDisable = true;
                manualModelDisable = true;

                setValid(false);
                break;
            default:
                return;
        }

        libraryPrepFileField.setDisable(fileModeDisable);
        browseLibraryPrepButton.setDisable(fileModeDisable);
        libraryPrepProtocolComboBox.setDisable(manualModelDisable);
        libraryPrepDescTextArea.setDisable(manualModelDisable);
    }

    private boolean validString(String str) {
        return str != null && str.trim().isEmpty() == false;
    }

    @Override
    public void onExitingPage(Wizard wizard) {
        switch (currentMode) {
            case QC_REPORT:
            case DISABLED:
                libraryPrepFileField.setText("");
                libraryPrepProtocolComboBox.setValue("");
                libraryPrepDescTextArea.setText("");
                break;
            case FILE:
                libraryPrepProtocolComboBox.setValue("");
                libraryPrepDescTextArea.setText("");
                setValid(true);
                break;
            case MANUAL:
                libraryPrepFileField.setText("");
                setValid(true);
                break;
        }
    }

    private Map<String, LibraryProtocol> parseLibraryPrepFile(File libraryPrepFile) throws Exception {
        LibraryPrepFileParser parser = new LibraryPrepFileParser(libraryPrepFile);

        parser.parse();

        if (parser.getErrors().isEmpty() == false) {
            Alert alert = new Alert(Alert.AlertType.ERROR);

            alert.setTitle("Error parsing library prep protocol file");
            alert.setContentText(Joiner.on("\n").join(parser.getErrors()));
            alert.show();

            return null;
        }

        return parser.getLibraryProtocolProjectMap();
    }

    @Override
    public void onEnteringPage(Wizard wizard) {
        wizard.getValidationSupport().registerValidator(validationField, Validator.createEmptyValidator(""));

        libPrepMap.clear();
        libraryPrepProtocolComboBox.getItems().clear();
        for (LibraryProtocol libraryProtocol : getModel().getLibraryProtocols()) {
            libPrepMap.put(libraryProtocol.getName(), libraryProtocol.getDescription());
            libraryPrepProtocolComboBox.getItems().add(libraryProtocol.getName());
        }

//        wizard.getValidationSupport().
    }

    @Override
    public SRAWizard.WizardPageInfo getNextPage() {
        if (currentMode == Mode.FILE) {
            try {
                Map<String, LibraryProtocol> projectLibraryMap = parseLibraryPrepFile(new File(libraryPrepFileField.getText()));

                if (projectLibraryMap == null) {
                    return null;
                } else {
                    getModel().setProjectLibraryMap(projectLibraryMap);
                }
            } catch (Exception ex) {
                Alert alert = new Alert(Alert.AlertType.ERROR);

                alert.setTitle("Error parsing library prep protocol file");
                alert.setContentText(ex.getMessage());
                alert.show();

                ex.printStackTrace();
            }
        } else if (currentMode == Mode.MANUAL) {
            String protocol = libraryPrepProtocolComboBox.getValue();
            String desc = libraryPrepDescTextArea.getText();

            if (protocol == null || protocol.isEmpty()) {
                Alert alert = new Alert(Alert.AlertType.ERROR);

                alert.setTitle("Error");
                alert.setContentText("Library Prep Protocol can not be empty");
                alert.show();
                return null;
            } else {
                getModel().setDefaultLibraryProtocol(new LibraryProtocol(protocol, desc));
            }
        }

        return SRAWizard.WizardPageInfo.QC_REPORT_UPLOAD;
    }

    private enum Mode {

        DISABLED,
        FILE,
        MANUAL,
        QC_REPORT;
    }

}
