/*Copyright (c) 2015 Johns Hopkins Center for Inherited Disease Research

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.jhmi.cidr.SRA_Submission.view.wizardpages;

import com.google.common.base.Joiner;
import com.google.common.collect.HashMultimap;
import com.google.common.collect.Multimap;
import edu.jhmi.cidr.SRA_Submission.objects.LibraryProtocol;
import edu.jhmi.cidr.SRA_Submission.util.parsers.QCReportParser;
import edu.jhmi.cidr.SRA_Submission.view.util.QCReportDataTable;
import edu.jhmi.cidr.SRA_Submission.view.util.FileListParser;
import edu.jhmi.cidr.SRA_Submission.view.util.GuiUtils;
import edu.jhmi.cidr.lib.conveniencestore.guiutils.GuiLoadScreen;
import java.io.File;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.ResourceBundle;
import javafx.application.Platform;
import javafx.collections.ListChangeListener;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.input.DragEvent;
import javafx.scene.input.Dragboard;
import javafx.scene.input.TransferMode;
import javafx.scene.layout.StackPane;
import javafx.stage.FileChooser;
import org.controlsfx.dialog.Wizard;
import org.controlsfx.validation.ValidationSupport;
import org.controlsfx.validation.Validator;

/**
 * FXML Controller class
 *
 * @author Alice Sanchez
 */
public class QCReportUploadPageController extends WizardPageViewController implements Initializable {

    private static final List<String> DEFAULT_SAMPLE_ID_COLUMN_NAMES = Arrays.asList("Local_Id", "SM_tag");
    private static final List<String> DEFAULT_RUN_GROUP_ID_COLUMNS = Arrays.asList("RG_PU", "RG_ID", "RG_ID_PU");

    private Multimap<String, QCReportParser.QCReportRecord> qcReportRecordsMap = HashMultimap.create();

    private GuiLoadScreen loadScreen;
    private QCReportDataTable qcReportDataTable;

    @FXML
    private TextField validationField;

    @FXML
    private Menu removeQCReportMenu;

    @FXML
    private ComboBox<String> runGroupIDComboBox;

    @FXML
    private TableView<QCReportParser.QCReportRecord> qcDataTableView;

    @FXML
    private ComboBox<String> sampleIDColumnComboBox;

    @FXML
    private ComboBox<String> experimentNameComboBox;

    @FXML
    private StackPane tableStackPane;

    @FXML
    void onAddQCReportList(ActionEvent event) {
        FileChooser fileChooser = new FileChooser();
        File selected;

        fileChooser.setTitle("Select QC Report List File");
        fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("csv", "*.csv"));
        selected = fileChooser.showOpenDialog(null);

        if (selected != null) {
            FileListParser parser = new FileListParser(selected);

            try {
                parseQCReports(parser.parse());
            } catch (Exception ex) {
                Alert alert = new Alert(Alert.AlertType.ERROR);

                alert.setTitle("Error");
                alert.setContentText(ex.getMessage());

                alert.show();
                ex.printStackTrace();
            }
        }
    }

    @FXML
    void onDragDroppedQCDataTable(DragEvent event) {
        Dragboard dragBoard = event.getDragboard();

        if (dragBoard.hasFiles()) {
            String sampleIDColumnName = sampleIDColumnComboBox.getValue();
            String runGroupIDColumnName = runGroupIDComboBox.getValue();
            String experimentNameColumnName = experimentNameComboBox.getValue();

            if (isEmptyString(sampleIDColumnName) || isEmptyString(runGroupIDColumnName) || isEmptyString(experimentNameColumnName)) {
                Alert alert = new Alert(Alert.AlertType.ERROR);

                alert.setTitle("Error");
                alert.setContentText("You must set Sample ID, Unique Run Group, and Experiment Name before uploading a QC Report");

                alert.showAndWait();
            } else {
                final List<File> files = dragBoard.getFiles();

                if (files.isEmpty() == false) {
                    new Thread(() -> {
                        List<String> errors;

                        toggleLoadScreen(true);
                        errors = parseQCReports(files);
                        toggleLoadScreen(false);

                        if (errors.isEmpty() == false) {
                            Platform.runLater(() -> {
                                GuiUtils.showDialog("Error", "Error processing QC Report(s)", errors);
                            });
                        }
                    }).start();
                }
            }
        }
    }

    @FXML
    void onDragOverQCDataTable(DragEvent event) {
        if (event.getGestureSource() != qcDataTableView && event.getDragboard().hasFiles()) {
            event.acceptTransferModes(TransferMode.ANY);
        }
    }

    private void refreshQCDataTable() {
        removeQCReportMenu.getItems().clear();
        qcDataTableView.getItems().clear();

        for (String qcFileName : qcReportRecordsMap.keySet()) {
            MenuItem removeQCReportItem = new MenuItem(qcFileName);

            qcDataTableView.getItems().addAll(qcReportRecordsMap.get(qcFileName));
            removeQCReportMenu.getItems().add(removeQCReportItem);

            removeQCReportItem.setOnAction((ActionEvent event) -> {
                qcReportRecordsMap.removeAll(qcFileName);
                refreshQCDataTable();
            });
        }

        qcDataTableView.requestFocus();
    }

    private List<String> parseQCReports(List<File> files) {
        final List<String> errors = new ArrayList<>();

        files.stream().forEach((file) -> {
            errors.addAll(parseQCReportFile(file));
        });

        Platform.runLater(() -> {
            refreshQCDataTable();

            if (errors.isEmpty() == false) {
                setValid(false);
            }
        });

        return errors;
    }

    private void toggleLoadScreen(final boolean show) {
        Platform.runLater(() -> {
            if (show) {
                loadScreen.show();
            } else {
                loadScreen.hide();
            }
        });
    }

    private void showErrorDialog(List<String> errors) {
        Alert alert = new Alert(Alert.AlertType.ERROR);

        alert.setTitle("Error");
        alert.setContentText(Joiner.on("\n").join(errors));

        System.out.println("errors = " + errors);
        alert.showAndWait();
    }

    private List<String> parseQCReportFile(File file) {
        QCReportParser parser = new QCReportParser(file, sampleIDColumnComboBox.getValue(), runGroupIDComboBox.getValue(), experimentNameComboBox.getValue());
        List<QCReportParser.QCReportRecord> records = parser.parse();
        List<String> errors = parser.getErrors();
        boolean missingLocalID = false;
        boolean missingExpName = false;
        boolean missingRunName = false;

        if (errors.isEmpty()) {
            for (QCReportParser.QCReportRecord record : records) {
                LibraryProtocol protocol = getModel().getLibraryProtocolForProject(record.getProject());

                if (protocol == null) {
                    errors.add("No library protocol found for project: " + record.getProject());
                } else {
                    record.setLibraryPrepProtocol(protocol.getName());
                    record.setLibraryPrepDescription(protocol.getDescription());

                    if (isEmptyString(record.getLocalID())) {
                        missingLocalID = true;
                    }
                    if (isEmptyString(record.getExperimentName())) {
                        missingExpName = true;
                    }
                    if (isEmptyString(record.getRunName())) {
                        missingRunName = true;
                    }
                }
            }

            if (missingLocalID) {
                errors.add("Missing local ID data in " + file.getName());
            }
            if (missingExpName) {
                errors.add("Missing experiment name data in " + file.getName());
            }
            if (missingRunName) {
                errors.add("Missing run name data in " + file.getName());
            }

            qcReportRecordsMap.replaceValues(file.getName(), records);
        }

        return errors;
    }

    private boolean isEmptyString(String str) {
        return (str == null || str.trim().isEmpty());
    }

    private void setValid(boolean valid) {
        validationField.setText(valid ? "Valid" : "");
    }

    @Override
    public void onEnteringPage(Wizard wizard) {
        ValidationSupport validationSupport = wizard.getValidationSupport();

//        validationSupport.registerValidator(sampleIDColumnComboBox, true, Validator.createEmptyValidator("Sample ID column name can not be empty"));
//        validationSupport.registerValidator(runGroupIDComboBox, true, Validator.createEmptyValidator("Run Group ID column name can not be empty"));
        validationSupport.registerValidator(validationField, Validator.createEmptyValidator(""));
    }

    @Override
    public void onExitingPage(Wizard wizard) {
        getModel().setQCReportRecords(qcReportRecordsMap.values());
    }

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        final QCReportParser.QCReportRecord placeHolder = new QCReportParser.QCReportRecord();
        loadScreen = new GuiLoadScreen(tableStackPane);
        qcReportDataTable = new QCReportDataTable(qcDataTableView);

        sampleIDColumnComboBox.getItems().setAll(DEFAULT_SAMPLE_ID_COLUMN_NAMES);
        experimentNameComboBox.getItems().setAll(DEFAULT_SAMPLE_ID_COLUMN_NAMES);
        runGroupIDComboBox.getItems().setAll(DEFAULT_RUN_GROUP_ID_COLUMNS);

        runGroupIDComboBox.setValue("");
        sampleIDColumnComboBox.setValue("");
        qcDataTableView.getItems().add(placeHolder);
        setValid(false);

        qcDataTableView.getItems().addListener(new ListChangeListener<QCReportParser.QCReportRecord>() {

            @Override
            public void onChanged(ListChangeListener.Change<? extends QCReportParser.QCReportRecord> c) {
                setValid(qcDataTableView.getItems().isEmpty() == false
                        && qcDataTableView.getItems().contains(placeHolder) == false);
            }
        });
    }

    @Override
    public SRAWizard.WizardPageInfo getNextPage() {
        return SRAWizard.WizardPageInfo.BAM_FILES_UPLOAD;
    }

}
