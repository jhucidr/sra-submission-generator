/*Copyright (c) 2015 Johns Hopkins Center for Inherited Disease Research

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.jhmi.cidr.SRA_Submission.model.generator;

import com.google.common.base.Joiner;
import edu.jhmi.cidr.SRA_Submission.model.models.ExperimentModel;
import edu.jhmi.cidr.SRA_Submission.model.models.Model;
import edu.jhmi.cidr.SRA_Submission.model.models.RunModel;
import edu.jhmi.cidr.SRA_Submission.model.models.SubmissionModel;
import edu.jhmi.cidr.SRA_Submission.model.models.elements.ExperimentElement;
import edu.jhmi.cidr.SRA_Submission.model.models.elements.RunElement;
import edu.jhmi.cidr.SRA_Submission.model.models.elements.SubmissionElement;
import static edu.jhmi.cidr.SRA_Submission.model.models.elements.SubmissionElement.SchemaType.EXPERIMENT;
import static edu.jhmi.cidr.SRA_Submission.model.models.elements.SubmissionElement.SchemaType.RUN;
import edu.jhmi.cidr.SRA_Submission.model.objects.BAMData;
import edu.jhmi.cidr.SRA_Submission.objects.Action;
import static edu.jhmi.cidr.SRA_Submission.objects.Action.ActionType.HOLD;
import static edu.jhmi.cidr.SRA_Submission.objects.Action.ActionType.MODIFY;
import static edu.jhmi.cidr.SRA_Submission.objects.Action.ActionType.PROTECT;
import static edu.jhmi.cidr.SRA_Submission.objects.Action.ActionType.RELEASE;
import static edu.jhmi.cidr.SRA_Submission.objects.Action.ActionType.SOURCE;
import static edu.jhmi.cidr.SRA_Submission.objects.Action.ActionType.SUPRESS;
import static edu.jhmi.cidr.SRA_Submission.objects.Action.ActionType.VALIDATE;
import edu.jhmi.cidr.SRA_Submission.objects.Contact;
import edu.jhmi.cidr.SRA_Submission.objects.SRADataGeneratorMessage;
import edu.jhmi.cidr.SRA_Submission.objects.SRAFreeFormData;
import edu.jhmi.cidr.SRA_Submission.util.SRAConstants;
import edu.jhmi.cidr.SRA_Submission.util.parsers.BAMHeaderParser;
import edu.jhmi.cidr.SRA_Submission.util.parsers.QCReportParser;
import java.text.SimpleDateFormat;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import javax.xml.bind.JAXBContext;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

/**
 *
 * @author asanch15
 */
public class SubmissionGenerator extends SRADataGenerator {

    private final static String READ_TYPE = "Application Read";

    private final SRAFreeFormData freeFormData;
    private final List<BAMData> bamDataValues;
    private final Map<SubmissionElement.SchemaType, Model> sourceMap = new HashMap<>();

    private SubmissionElement submissionElement;

    public SubmissionGenerator(SRAFreeFormData freeFormData, List<BAMData> bamDataValues) {
        this.freeFormData = freeFormData;
        this.bamDataValues = bamDataValues;
    }

    private String parseInstrumentName(BAMHeaderParser.BAMHeaderData headerData) {
        BAMHeaderParser.BAMRunData runData = headerData.getRuns().get(0);
        String platform = runData.getPlatform();
        String instrument = runData.getInstrument();
        StringBuilder sb = new StringBuilder();
        char[] strChars = instrument.split("_")[0].toCharArray();

        sb.append(Character.toUpperCase(platform.charAt(0)))
                .append(platform.substring(1).toLowerCase()).append(" ");

        for (int i = 0; i < strChars.length; i++) {
            char c = strChars[i];

            if (Character.isDigit(c) && i > 0) {
                if (Character.isLetter(strChars[i - 1])) {
                    sb.append(" ");
                }
            }

            sb.append(c);
        }

        return sb.toString();
    }

    private ExperimentElement generateExperimentElement(BAMData bamData) {
        ExperimentElement experimentElement = new ExperimentElement();
        QCReportParser.QCReportRecord qcReportData = bamData.getQcReportData();
        BAMHeaderParser.BAMHeaderData headerData = bamData.getHeaderData();
        List<BAMHeaderParser.BAMSoftwareData> pipelineSoftware = headerData.getSoftware();

        long readLength = Math.round(Double.valueOf(qcReportData.getReadOneLength()));
        long mateReadLength = Math.round(Double.valueOf(qcReportData.getReadTwoLength()));
        long meanInsertSize = Math.round(Double.valueOf(qcReportData.getInsertSize()));
        String instumentModel = parseInstrumentName(headerData);

        experimentElement.setTitle(freeFormData.getTitle().get() + " sample " + qcReportData.getLocalID());
        experimentElement.setCenterName(freeFormData.getCenterName().get());
        experimentElement.setAlias(qcReportData.getLibraryName());
//        experimentElement.setAlias(qcReportData.getLocalID());
        experimentElement.setDesignDescription(bamData.getQcReportData().getLibraryPrepDescription()
                + ": " + bamData.getQcReportData().getLibraryPrepProtocol());
        experimentElement.setInstrumentIllumina(instumentModel);

        experimentElement.setLibraryDescriptor(qcReportData.getLibraryName(),
                freeFormData.getLibraryStrategy().get(),
                freeFormData.getLibrarySource().get(),
                freeFormData.getLibrarySelection().get());
        experimentElement.setLibraryLayoutPaired(String.valueOf(meanInsertSize), Double.valueOf(qcReportData.getInsertSDev()));

        experimentElement.setSampleDescriptor(freeFormData.getAlias().get(), qcReportData.getLocalID());
        experimentElement.setStudyAccession(freeFormData.getAlias().get());

        experimentElement.setSpotLength(String.valueOf(readLength + mateReadLength));
        experimentElement.addRead(READ_TYPE, "Forward", "1");
        experimentElement.addRead(READ_TYPE, "Reverse", String.valueOf(readLength + 1));

        for (BAMHeaderParser.BAMSoftwareData software : pipelineSoftware) {
            experimentElement.addPipelineSection(software.getName(), software.getVersion());
        }

        return experimentElement;
    }

    private ExperimentModel generateExperimentModel() throws Exception {
        JAXBContext context = JAXBContext.newInstance(SRAConstants.EXPERIMENT_CONTEXT_PATH);
        ExperimentModel experimentModel = new ExperimentModel(context);

        for (BAMData bamData : bamDataValues) {
            experimentModel.addElement(generateExperimentElement(bamData));
        }

        return experimentModel;
    }

    private RunElement generateRunElement(BAMData bamData) {
        RunElement runElement = new RunElement();
        String refCenterName = freeFormData.getCenterName().get();
        runElement.setRunInfo(bamData.getQcReportData().getRunName(), refCenterName);
        runElement.setExperimentReference(bamData.getQcReportData().getLibraryName(), refCenterName);
        runElement.setReferenceGenome(freeFormData.getReferencGenome().get());

        runElement.addFile(bamData.getHeaderData().getFilename(),
                BAMHeaderParser.FILE_TYPE,
                bamData.getHeaderData().getChecksum(),
                BAMHeaderParser.getChecksumAlgorithmName());

        return runElement;
    }

    private RunModel generateRunModel() throws Exception {
        JAXBContext context = JAXBContext.newInstance(SRAConstants.RUN_CONTEXT_PATH);
        RunModel runModel = new RunModel(context);

        for (BAMData bAMData : bamDataValues) {
            runModel.addElement(generateRunElement(bAMData));
        }

        return runModel;
    }

    private void addSource(String schemaName) throws Exception {
        SubmissionElement.SchemaType schema = SubmissionElement.SchemaType.fromName(schemaName);

        if (schema != null) {
            submissionElement.addSourceAction(schemaName + ".xml", schema);

            switch (schema) {
                case EXPERIMENT:
                    sourceMap.put(schema, generateExperimentModel());
                    break;
                case RUN:
                    sourceMap.put(schema, generateRunModel());
                    break;
                default:
                    System.out.println("Unsupported source: " + schema.getName());
            }
        }
    }

    private void addActions(List<Action> actions) throws Exception {
        for (Action action : actions) {
            switch (action.getType()) {
                case SOURCE:
                    addSource(action.getFirstAttribute());
                    break;
                case PROTECT:
                    submissionElement.addProtectAction();
                    break;
                case MODIFY:
                    submissionElement.addModifyAction(action.getFirstAttribute());
                    break;
                case RELEASE:
                    if (action.getFirstAttribute().isEmpty()) {
                        submissionElement.addReleaseAction();
                    } else {
                        submissionElement.addReleaseAction(action.getFirstAttribute());
                    }
                    break;
                case SUPRESS:
                    submissionElement.addSupressAction(action.getFirstAttribute());
                    break;
                case VALIDATE:
                    submissionElement.addValidate(action.getFirstAttribute());
                    break;
                case HOLD:
                    if (action.getFirstAttribute().isEmpty()) {
                        submissionElement.addHoldAction();
                    } else if (action.getSecondAttribute().isEmpty()) {
                        submissionElement.addHoldAction(stringToXMLCalendar(action.getFirstAttribute()));
                    } else {
                        submissionElement.addHoldAction(action.getSecondAttribute(), stringToXMLCalendar(action.getFirstAttribute()));
                    }
                    break;
                default:
                    System.out.println("Unsupported action: " + action.getType().getName());
            }
        }
    }

    private XMLGregorianCalendar stringToXMLCalendar(String dateStr) throws Exception {
        SimpleDateFormat formatter = new SimpleDateFormat(Action.DATE_FORMAT);
        Date date = formatter.parse(dateStr);
        GregorianCalendar gregCal = new GregorianCalendar();

        gregCal.setTime(date);

        return DatatypeFactory.newInstance().newXMLGregorianCalendar(gregCal);
    }

    private void addContacts(List<Contact> contacts) {
        for (Contact contact : contacts) {
            submissionElement.addContact(contact.getName(),
                    contact.getInformOnStatus(),
                    contact.getInformOnError()
            );
        }
    }

    @Override
    public SubmissionModel generateSubmission() throws Exception {
        JAXBContext context;
        SubmissionModel submissionModel;

        try {
            context = JAXBContext.newInstance(SRAConstants.SUBMISSION_CONTEXT_PATH);
            submissionModel = new SubmissionModel(context);
        } catch (Exception ex) {
            updateObservers(SRADataGeneratorMessage.ERROR, "XML generation error");
            ex.printStackTrace();
            throw ex;
        }

//        if (bamDataValues.isEmpty()) {
//            updateObservers(SRADataGeneratorMessage.ERROR, "No BAM files found");
//            return null;
//        }
        submissionElement = new SubmissionElement();
        sourceMap.clear();

        submissionElement.setTitle(freeFormData.getTitle().get());
        submissionElement.setCenterName(freeFormData.getCenterName().get());
        submissionElement.setAlias(freeFormData.getAlias().get());

        addContacts(freeFormData.getContacts());

        try {
            addActions(freeFormData.getActions());
        } catch (Exception ex) {
            updateObservers(SRADataGeneratorMessage.ERROR, "Error adding actions");
            ex.printStackTrace();
            throw ex;
        }

        submissionModel.addElement(submissionElement);

        for (Map.Entry<SubmissionElement.SchemaType, Model> entry : sourceMap.entrySet()) {
            SubmissionElement.SchemaType schema = entry.getKey();
            Model model = entry.getValue();

            submissionModel.addChildModel(schema.getName(), model);
        }

        updateObservers(SRADataGeneratorMessage.FINISHED_PROCESSING, 0L);

        return submissionModel;
    }

    @Override
    public void updateObservers(SRADataGeneratorMessage key, Object value) {
        Entry<SRADataGeneratorMessage, Object> entry = new AbstractMap.SimpleEntry<>(key, value);

        setChanged();
        notifyObservers(entry);
    }

}
