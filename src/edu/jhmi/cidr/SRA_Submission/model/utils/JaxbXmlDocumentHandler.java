/*Copyright (c) 2015 Johns Hopkins Center for Inherited Disease Research

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.jhmi.cidr.SRA_Submission.model.utils;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.net.URL;
import java.util.List;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

/**
 *
 * @author asanch15
 */
public class JaxbXmlDocumentHandler<T> {

    private final JAXBContext context;

    public JaxbXmlDocumentHandler(JAXBContext context) {
        this.context = context;
    }

    /**
     * Casts the return object from the unmarshal method to the correct generic
     * when passing a URL to unmarshal a JAXBElement is returned but when
     * passing a string or file the generic is returned
     *
     * PROCEED AT YOUR OWN RISK PRETTY PLEASE DON'T CHANGE
     *
     * @param obj
     * @return
     * @throws ClassCastException
     */
    private T castDocumentObject(Object obj) throws ClassCastException {
        if (obj instanceof JAXBElement) {
            System.out.println("obj = " + ((JAXBElement) obj).getValue());
            return ((JAXBElement<T>) obj).getValue();
        } else {
            return (T) obj;
        }
    }

    public T parse(File file) throws Exception {
        Unmarshaller um = context.createUnmarshaller();
        Object documentRootNode;

        if (file == null) {
            throw new NullPointerException("file must not be null");
        }

        documentRootNode = um.unmarshal(file);

        return castDocumentObject(documentRootNode);
    }

    public T parse(URL url) throws Exception {
        Unmarshaller um = context.createUnmarshaller();
        Object documentRootNode;

        if (url == null) {
            throw new NullPointerException("url must not be null");
        }

        documentRootNode = um.unmarshal(url);

        return castDocumentObject(documentRootNode);
    }

    public T parse(String str) throws Exception {
        Unmarshaller um = context.createUnmarshaller();
        Object documentRootNode;

        if (str == null) {
            throw new NullPointerException("str must not be null");
        }

        documentRootNode = um.unmarshal(new ByteArrayInputStream(str.getBytes()));

        return castDocumentObject(documentRootNode);
    }

    private Node generateDOMNode(T element) throws Exception {
        DocumentBuilder db = DocumentBuilderFactory.newInstance().newDocumentBuilder();
        Document doc = db.newDocument();
        Marshaller m = context.createMarshaller();

        m.marshal(element, doc);

        return doc;
    }

    private void writeDOM(Document doc, File outFile) throws Exception {
        Transformer transformer = TransformerFactory.newInstance().newTransformer();
        Source source = new DOMSource(doc);
        Result result = new StreamResult(outFile);

        transformer.setOutputProperty(OutputKeys.INDENT, "yes");
        transformer.transform(source, result);
    }

    public void write(List<T> documentElements, String rootName, File outputFile) throws Exception {
        DocumentBuilder db = DocumentBuilderFactory.newInstance().newDocumentBuilder();
        Document doc = db.newDocument();
        Element rootElement = doc.createElement(rootName);

        if (documentElements == null) {
            throw new NullPointerException("documentElements must not be null");
        }

        for (T element : documentElements) {
            Node adoptedNode = doc.importNode(generateDOMNode(element).getFirstChild(), true);
            rootElement.appendChild(adoptedNode);
        }

        doc.appendChild(rootElement);

        writeDOM(doc, outputFile);
    }

    public void write(T documentRootNode, File outputFile) throws Exception {
        DocumentBuilder db = DocumentBuilderFactory.newInstance().newDocumentBuilder();
        Document doc = db.newDocument();
        Node rootElement = doc.importNode(generateDOMNode(documentRootNode).getFirstChild(), true);

        if (documentRootNode == null) {
            throw new NullPointerException("documentRootNode must not be null");
        }
        
        doc.appendChild(rootElement);
        writeDOM(doc, outputFile);
    }

//    public void write(T documentRootNode, File outputFile) throws Exception {
//        Marshaller m = context.createMarshaller();
//
//        if (documentRootNode == null) {
//            throw new NullPointerException("documentRootNode must not be null");
//        }
//        if (outputFile == null) {
//            throw new NullPointerException("outputFile must not be null");
//        }
//
//        m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
//        m.marshal(documentRootNode, outputFile);
//    }
}
