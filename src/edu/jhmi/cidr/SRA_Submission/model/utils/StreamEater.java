/*Copyright (c) 2015 Johns Hopkins Center for Inherited Disease Research

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/

package edu.jhmi.cidr.SRA_Submission.model.utils;


import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;

/**
 * This class was designed to overcome known issues with the built-in
 * Runtime.getRuntime().exec("command") Java feature. On some native platforms,
 * there is a very small buffer size for input and output streams. Failure to
 * promptly write to a Process's input, or to read from its output, can cause
 * the Process to hang, block, or even deadlock. The solution is to create two
 * Threads, one for a Process's input, and one for its output, and to continuously
 * feed or drain, respectively, the stdin or stdout of a process launched through
 * the method mentioned above. That's where this class comes in... it can be configured
 * to be used as both a "feeder" and "drainer" Thread.
 *
 * <pre>
 * Example Usage:
 *
 * String command = "ping localhost";
 *
 * Process p = Runtime.getRuntime().exec(command);
 *
 * StreamEater stdErrEater = new StreamEater(p.getErrorStream(), null, StreamEater.Type.STDERR, true, false);
 *
 * StreamEater stdOutEater = new StreamEater(p.getInputStream(), null, StreamEater.Type.STDOUT, true, false);
 *
 * stdErrEater.start();
 * stdOutEater.start();
 *
 * p.waitFor();
 *
 * </pre>
 * @author mbarn
 */
public class StreamEater extends Thread {

    private final InputStream in;
    private final OutputStream out;
    private final Type type;
    private final boolean printToConsole;
    private final boolean binaryOutput;

    /**
     * Construct your StreamEater with an {@link InputStream}, an {@link OutputStream}, and a
     * {@link Type}. The OutputStream is used to "redirect" the input from the
     * InputStream, should you need to do so, however the OutputStream is allowed to
     * be null. The boolean flag allows you to print process output both to
     * the specified OutuputStream and to standard out if set to true. Anything
     * printed to standard out will have the Type prepended. Also note that if you are
     * writing a binary file, the value of the <code>binaryOutput</code> parameter <em>must</em>
     *  be set to true.
     * @param in InputStream
     * @param out OutputStream
     * @param type Type
     * @param printToConsole boolean
     * @param binaryOutput boolean
     * @throws NullPointerException if the InputStream or the Type is null.
     */
    public StreamEater(InputStream in, OutputStream out, Type type, boolean printToConsole, boolean binaryOutput) {
        if (in == null) {
            throw new NullPointerException("InputStream must not be null.");
        }
        if (type == null) {
            throw new NullPointerException("Type must not be null.");
        }
        this.in = in;
        this.out = out;
        this.type = type;
        this.printToConsole = printToConsole;
        this.binaryOutput = binaryOutput;
    }

    private void runsWithBinary() throws IOException {
        BufferedInputStream bis = null;
        try {
            bis = new BufferedInputStream(in);
            int b = -1;
            while ((b = in.read()) != -1) {
                if (out != null) {
                    out.write((byte) b);
                    out.flush();
                }
            }
        } finally {
            if (bis != null) {
                try {
                    bis.close();
                } catch (IOException ignore) {
                    //ignore exception
                }
            }
        }
    }

    private void runsWithAscii() throws IOException {
        PrintWriter pw = null;
        if (out != null) {
            pw = new PrintWriter(out);
        }
        // NOTE: DO NOT CLOSE this BufferedReader
        BufferedReader br = new BufferedReader(new InputStreamReader(in));
        String line = null;
        while ((line = br.readLine()) != null) {
            if (printToConsole) {
                StringBuilder sb = new StringBuilder();
                sb.append(type.toString());
                sb.append(" > ");
                sb.append(line);
                System.out.println(sb.toString());
            }
            if (pw != null) {
                pw.println(line);
                pw.flush();
            }
        }
    }

    @Override
    public void run() {
        try {
            if (binaryOutput) {
                runsWithBinary();
            } else {
                runsWithAscii();
            }
        } catch (IOException ex) {
            StringBuilder sb = new StringBuilder();
            sb.append("Problem at StreamEater.run() with ");
            sb.append(binaryOutput ? "binary" : "ASCII");
            sb.append(" output");
            ex.printStackTrace();
        }
    }

    public static enum Type {

        STDOUT, STDERR;
    }
}