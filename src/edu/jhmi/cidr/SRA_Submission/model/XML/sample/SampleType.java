/*Copyright (c) 2015 Johns Hopkins Center for Inherited Disease Research

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/

//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.7 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2013.08.27 at 03:51:33 PM EDT 
//


package edu.jhmi.cidr.SRA_Submission.model.XML.sample;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 *  A Sample defines an isolate of sequenceable material upon which sequencing experiments can be based. The Sample
 *         object may be a surrogate for taxonomy accession or an anonymized individual identifier. Or, it may fully specify provenance and
 *         isolation method of the starting material. 
 * 
 * <p>Java class for SampleType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SampleType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="IDENTIFIERS" type="{}IdentifierType" minOccurs="0"/>
 *         &lt;element name="TITLE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SAMPLE_NAME">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;all>
 *                   &lt;element name="TAXON_ID" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *                   &lt;element name="SCIENTIFIC_NAME" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="COMMON_NAME" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="ANONYMIZED_NAME" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="INDIVIDUAL_NAME" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                 &lt;/all>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="DESCRIPTION" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SAMPLE_LINKS" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence maxOccurs="unbounded">
 *                   &lt;element name="SAMPLE_LINK" type="{}LinkType"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="SAMPLE_ATTRIBUTES" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence maxOccurs="unbounded">
 *                   &lt;element name="SAMPLE_ATTRIBUTE" type="{}AttributeType"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *       &lt;attGroup ref="{}NameGroup"/>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SampleType", propOrder = {
    "identifiers",
    "title",
    "samplename",
    "description",
    "samplelinks",
    "sampleattributes"
})
@XmlRootElement(name="SAMPLE")
public class SampleType {

    @XmlElement(name = "IDENTIFIERS")
    protected IdentifierType identifiers;
    @XmlElement(name = "TITLE")
    protected String title;
    @XmlElement(name = "SAMPLE_NAME", required = true)
    protected SampleType.SAMPLENAME samplename;
    @XmlElement(name = "DESCRIPTION")
    protected String description;
    @XmlElement(name = "SAMPLE_LINKS")
    protected SampleType.SAMPLELINKS samplelinks;
    @XmlElement(name = "SAMPLE_ATTRIBUTES")
    protected SampleType.SAMPLEATTRIBUTES sampleattributes;
    @XmlAttribute(name = "alias")
    protected String alias;
    @XmlAttribute(name = "center_name")
    protected String centerName;
    @XmlAttribute(name = "broker_name")
    protected String brokerName;
    @XmlAttribute(name = "accession")
    protected String accession;

    /**
     * Gets the value of the identifiers property.
     * 
     * @return
     *     possible object is
     *     {@link IdentifierType }
     *     
     */
    public IdentifierType getIDENTIFIERS() {
        return identifiers;
    }

    /**
     * Sets the value of the identifiers property.
     * 
     * @param value
     *     allowed object is
     *     {@link IdentifierType }
     *     
     */
    public void setIDENTIFIERS(IdentifierType value) {
        this.identifiers = value;
    }

    /**
     * Gets the value of the title property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTITLE() {
        return title;
    }

    /**
     * Sets the value of the title property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTITLE(String value) {
        this.title = value;
    }

    /**
     * Gets the value of the samplename property.
     * 
     * @return
     *     possible object is
     *     {@link SampleType.SAMPLENAME }
     *     
     */
    public SampleType.SAMPLENAME getSAMPLENAME() {
        return samplename;
    }

    /**
     * Sets the value of the samplename property.
     * 
     * @param value
     *     allowed object is
     *     {@link SampleType.SAMPLENAME }
     *     
     */
    public void setSAMPLENAME(SampleType.SAMPLENAME value) {
        this.samplename = value;
    }

    /**
     * Gets the value of the description property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDESCRIPTION() {
        return description;
    }

    /**
     * Sets the value of the description property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDESCRIPTION(String value) {
        this.description = value;
    }

    /**
     * Gets the value of the samplelinks property.
     * 
     * @return
     *     possible object is
     *     {@link SampleType.SAMPLELINKS }
     *     
     */
    public SampleType.SAMPLELINKS getSAMPLELINKS() {
        return samplelinks;
    }

    /**
     * Sets the value of the samplelinks property.
     * 
     * @param value
     *     allowed object is
     *     {@link SampleType.SAMPLELINKS }
     *     
     */
    public void setSAMPLELINKS(SampleType.SAMPLELINKS value) {
        this.samplelinks = value;
    }

    /**
     * Gets the value of the sampleattributes property.
     * 
     * @return
     *     possible object is
     *     {@link SampleType.SAMPLEATTRIBUTES }
     *     
     */
    public SampleType.SAMPLEATTRIBUTES getSAMPLEATTRIBUTES() {
        return sampleattributes;
    }

    /**
     * Sets the value of the sampleattributes property.
     * 
     * @param value
     *     allowed object is
     *     {@link SampleType.SAMPLEATTRIBUTES }
     *     
     */
    public void setSAMPLEATTRIBUTES(SampleType.SAMPLEATTRIBUTES value) {
        this.sampleattributes = value;
    }

    /**
     * Gets the value of the alias property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAlias() {
        return alias;
    }

    /**
     * Sets the value of the alias property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAlias(String value) {
        this.alias = value;
    }

    /**
     * Gets the value of the centerName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCenterName() {
        return centerName;
    }

    /**
     * Sets the value of the centerName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCenterName(String value) {
        this.centerName = value;
    }

    /**
     * Gets the value of the brokerName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBrokerName() {
        return brokerName;
    }

    /**
     * Sets the value of the brokerName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBrokerName(String value) {
        this.brokerName = value;
    }

    /**
     * Gets the value of the accession property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAccession() {
        return accession;
    }

    /**
     * Sets the value of the accession property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAccession(String value) {
        this.accession = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence maxOccurs="unbounded">
     *         &lt;element name="SAMPLE_ATTRIBUTE" type="{}AttributeType"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "sampleattribute"
    })
    public static class SAMPLEATTRIBUTES {

        @XmlElement(name = "SAMPLE_ATTRIBUTE", required = true)
        protected List<AttributeType> sampleattribute;

        /**
         * Gets the value of the sampleattribute property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the sampleattribute property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getSAMPLEATTRIBUTE().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link AttributeType }
         * 
         * 
         */
        public List<AttributeType> getSAMPLEATTRIBUTE() {
            if (sampleattribute == null) {
                sampleattribute = new ArrayList<AttributeType>();
            }
            return this.sampleattribute;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence maxOccurs="unbounded">
     *         &lt;element name="SAMPLE_LINK" type="{}LinkType"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "samplelink"
    })
    public static class SAMPLELINKS {

        @XmlElement(name = "SAMPLE_LINK", required = true)
        protected List<LinkType> samplelink;

        /**
         * Gets the value of the samplelink property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the samplelink property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getSAMPLELINK().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link LinkType }
         * 
         * 
         */
        public List<LinkType> getSAMPLELINK() {
            if (samplelink == null) {
                samplelink = new ArrayList<LinkType>();
            }
            return this.samplelink;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;all>
     *         &lt;element name="TAXON_ID" type="{http://www.w3.org/2001/XMLSchema}int"/>
     *         &lt;element name="SCIENTIFIC_NAME" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="COMMON_NAME" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="ANONYMIZED_NAME" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="INDIVIDUAL_NAME" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *       &lt;/all>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {

    })
    public static class SAMPLENAME {

        @XmlElement(name = "TAXON_ID")
        protected int taxonid;
        @XmlElement(name = "SCIENTIFIC_NAME")
        protected String scientificname;
        @XmlElement(name = "COMMON_NAME")
        protected String commonname;
        @XmlElement(name = "ANONYMIZED_NAME")
        protected String anonymizedname;
        @XmlElement(name = "INDIVIDUAL_NAME")
        protected String individualname;

        /**
         * Gets the value of the taxonid property.
         * 
         */
        public int getTAXONID() {
            return taxonid;
        }

        /**
         * Sets the value of the taxonid property.
         * 
         */
        public void setTAXONID(int value) {
            this.taxonid = value;
        }

        /**
         * Gets the value of the scientificname property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getSCIENTIFICNAME() {
            return scientificname;
        }

        /**
         * Sets the value of the scientificname property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setSCIENTIFICNAME(String value) {
            this.scientificname = value;
        }

        /**
         * Gets the value of the commonname property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCOMMONNAME() {
            return commonname;
        }

        /**
         * Sets the value of the commonname property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCOMMONNAME(String value) {
            this.commonname = value;
        }

        /**
         * Gets the value of the anonymizedname property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getANONYMIZEDNAME() {
            return anonymizedname;
        }

        /**
         * Sets the value of the anonymizedname property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setANONYMIZEDNAME(String value) {
            this.anonymizedname = value;
        }

        /**
         * Gets the value of the individualname property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getINDIVIDUALNAME() {
            return individualname;
        }

        /**
         * Sets the value of the individualname property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setINDIVIDUALNAME(String value) {
            this.individualname = value;
        }

    }

}
