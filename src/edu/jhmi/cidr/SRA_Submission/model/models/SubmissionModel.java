/*Copyright (c) 2015 Johns Hopkins Center for Inherited Disease Research

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.jhmi.cidr.SRA_Submission.model.models;

import edu.jhmi.cidr.SRA_Submission.model.XML.submission.SubmissionType;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.xml.bind.JAXBContext;

/**
 *
 * @author asanch15
 */
public class SubmissionModel extends Model<SubmissionType> {
    private static final String FILE_NAME = "submission";
    private final Map<String, Model> childModels;
    private final List<File> generatedFiles = new ArrayList<>();
    
    public SubmissionModel(JAXBContext context) {
        super(context, SubmissionType.class, "SUBMISSION_SET");
        this.childModels = new HashMap<>();
    }
    
    private String appendXmlExtension(String filename){
        return filename + ".xml";
    }
    
    private void writeChildModelsToDisk(File outputDir) throws Exception{
        for(Map.Entry<String, Model> en : childModels.entrySet()){
            File outputFile = new File(outputDir, appendXmlExtension(en.getKey()));
            
            en.getValue().writeToDisk(outputFile);
            generatedFiles.add(outputFile);
        }
    }
    
    public void addChildModel(String name, Model model){
        childModels.put(name, model);
    }
    
    public void removeChildModel(String name){
        childModels.remove(name);
    }
    
    @Override
    public void writeToDisk(File outputDir) throws Exception {
        File outputFile = new File(outputDir, appendXmlExtension(FILE_NAME));
        
        getDocumentHandler().write(this.elements.get(0), outputFile);
        generatedFiles.add(outputFile);
        writeChildModelsToDisk(outputDir);
    }

    public List<File> getGeneratedFiles() {
        return generatedFiles;
    }
    
}
