/*Copyright (c) 2015 Johns Hopkins Center for Inherited Disease Research

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.jhmi.cidr.SRA_Submission.model.models;

import edu.jhmi.cidr.SRA_Submission.model.models.elements.Element;
import edu.jhmi.cidr.SRA_Submission.model.utils.JaxbXmlDocumentHandler;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import javax.xml.bind.JAXBContext;

/**
 *
 * @author asanch15
 * @param <T>
 */
public class Model<T> extends Observable {

    private final Class<T> elementClass;
    private final String rootName;
    protected final List<T> elements;
    private final JaxbXmlDocumentHandler<T> documentHandler;

    public Model(JAXBContext context, Class<T> elementClass, String rootName) {
        this.elements = new ArrayList<>();
        this.elementClass = elementClass;
        this.documentHandler = new JaxbXmlDocumentHandler<>(context);
        this.rootName = rootName;
    }

    public void addElement(Element<T> element) {
        elements.add(element.getElement());
    }

    public int numNodes() {
        return elements.size();
    }

    public Element<T> createElement() {
        T element = null;

        try {
            element = elementClass.newInstance();
        } catch (Exception e) {
            System.out.println("Failed to create a new element");
            e.printStackTrace();
        }

        return new Element<T>(element);
    }

    protected JaxbXmlDocumentHandler<T> getDocumentHandler() {
        return documentHandler;
    }

    public void writeToDisk(File file) throws Exception {
        documentHandler.write(elements, rootName, file);
    }
}
