/*Copyright (c) 2015 Johns Hopkins Center for Inherited Disease Research

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.jhmi.cidr.SRA_Submission.model.models;

import com.google.common.base.Joiner;
import com.google.common.io.Files;
import edu.jhmi.cidr.SRA_Submission.model.ModelMessage;
import edu.jhmi.cidr.SRA_Submission.model.generator.SubmissionGenerator;
import edu.jhmi.cidr.SRA_Submission.model.models.elements.SubmissionElement;
import edu.jhmi.cidr.SRA_Submission.model.objects.BAMData;
import edu.jhmi.cidr.SRA_Submission.objects.Action;
import edu.jhmi.cidr.SRA_Submission.objects.Contact;
import edu.jhmi.cidr.SRA_Submission.objects.FileError;
import edu.jhmi.cidr.SRA_Submission.objects.LibraryProtocol;
import edu.jhmi.cidr.SRA_Submission.objects.SRAFreeFormData;
import edu.jhmi.cidr.SRA_Submission.util.parsers.MD5ChecksumFileParser;
import edu.jhmi.cidr.SRA_Submission.util.parsers.BAMHeaderParser;
import edu.jhmi.cidr.SRA_Submission.util.parsers.QCReportParser;
import edu.jhmi.cidr.lib.conveniencestore.fileutils.TarFileCreator;
import java.io.File;
import java.io.FilenameFilter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Observable;
import java.util.Set;

/**
 *
 * @author Alice Sanchez
 */
public class SRASubmissionModel extends Observable {

    private static final FilenameFilter BAM_FILE_FILTER = (File dir, String name) -> Files.getFileExtension(name).equalsIgnoreCase("bam");

    private final SRAFreeFormData sraFreeFormData = new SRAFreeFormData();
    private Map<String, QCReportParser.QCReportRecord> qcReportRecordsMap;
    private Map<String, BAMHeaderParser.BAMHeaderData> bamHeaderRecordsMap;
    private Map<String, LibraryProtocol> projectLibraryMap;
    private LibraryProtocol defaultLibraryProtocol;
//    private Map<String, BAMHeaderParser.BAMHeaderData> bamFileHeaderRecords;
    private Map<String, String> checksumMap;
//    private Optional<BAMChecksumFileParser> checksumParser = Optional.absent();
    private List<File> qcReportFiles;
    private List<File> bamFileFolders;
    private List<BAMData> bamDataList;
    private final List<LibraryProtocol> libraryProtocols = new ArrayList<>();
    private File checksumFile;
    private File workingDirectory;

    public static void main(String[] args) throws Exception {
        SRAFreeFormData data = new SRAFreeFormData();
        SRASubmissionModel model = new SRASubmissionModel();
        Action addExperimentAction = new Action(Action.ActionType.SOURCE);
        Action addRunAction = new Action(Action.ActionType.SOURCE);
        Contact contact = new Contact("Test", "test@email.com", "test@email.com");
//        List<String> errors;

        data.getAlias().set("phs000350");
        data.getCenterName().set("CIDR");
        data.getLibraryProtocol().set("Agilent_SureSelect_All_Exon_50Mb_v3");
        data.getLibraryLayout().set("Paired");
        data.getDesignDescription().set("3ug of gDNA sheared to 150-200bp using Covaris E210; libraries were generated using SPRIworks Fragment Library System and reagents; whole exome enrichment using Agilent SureSelect 50Mb Human All exon capture process and reagents.");
        data.getLibrarySelection().set("Hybrid Selection");
        data.getLibrarySource().set("GENOMIC");
        data.getLibraryStrategy().set("WXS");
//        data.getPipelineName().set("bwa");
//        data.getPipelineVersion().set("0.5.9-r16");
        data.getReferencGenome().set("GRCh37");
        data.getTitle().set("Super Awesome Test of SRA Auto Submission :D");
        data.getSampleIDColumn().set("Local_Id");
        data.getRunGroupIDColumn().set("RG_ID");

        addExperimentAction.firstAttributeProperty().set(SubmissionElement.SchemaType.EXPERIMENT.getName());
        data.getActions().add(addExperimentAction);

        data.getActions().add(new Action(Action.ActionType.PROTECT));

        addRunAction.firstAttributeProperty().set(SubmissionElement.SchemaType.RUN.getName());
        data.getActions().add(addRunAction);

        data.getContacts().add(contact);

        data.getOutputFolderPath().set("O:\\asanch15\\SRA_Test");

        model.setQCReportFiles(
                Arrays.asList(new File("QCreport.csv")));
        model.setChecksumFile(new File("checksum.md5"));
        model.setBAMFolders(
                Arrays.asList(
                        new File("BAM_Folder")));
        SubmissionReport report = model.generateSubmission(data, true);

        if (report.getWarnings().isEmpty() == false) {
            System.out.println("Warnings: ");
            for (String warnings : report.getWarnings()) {
                System.out.println("\t" + warnings);
            }
        } else {
            System.out.println("No Warnings");
        }

        if (report.getErrors().isEmpty() == false) {
            System.out.println("Errors: ");
            for (String error : report.getErrors()) {
                System.out.println("\t" + error);
            }
        } else {
            System.out.println("No Errors");
        }

    }

    public void init() throws Exception {
        DefaultPropertiesModel propertiesModel = new DefaultPropertiesModel(new File("defaults.props"));
        DefaultPropertiesModel.SRADefaultProperties properties;
        Action experiment = new Action(Action.ActionType.SOURCE, false);
        Action protect = new Action(Action.ActionType.PROTECT, false);
        Action run = new Action(Action.ActionType.SOURCE, false);

        propertiesModel.init();
        properties = propertiesModel.getDefaultProperties();
        sraFreeFormData.getCenterName().set(properties.getCenterName());
        sraFreeFormData.getLibraryStrategy().set(properties.getLibraryStrategy());
        sraFreeFormData.getLibrarySelection().set(properties.getLibrarySelection());
        sraFreeFormData.getLibrarySource().set(properties.getLibrarySource());
        sraFreeFormData.getLibraryLayout().set(properties.getLibraryLayout());
        sraFreeFormData.getReferencGenome().set(properties.getRefGenome());

        experiment.firstAttributeProperty().set(SubmissionElement.SchemaType.EXPERIMENT.getName());
        run.firstAttributeProperty().set(SubmissionElement.SchemaType.RUN.getName());
        sraFreeFormData.getActions().addAll(experiment, run, protect);
        
        libraryProtocols.addAll(properties.getLibraryProtocols());
    }

    public SRAFreeFormData getSraFreeFormData() {
        return sraFreeFormData;
    }

    public List<LibraryProtocol> getLibraryProtocols() {
        return libraryProtocols;
    }
    
    public List<FileError> processMasterDataFile() {
        List<FileError> errors = new ArrayList<>();

        return errors;
    }

    public void setWorkingDirectory(File workingDirectory) {
        this.workingDirectory = workingDirectory;
    }

    public File getWorkingDirectory() {
        return workingDirectory;
    }

    public void setQCReportFiles(List<File> qcReports) {
        qcReportFiles = new ArrayList<>(qcReports);
    }

    public void setQCReportRecords(Collection<QCReportParser.QCReportRecord> records) {
        qcReportRecordsMap = new HashMap<>();

        for (QCReportParser.QCReportRecord qcReportRecord : records) {
            qcReportRecordsMap.put(qcReportRecord.getExperimentName(), qcReportRecord);
        }
    }

    public void setBamHeaderRecords(Collection<BAMHeaderParser.BAMHeaderData> bamHeaderRecords) {
        bamHeaderRecordsMap = new HashMap<>();

        for (BAMHeaderParser.BAMHeaderData bamHeaderData : bamHeaderRecords) {
            bamHeaderRecordsMap.put(bamHeaderData.getRuns().get(0).getExperimentName(), bamHeaderData);
        }
    }

    public Set<String> getQCReportExperimentalNames() {
        if (qcReportRecordsMap == null) {
            return Collections.emptySet();
        }

        return new HashSet<>(qcReportRecordsMap.keySet());
    }

    public List<BAMData> getCombinedQCBAMData() {
        List<BAMData> combinedBamDataList = new ArrayList<>();

        for (Map.Entry<String, QCReportParser.QCReportRecord> entry : qcReportRecordsMap.entrySet()) {
            String experimentName = entry.getKey();
            QCReportParser.QCReportRecord qcReportRecord = entry.getValue();
            BAMHeaderParser.BAMHeaderData bamHeaderData = bamHeaderRecordsMap.get(experimentName);

            if (bamHeaderData != null) {
                combinedBamDataList.add(new BAMData(bamHeaderData, qcReportRecord));
            }
        }

        bamDataList = new ArrayList<>(combinedBamDataList);
        return combinedBamDataList;
    }

    private List<String> parseQCReportFiles(String sampleIDColumn, String runGroupColumn, String experimentIDColumn) {
        List<String> errors = new ArrayList<>();

        qcReportRecordsMap = new HashMap<>();

        for (File qcReport : qcReportFiles) {
            QCReportParser parser = new QCReportParser(qcReport, sampleIDColumn, runGroupColumn, experimentIDColumn);
            List<QCReportParser.QCReportRecord> records = parser.parse();

            if (parser.getErrors().isEmpty()) {
                for (QCReportParser.QCReportRecord record : records) {
                    qcReportRecordsMap.put(record.getExperimentName(), record);
                }
            } else {
                errors.addAll(parser.getErrors());
            }

        }

        return errors;
    }

    public void setChecksumFile(File checksumFile) {
        this.checksumFile = checksumFile;
    }

    private List<String> parseChecksumFile() {
        List<String> errors = new ArrayList<>();
        MD5ChecksumFileParser parser = new MD5ChecksumFileParser(checksumFile);

        try {
            parser.parse();
            checksumMap = parser.getChecksumMap();
        } catch (Exception ex) {
            errors.add("Error parsing checksum file: " + checksumFile.getAbsolutePath());
        }

        return errors;
    }

//    private String getExperimentNameFromBAMFileName(String filename) {
//        return filename.split("\\.")[0];
//    }
    public void setBAMFolders(List<File> bamFolders) {
        this.bamFileFolders = new ArrayList<>(bamFolders);
    }

    private QCReportParser.QCReportRecord getQCReportRecord(String experimentName) {
        for (String recordName : qcReportRecordsMap.keySet()) {
            if (experimentName.startsWith(recordName)) {
                return qcReportRecordsMap.get(recordName);
            }
        }

        return null;
    }

    public List<String> parseBAMFolders() {
        List<String> errors = new ArrayList<>();
        bamDataList = new ArrayList<>();
//        bamFileHeaderRecords = new HashMap<>();

        for (File bamFolder : bamFileFolders) {

            if (bamFolder.isDirectory()) {
                File[] bamFiles = bamFolder.listFiles(BAM_FILE_FILTER);

                if (bamFiles.length != 0) {
                    for (File bamFile : bamFiles) {
//                        String experimentName = getExperimentNameFromBAMFileName(bamFile.getName());
                        String experimentName = Files.getNameWithoutExtension(bamFile.getName());
                        QCReportParser.QCReportRecord qcRecord = getQCReportRecord(experimentName);

                        if (qcRecord != null) {
                            BAMHeaderParser parser = new BAMHeaderParser(bamFile);
                            BAMHeaderParser.BAMHeaderData data;

//                            if (checksumMap != null) {
//                                parser.setChecksum(checksumMap.get(bamFile.getName()));
//                            }
                            try {
                                data = parser.parse();

                                if (parser.getErrors().isEmpty()) {
//                                    bamFileHeaderRecords.put(experimentName, data);
                                    bamDataList.add(new BAMData(data, qcRecord));
                                } else {
                                    errors.addAll(parser.getErrors());
                                }
                            } catch (Exception ex) {
                                ex.printStackTrace();
                                errors.add("Error parsing file: " + bamFile);
                            }
                        }
                    }
                } else {
                    errors.add(bamFolder.getAbsolutePath() + " contains no BAM files");
                }
            } else {
                errors.add(bamFolder.getAbsolutePath() + " is not a folder");
            }
        }

        if (bamDataList.isEmpty()) {
            errors.add("No BAM files found based on QCReport Data");
        }

        return errors;
    }

    public void setBamDataList(List<BAMData> bamDataList) {
        this.bamDataList = bamDataList;
    }

    public void generateSubmission(File outputFile) throws Exception {
        if(bamDataList == null){
            bamDataList = getCombinedQCBAMData();
        }
        
        if (bamDataList.isEmpty() == false) {
            System.out.println("Calculating Checksums");
            for (BAMData bamData : bamDataList) {
                if (bamData.getHeaderData().getChecksum() == null || bamData.getHeaderData().getChecksum().isEmpty()) {
                    bamData.getHeaderData().setChecksum(BAMHeaderParser.getFileChecksumValue(bamData.getHeaderData().getFile()));
                }
            }

            SubmissionGenerator sraDataGenerator = new SubmissionGenerator(sraFreeFormData, bamDataList);
            SubmissionModel submissionModel = sraDataGenerator.generateSubmission();
            File tempDir = Files.createTempDir();
            List<File> generatedFiles;

            System.out.println("Writing Submission Files");
            submissionModel.writeToDisk(tempDir);

            System.out.println("Placing Files into a Tar Ball");
            generatedFiles = submissionModel.getGeneratedFiles();
//            new TarFileCreator(generatedFiles).writeTarFile(new File(outputDir, outputFileName + ".tar"));
            
            if(Files.getFileExtension(outputFile.getName()).equalsIgnoreCase("tar") == false){
                outputFile = new File(outputFile.getParentFile(), outputFile.getName() + ".tar");
            }
            
            new TarFileCreator(generatedFiles).writeTarFile(outputFile);

            for (File file : generatedFiles) {
                file.delete();
            }
        }
    }

    public SubmissionReport generateSubmission(SRAFreeFormData freeFormData, boolean runWithWarnings) throws Exception {
        List<String> errors = new ArrayList<>();
        List<String> warnings = new ArrayList<>();
        boolean success = false;

        System.out.println("Parsing QC Reports");
        errors.addAll(parseQCReportFiles(freeFormData.getSampleIDColumn().get().trim(), freeFormData.getRunGroupIDColumn().get().trim(), ""));

        if (checksumFile != null) {
            System.out.println("Parsing Checksum File");
            errors.addAll(parseChecksumFile());
        }

        if (errors.isEmpty()) {
            System.out.println("Parsing BAM Folders");
            errors.addAll(parseBAMFolders());

            if (errors.isEmpty()) {
                if (checksumMap != null) {
                    List<String> missingChecksums = new ArrayList<>();

                    for (BAMData bamData : bamDataList) {
                        File bamFile = bamData.getHeaderData().getFile();
                        String checksum = checksumMap.get(bamFile.getName());

                        if (checksum == null) {
                            if (runWithWarnings) {
                                bamData.getHeaderData().setChecksum(BAMHeaderParser.getFileChecksumValue(bamFile));
                            } else {
                                missingChecksums.add(bamFile.getName());
                            }
                        } else {
                            bamData.getHeaderData().setChecksum(checksum);
                        }
                    }

                    if (missingChecksums.isEmpty() == false) {
                        warnings.add("Missing Checksums for Files: " + Joiner.on(", ").join(missingChecksums));
                    }
                } else {
                    for (BAMData bamData : bamDataList) {
                        bamData.getHeaderData().setChecksum(BAMHeaderParser.getFileChecksumValue(bamData.getHeaderData().getFile()));
                    }
                }
            }
        }

        if (errors.isEmpty() && (warnings.isEmpty() || runWithWarnings)) {
            if (bamDataList.isEmpty() == false) {
                SubmissionGenerator sraDataGenerator = new SubmissionGenerator(freeFormData, bamDataList);
                SubmissionModel submissionModel = sraDataGenerator.generateSubmission();
                File outputDir = new File(freeFormData.getOutputFolderPath().get());
                File tempDir = Files.createTempDir();
                List<File> generatedFiles;

                System.out.println("Writing Submission Files");
                submissionModel.writeToDisk(tempDir);

                System.out.println("Placing Files into a Tar Ball");
                generatedFiles = submissionModel.getGeneratedFiles();
                new TarFileCreator(generatedFiles).writeTarFile(new File(outputDir, "submission_" + freeFormData.getOutputFileName().getValue() + ".tar"));

                for (File file : generatedFiles) {
                    file.delete();
                }

                success = true;

            } else {
                errors.add("No matches found between BAM files and QC reports");
            }
        }

        return new SubmissionReport(success, errors, warnings);
    }

    public void setDefaultLibraryProtocol(LibraryProtocol defaultLibraryProtocol) {
        this.defaultLibraryProtocol = defaultLibraryProtocol;
        this.projectLibraryMap = null;
    }

    public void setProjectLibraryMap(Map<String, LibraryProtocol> projectLibraryMap) {
        this.projectLibraryMap = new HashMap<>(projectLibraryMap);
        this.defaultLibraryProtocol = null;
    }

    public LibraryProtocol getLibraryProtocolForProject(String projectName) {
        if (defaultLibraryProtocol != null) {
            return defaultLibraryProtocol;
        } else if (projectLibraryMap != null) {
            return projectLibraryMap.get(projectName);
        }

        return null;
    }

    public class SubmissionReport {

        private final boolean successful;
        private final List<String> errors;
        private final List<String> warnings;

        public SubmissionReport(boolean successful, List<String> errors, List<String> warnings) {
            this.successful = successful;
            this.errors = errors;
            this.warnings = warnings;
        }

        public boolean isSuccessful() {
            return successful;
        }

        public List<String> getErrors() {
            return errors;
        }

        public List<String> getWarnings() {
            return warnings;
        }

    }

    private void updateObservers(ModelMessage msg) {
        setChanged();
        notifyObservers(msg);
    }

}
