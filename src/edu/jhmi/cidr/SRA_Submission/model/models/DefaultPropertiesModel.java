/*Copyright (c) 2015 Johns Hopkins Center for Inherited Disease Research

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.jhmi.cidr.SRA_Submission.model.models;

import edu.jhmi.cidr.SRA_Submission.model.ModelMessage;
import edu.jhmi.cidr.SRA_Submission.objects.LibraryProtocol;
import edu.jhmi.cidr.SRA_Submission.util.parsers.LibraryProtocolFileParser;
import static edu.jhmi.cidr.lib.Constants.NEW_LINE;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Observable;
import java.util.Properties;
import java.util.Set;

/**
 *
 * @author asanch15
 */
public class DefaultPropertiesModel extends Observable {

    private static final File LIB_PREP_FILE = new File("");
    
    private final List<File> propertyFiles;
    private final Set<PropsFileKey> missingProperties = new HashSet<>();
    private final SRADefaultProperties defaultProperties = new SRADefaultProperties();

    public static void main(String[] args) throws Exception {
        DefaultPropertiesModel propsModel = new DefaultPropertiesModel(new File("defaults.props"));
        
        propsModel.init();
        
        System.out.println("Missing: " + propsModel.getMissingProperties());
        System.out.println(propsModel.getDefaultProperties());
    }
    
    public DefaultPropertiesModel(File... propertyFiles) {
        this.propertyFiles = Arrays.asList(propertyFiles);

        for (PropsFileKey key : PropsFileKey.values()) {
            missingProperties.add(key);
        }
    }

    public void init() throws Exception {
        for (File propFile : propertyFiles) {
            parsePropFile(propFile);
        }

//        querySeqTraq();
        parseLibPrepFile();
        updateObservers(ModelMessage.FINSHED_PROCESSING_DEFAULT_PROPERTIES);
    }

    private void parsePropFile(File propFile) {
        Properties props = new Properties();

        try {
            props.load(new FileReader(propFile));

            for (PropsFileKey key : PropsFileKey.values()) {
                if (props.containsKey(key.getName())) {
                    defaultProperties.setValue(key, props.getProperty(key.getName()));
                    missingProperties.remove(key);
                }
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
    
    private void parseLibPrepFile() throws Exception{
        LibraryProtocolFileParser parser = new LibraryProtocolFileParser(LIB_PREP_FILE);
        
        for (LibraryProtocol libraryProtocol : parser.parse()) {
            defaultProperties.libraryProtocols.add(libraryProtocol);
        }
    }

//    private void querySeqTraq() {
//        DesignDescriptionGoldenRetriever descriptionGoldenRetriever = new DesignDescriptionGoldenRetriever();
//
//        try {
//            descriptionGoldenRetriever.emilioAestivate();
//        } catch (Exception ex) {
//            System.out.println("Error querying SeqTraq");
//            ex.printStackTrace();
//        }
//
//        for (DesignDescription designDescription : descriptionGoldenRetriever.getResults()) {
//            defaultProperties.addLibraryProtocol(
//                    new LibraryProtocol(designDescription.getLibrary_protocol(),
//                            designDescription.getDescription()));
//        }
//    }

    public Set<PropsFileKey> getMissingProperties() {
        return missingProperties;
    }

    private void updateObservers(ModelMessage msg) {
        setChanged();
        notifyObservers(msg);
    }

    public SRADefaultProperties getDefaultProperties() {
        return defaultProperties;
    }

    public class SRADefaultProperties {

        private String centerName;
        private String libraryStrategy;
        private String librarySource;
        private String libraryLayout;
        private String librarySelection;
        private final List<LibraryProtocol> libraryProtocols = new ArrayList<>();
        private String pipelineName;
        private String pipelineVersion;
        private String readType;
        private String refGenome;
        private String fileType;

        public SRADefaultProperties() {
        }

        void setValue(PropsFileKey key, String value) {
            switch (key) {
                case CENTER_NAME:
                    centerName = value;
                    break;
                case FILE_TYPE:
                    fileType = value;
                    break;
                case LIBRARY_LAYOUT:
                    libraryLayout = value;
                    break;
                case LIBRARY_SELECTION:
                    librarySelection = value;
                    break;
                case LIBRARY_SOURCE:
                    librarySource = value;
                    break;
                case LIBRARY_STRATEGY:
                    libraryStrategy = value;
                    break;
                case PIPELINE_PROGRAM:
                    pipelineName = value;
                    break;
                case PIPELINE_VERSION:
                    pipelineVersion = value;
                    break;
                case READ_TYPE:
                    readType = value;
                    break;
                case REF_GENOME:
                    refGenome = value;
                    break;
                default:
                    System.out.println("Unknown Props Key: " + key);
            }
        }

        void addLibraryProtocol(LibraryProtocol protocol) {
            libraryProtocols.add(protocol);
        }

        /**
         * @return the centerName
         */
        public String getCenterName() {
            return centerName;
        }

        /**
         * @return the libraryStrategy
         */
        public String getLibraryStrategy() {
            return libraryStrategy;
        }

        /**
         * @return the librarySource
         */
        public String getLibrarySource() {
            return librarySource;
        }

        /**
         * @return the libraryLayout
         */
        public String getLibraryLayout() {
            return libraryLayout;
        }

        /**
         * @return the librarySelection
         */
        public String getLibrarySelection() {
            return librarySelection;
        }

        /**
         * @return the libraryProtocols
         */
        public List<LibraryProtocol> getLibraryProtocols() {
            return libraryProtocols;
        }

        /**
         * @return the pipelineName
         */
        public String getPipelineName() {
            return pipelineName;
        }

        /**
         * @return the pipelineVersion
         */
        public String getPipelineVersion() {
            return pipelineVersion;
        }

        /**
         * @return the readType
         */
        public String getReadType() {
            return readType;
        }

        /**
         * @return the refGenome
         */
        public String getRefGenome() {
            return refGenome;
        }

        /**
         * @return the fileType
         */
        public String getFileType() {
            return fileType;
        }

        @Override
        public String toString() {
            StringBuilder sb = new StringBuilder();

            sb.append("Center Name: ").append(centerName).append(NEW_LINE)
                    .append("Library Strategy: ").append(libraryStrategy).append(NEW_LINE)
                    .append("Library Source: ").append(librarySource).append(NEW_LINE)
                    .append("Library Selection: ").append(librarySelection).append(NEW_LINE)
                    .append("Library Layout: ").append(libraryLayout).append(NEW_LINE)
                    .append("Pipeline Name: ").append(pipelineName).append(" Version: ").append(pipelineVersion).append(NEW_LINE)
                    .append("File Type: ").append(fileType).append(NEW_LINE)
                    .append("Reference Genome: ").append(refGenome).append(NEW_LINE)
                    .append("Library Protocols: ").append(NEW_LINE);
            
            for (LibraryProtocol protocol : libraryProtocols) {
                sb.append(protocol).append(NEW_LINE);
            }

            return sb.toString();
        }

    }

    public enum PropsFileKey {

        CENTER_NAME("center_name"),
        LIBRARY_STRATEGY("library_strategy"),
        LIBRARY_SOURCE("library_source"),
        LIBRARY_SELECTION("library_selection"),
        LIBRARY_LAYOUT("library_layout"),
        PIPELINE_PROGRAM("pipeline_program"),
        PIPELINE_VERSION("pipeline_version"),
        FILE_TYPE("file_type"),
        READ_TYPE("read_type"),
        REF_GENOME("ref_genome");

        private final String name;

        private PropsFileKey(String name) {
            this.name = name;
        }

        public String getName() {
            return name;
        }

    }
}
