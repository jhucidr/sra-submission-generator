/*Copyright (c) 2015 Johns Hopkins Center for Inherited Disease Research

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.jhmi.cidr.SRA_Submission.model.models;

import edu.jhmi.cidr.SRA_Submission.util.SRAConstants;
import javax.xml.bind.JAXBContext;

/**
 *
 * @author asanch15
 */
public class ModelFactory {

    public static final int SUBMISSION = 0;
    public static final int RUN = 1;
    public static final int ANALYSIS = 2;
    public static final int EXPERIMENT = 3;
    public static final int SAMPLE = 4;
    public static final int STUDY = 5;
    public static ModelFactory instance;

    public static ModelFactory getFactoryInstance() {
        if (instance == null) {
            instance = new ModelFactory();
        }

        return instance;
    }

    public Model getModel(int modelID) throws Exception {
        Model model;
        StringBuilder sb = new StringBuilder();
        sb.append(SRAConstants.WRAPPER_CONTEXT_PATH).append(":");
        
        switch (modelID) {
            case SUBMISSION:
                sb.append(SRAConstants.SUBMISSION_CONTEXT_PATH);
                model = new SubmissionModel(JAXBContext.newInstance(sb.toString()));
                break;
            case RUN:
                sb.append(SRAConstants.RUN_CONTEXT_PATH);
                model = new RunModel(JAXBContext.newInstance(sb.toString()));
                break;
            case ANALYSIS:
                sb.append(SRAConstants.ANALYSIS_CONTEXT_PATH);
                model = new AnalysisModel(JAXBContext.newInstance(sb.toString()));
                break;
            case EXPERIMENT:
                sb.append(SRAConstants.EXPERIMENT_CONTEXT_PATH);
                model = new ExperimentModel(JAXBContext.newInstance(sb.toString()));
                break;
            case SAMPLE:
                sb.append(SRAConstants.SAMPLE_CONTEXT_PATH);
                model = new SampleModel(JAXBContext.newInstance(sb.toString()));
                break;
            case STUDY:
                sb.append(SRAConstants.STUDY_CONTEXT_PATH);
                model = new StudyModel(JAXBContext.newInstance(sb.toString()));
                break;
            default:
                throw new Exception("Invalid model ID");
        }

        return model;
    }
}
