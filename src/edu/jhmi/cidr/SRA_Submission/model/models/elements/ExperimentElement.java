/*Copyright (c) 2015 Johns Hopkins Center for Inherited Disease Research

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.jhmi.cidr.SRA_Submission.model.models.elements;

import edu.jhmi.cidr.SRA_Submission.model.XML.experiment.ExperimentType;
import edu.jhmi.cidr.SRA_Submission.model.XML.experiment.LibraryDescriptorType;
import edu.jhmi.cidr.SRA_Submission.model.XML.experiment.LibraryType;
import edu.jhmi.cidr.SRA_Submission.model.XML.experiment.ObjectFactory;
import edu.jhmi.cidr.SRA_Submission.model.XML.experiment.PipelineType;
import edu.jhmi.cidr.SRA_Submission.model.XML.experiment.PlatformType;
import edu.jhmi.cidr.SRA_Submission.model.XML.experiment.ProcessingType;
import edu.jhmi.cidr.SRA_Submission.model.XML.experiment.SampleDescriptorType;
import edu.jhmi.cidr.SRA_Submission.model.XML.experiment.SpotDescriptorType;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 *
 * @author asanch15
 */
public class ExperimentElement extends Element<ExperimentType> {

    private final ObjectFactory objFactory;
    private final LibraryType libType;
    private final LibraryDescriptorType libDesc;
    private final List<SpotDescriptorType.SPOTDECODESPEC.READSPEC> reads;
    private final List<PipelineType.PIPESECTION> pipelineSections;
    private BigInteger spotLength;

    public ExperimentElement() {
        super(new ExperimentType());
        objFactory = new ObjectFactory();
        libType = objFactory.createLibraryType();
        libDesc = objFactory.createLibraryDescriptorType();
        reads = new ArrayList<>();
        pipelineSections = new ArrayList<>();
        spotLength = new BigInteger("0");
    }

    public void setTitle(String title) {
        element.setTITLE(title);
    }

    public void setAlias(String alias) {
        element.setAlias(alias);
    }

    public void setCenterName(String center) {
        element.setCenterName(center);
    }

    public void setStudyAccession(String accession) {
        ExperimentType.STUDYREF studyRef = objFactory.createExperimentTypeSTUDYREF();

        studyRef.setAccession(accession);
        element.setSTUDYREF(studyRef);
    }

    public void setDesignDescription(String desc) {
        libType.setDESIGNDESCRIPTION(desc);
    }

    public void setSampleDescriptor(String refCenter, String refName) {
        SampleDescriptorType sampleType = objFactory.createSampleDescriptorType();

        sampleType.setRefcenter(refCenter);
        sampleType.setRefname(refName);
        libType.setSAMPLEDESCRIPTOR(sampleType);
    }

    public void setLibraryDescriptor(String name, String strategy, String source, String selection) {
        libDesc.setLIBRARYNAME(name);
        libDesc.setLIBRARYSTRATEGY(strategy);
        libDesc.setLIBRARYSOURCE(source);
        libDesc.setLIBRARYSELECTION(selection);
//        libDesc.setLIBRARYCONSTRUCTIONPROTOCOL(protocol);
    }

    public void setLibraryLayoutPaired(String length, double sDev) {
        LibraryDescriptorType.LIBRARYLAYOUT layout = objFactory.createLibraryDescriptorTypeLIBRARYLAYOUT();
        LibraryDescriptorType.LIBRARYLAYOUT.PAIRED paired = objFactory.createLibraryDescriptorTypeLIBRARYLAYOUTPAIRED();

        paired.setNOMINALLENGTH(new BigInteger(length));
        paired.setNOMINALSDEV(sDev);

        layout.setPAIRED(paired);
        layout.setSINGLE(null);
        libDesc.setLIBRARYLAYOUT(layout);
    }

    public void setLibaryLayoutSingle() {
        LibraryDescriptorType.LIBRARYLAYOUT layout = objFactory.createLibraryDescriptorTypeLIBRARYLAYOUT();
        LibraryDescriptorType.LIBRARYLAYOUT.SINGLE single = objFactory.createLibraryDescriptorTypeLIBRARYLAYOUTSINGLE();

        layout.setSINGLE(single);
        layout.setPAIRED(null);
        libDesc.setLIBRARYLAYOUT(layout);
    }

    public void setSpotLength(String length) {
        spotLength = new BigInteger(length);
    }

    public void addRead(String readClass, String type, String baseCoord) {
        SpotDescriptorType.SPOTDECODESPEC.READSPEC readSpec = objFactory.createSpotDescriptorTypeSPOTDECODESPECREADSPEC();

        readSpec.setREADCLASS(readClass);
        readSpec.setREADTYPE(type);
        readSpec.setBASECOORD(new BigInteger(baseCoord));

        reads.add(readSpec);
    }

    public void setInstrumentIllumina(String model) {
        PlatformType platform = objFactory.createPlatformType();
        PlatformType.ILLUMINA illumnia = objFactory.createPlatformTypeILLUMINA();
        
        illumnia.setINSTRUMENTMODEL(model);
        platform.setILLUMINA(illumnia);
        element.setPLATFORM(platform);
    }

    public void addPipelineSection(String program, String version) {
        PipelineType.PIPESECTION pipeSection = objFactory.createPipelineTypePIPESECTION();

        pipeSection.setPROGRAM(program);
        pipeSection.setVERSION(version);

        pipelineSections.add(pipeSection);
    }

    private SpotDescriptorType getSpotDescriptor() {
        SpotDescriptorType spot = objFactory.createSpotDescriptorType();
        SpotDescriptorType.SPOTDECODESPEC spotDecode = objFactory.createSpotDescriptorTypeSPOTDECODESPEC();

        spotDecode.setSPOTLENGTH(spotLength);

        for (int i = 0; i < reads.size(); i++) {
            SpotDescriptorType.SPOTDECODESPEC.READSPEC read = reads.get(i);

            read.setREADINDEX(new BigInteger(String.valueOf(i)));
            spotDecode.getREADSPEC().add(read);
        }

        spot.setSPOTDECODESPEC(spotDecode);
        return spot;
    }

    private ProcessingType getProcess() {
        ProcessingType process = objFactory.createProcessingType();
        PipelineType pipeline = objFactory.createPipelineType();

        for (int i = 0; i < pipelineSections.size(); i++) {
            PipelineType.PIPESECTION pipeSection = pipelineSections.get(i);

            pipeSection.setSTEPINDEX(String.valueOf(i));

            if (i > 0) {
                pipeSection.getPREVSTEPINDEX().add(String.valueOf(i - 1));
            } else {
                pipeSection.getPREVSTEPINDEX().add("nil");
            }

            pipeline.getPIPESECTION().add(pipeSection);
        }

        process.setPIPELINE(pipeline);
        return process;
    }

    @Override
    public ExperimentType getElement() {
        libType.setSPOTDESCRIPTOR(getSpotDescriptor());
        libType.setLIBRARYDESCRIPTOR(libDesc);
        element.setDESIGN(libType);
        element.setPROCESSING(getProcess());

        return element;
    }
}
