/*Copyright (c) 2015 Johns Hopkins Center for Inherited Disease Research

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package edu.jhmi.cidr.SRA_Submission.model.models.elements;

import edu.jhmi.cidr.SRA_Submission.model.XML.run.AttributeType;
import edu.jhmi.cidr.SRA_Submission.model.XML.run.ObjectFactory;
import edu.jhmi.cidr.SRA_Submission.model.XML.run.RunType;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author asanch15
 */
public class RunElement extends Element<RunType> {
    private final ObjectFactory objFactory;
    private RunType.EXPERIMENTREF experimentRef;
    private final List<RunType.DATABLOCK.FILES.FILE> files;

    public RunElement() {
        super(new RunType());
        this.objFactory = new ObjectFactory();
        this.experimentRef = objFactory.createRunTypeEXPERIMENTREF();
        this.files = new ArrayList<>();
    }
    
    public void setRunInfo(String alias, String centerName){
        element.setAlias(alias);
        element.setCenterName(centerName);
    }
    
    public void setExperimentReference(String experimentName, String refCenterName){ 
        experimentRef.setRefname(experimentName);
        experimentRef.setRefcenter(refCenterName);
    }
    
     public void setExperimentReference(String experimentName, String refCenterName, String studyAccession){ 
         setExperimentReference(experimentName, refCenterName);
         experimentRef.setAccession(studyAccession);
     }
     
     public void setReferenceGenome(String refGenome){
         AttributeType refGenomeAttribute = new AttributeType();
         RunType.RUNATTRIBUTES runattributes = new RunType.RUNATTRIBUTES();
         
         refGenomeAttribute.setTAG("assembly");
         refGenomeAttribute.setVALUE(refGenome);
         runattributes.getRUNATTRIBUTE().add(refGenomeAttribute);
         
         element.setRUNATTRIBUTES(runattributes);
     }
    
    public void addFile(String fileName, String fileType, String checksum, String checksumMethod){
        RunType.DATABLOCK.FILES.FILE file = objFactory.createRunTypeDATABLOCKFILESFILE();
        
        file.setFilename(fileName);
        file.setFiletype(fileType);
        file.setChecksum(checksum);
        file.setChecksumMethod(checksumMethod);
        
        files.add(file);
    }
    
    private RunType.DATABLOCK getDataBlock(){
        RunType.DATABLOCK dataBlock = objFactory.createRunTypeDATABLOCK();
        RunType.DATABLOCK.FILES fileSet = objFactory.createRunTypeDATABLOCKFILES();
        
        fileSet.getFILE().addAll(files);
        dataBlock.setFILES(fileSet);
        
        return dataBlock;
    }
    
    @Override
    public RunType getElement(){
        element.setEXPERIMENTREF(experimentRef);
        element.getDATABLOCK().add(getDataBlock());
        
        return element;
    }
}
