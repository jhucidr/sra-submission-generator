/*Copyright (c) 2015 Johns Hopkins Center for Inherited Disease Research

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.jhmi.cidr.SRA_Submission.model.models.elements;

import edu.jhmi.cidr.SRA_Submission.model.XML.submission.ObjectFactory;
import edu.jhmi.cidr.SRA_Submission.model.XML.submission.SubmissionType;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.xml.datatype.XMLGregorianCalendar;

/**
 *
 * @author asanch15
 */
public class SubmissionElement extends Element<SubmissionType> {

    private final ObjectFactory objFactory;
    private final List<SubmissionType.CONTACTS.CONTACT> contacts;
    private final List<SubmissionType.ACTIONS.ACTION> actions;

    public SubmissionElement() {
        super(new SubmissionType());
        this.objFactory = new ObjectFactory();
        this.contacts = new ArrayList<>();
        this.actions = new ArrayList<>();
    }

    public void setAlias(String alias) {
        element.setAlias(alias);
    }

    public void setCenterName(String centerName) {
        element.setCenterName(centerName);
    }

    public void setAccession(String accession) {
        element.setAccession(accession);
    }

    public void setTitle(String title) {
        element.setTITLE(title);
    }

    public void addContact(String name, String informOnStatusEmail, String informOnErrorEmail) {
        SubmissionType.CONTACTS.CONTACT contact = objFactory.createSubmissionTypeCONTACTSCONTACT();

        contact.setName(name);
        contact.setInformOnStatus(informOnStatusEmail);
        contact.setInformOnError(informOnErrorEmail);

        contacts.add(contact);
    }

    public void addSourceAction(String xmlFilename, SchemaType schema) {
        SubmissionType.ACTIONS.ACTION action = objFactory.createSubmissionTypeACTIONSACTION();
        SubmissionType.ACTIONS.ACTION.ADD add = objFactory.createSubmissionTypeACTIONSACTIONADD();

        add.setSource(xmlFilename);
        add.setSchema(schema.getName());

//        if(schema == SchemaType.EXPERIMENT){
//            SubmissionType.ACTIONS.ACTION.PROTECT protect = objFactory.createSubmissionTypeACTIONSACTIONPROTECT();
//            
//            action.setPROTECT(protect);
//        }
//        
        action.setADD(add);
        actions.add(action);
    }

    public void addProtectAction() {
        SubmissionType.ACTIONS.ACTION action = objFactory.createSubmissionTypeACTIONSACTION();
        SubmissionType.ACTIONS.ACTION.PROTECT protect = objFactory.createSubmissionTypeACTIONSACTIONPROTECT();
        
        action.setPROTECT(protect);
        actions.add(action);
    }

    public void addHoldAction() {
        SubmissionType.ACTIONS.ACTION action = objFactory.createSubmissionTypeACTIONSACTION();
        SubmissionType.ACTIONS.ACTION.HOLD hold = objFactory.createSubmissionTypeACTIONSACTIONHOLD();

        action.setHOLD(hold);
        actions.add(action);
    }

    public void addHoldAction(XMLGregorianCalendar holdUntilDate) {
        SubmissionType.ACTIONS.ACTION action = objFactory.createSubmissionTypeACTIONSACTION();
        SubmissionType.ACTIONS.ACTION.HOLD hold = objFactory.createSubmissionTypeACTIONSACTIONHOLD();

        hold.setHoldUntilDate(holdUntilDate);

        action.setHOLD(hold);
        actions.add(action);

    }

    public void addHoldAction(String target, XMLGregorianCalendar holdUntilDate) {
        SubmissionType.ACTIONS.ACTION action = objFactory.createSubmissionTypeACTIONSACTION();
        SubmissionType.ACTIONS.ACTION.HOLD hold = objFactory.createSubmissionTypeACTIONSACTIONHOLD();

        hold.setTarget(target);
        hold.setHoldUntilDate(holdUntilDate);

        action.setHOLD(hold);
        actions.add(action);
    }

    public void addModifyAction(String schema) {
        SubmissionType.ACTIONS.ACTION action = objFactory.createSubmissionTypeACTIONSACTION();
        SubmissionType.ACTIONS.ACTION.MODIFY modify = objFactory.createSubmissionTypeACTIONSACTIONMODIFY();

        modify.setSource(schema+".xml");
        modify.setSchema(schema);

        actions.add(action);
    }

    public void addReleaseAction() {
        SubmissionType.ACTIONS.ACTION action = objFactory.createSubmissionTypeACTIONSACTION();
        SubmissionType.ACTIONS.ACTION.RELEASE release = objFactory.createSubmissionTypeACTIONSACTIONRELEASE();

        action.setRELEASE(release);

        actions.add(action);
    }

    public void addReleaseAction(String target) {
        SubmissionType.ACTIONS.ACTION action = objFactory.createSubmissionTypeACTIONSACTION();
        SubmissionType.ACTIONS.ACTION.RELEASE release = objFactory.createSubmissionTypeACTIONSACTIONRELEASE();

        release.setTarget(target);
        action.setRELEASE(release);

        actions.add(action);
    }

    public void addSupressAction(String target) {
        SubmissionType.ACTIONS.ACTION action = objFactory.createSubmissionTypeACTIONSACTION();
        SubmissionType.ACTIONS.ACTION.SUPPRESS supress = objFactory.createSubmissionTypeACTIONSACTIONSUPPRESS();

        supress.setTarget(target);
        action.setSUPPRESS(supress);

        actions.add(action);
    }

    public void addValidate(String schema) {
        SubmissionType.ACTIONS.ACTION action = objFactory.createSubmissionTypeACTIONSACTION();
        SubmissionType.ACTIONS.ACTION.VALIDATE validate = objFactory.createSubmissionTypeACTIONSACTIONVALIDATE();

        validate.setSource(schema + ".xml");
        validate.setSchema(schema);
        action.setVALIDATE(validate);

        actions.add(action);
    }

    private SubmissionType.CONTACTS getContacts() {
        SubmissionType.CONTACTS contactSet = objFactory.createSubmissionTypeCONTACTS();

        contactSet.getCONTACT().addAll(contacts);

        return contactSet;
    }

    public SubmissionType.ACTIONS getActions() {
        SubmissionType.ACTIONS actionSet = objFactory.createSubmissionTypeACTIONS();

        actionSet.getACTION().addAll(actions);

        return actionSet;
    }

    @Override
    public SubmissionType getElement() {
        element.setCONTACTS(getContacts());
        element.setACTIONS(getActions());

        return element;
    }

    public enum SchemaType {

        ANALYSIS("analysis"), EXPERIMENT("experiment"),
        RUN("run"), SAMPLE("sample"), STUDY("study");

        private final static Map<String, SchemaType> enumMap = new HashMap<>();

        private final String name;

        static {
            for (SchemaType schema : values()) {
                enumMap.put(schema.getName(), schema);
            }
        }

        private SchemaType(String name) {
            this.name = name;
        }

        public String getName() {
            return this.name;
        }

        public static SchemaType fromName(String name) {
            return enumMap.get(name);
        }

        public static Set<String> getNames() {
            return Collections.unmodifiableSet(enumMap.keySet());
        }
    }
}
