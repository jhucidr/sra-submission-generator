/*Copyright (c) 2015 Johns Hopkins Center for Inherited Disease Research

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.jhmi.cidr.SRA_Submission.objects;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import org.apache.commons.validator.routines.EmailValidator;

/**
 *
 * @author asanch15
 */
public class Contact {

    private final String name;
    private final String informOnStatus;
    private final String informOnError;

    public Contact(String name, String informOnStatus, String informOnError) {
        this.name = name.trim();
        this.informOnStatus = informOnStatus.trim();

        if (informOnError.isEmpty()) {
            this.informOnError = this.informOnStatus;
        } else {
            this.informOnError = informOnError.trim();
        }
    }

    public String getName() {
        return name;
    }

    public String getInformOnStatus() {
        return informOnStatus;
    }

    public String getInformOnError() {
        return informOnError;
    }

    public List<String> validate() {
        List<String> errors = new ArrayList<>();
        EmailValidator emailValidator = EmailValidator.getInstance();

        if (name.isEmpty()) {
            errors.add("Invalid contact name");
        }

        //TODO add email regex
        if (informOnStatus.isEmpty() || !emailValidator.isValid(informOnStatus)) {
            errors.add("Invalid inform on status email");
        }

        if (informOnError.isEmpty() || !emailValidator.isValid(informOnError)) {
            errors.add("Invalid inform on error email");
        }

        return errors;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 29 * hash + Objects.hashCode(this.name);
        hash = 29 * hash + Objects.hashCode(this.informOnStatus);
        hash = 29 * hash + Objects.hashCode(this.informOnError);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Contact other = (Contact) obj;
        if (!Objects.equals(this.name, other.name)) {
            return false;
        }
        if (!Objects.equals(this.informOnStatus, other.informOnStatus)) {
            return false;
        }
        if (!Objects.equals(this.informOnError, other.informOnError)) {
            return false;
        }
        return true;
    }

}
