/*Copyright (c) 2015 Johns Hopkins Center for Inherited Disease Research

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.jhmi.cidr.SRA_Submission.objects;

import edu.jhmi.cidr.SRA_Submission.model.models.elements.SubmissionElement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 *
 * @author asanch15
 */
public class Action {

    public static final String DATE_FORMAT = "MM-dd-yyyy";

    private final ActionType type;
    private final StringProperty firstAttributeProperty = new SimpleStringProperty("");
    private final StringProperty secondAttributeProperty = new SimpleStringProperty("");
    private final boolean removable;

    public Action(ActionType type) {
        this.type = type;
        this.removable = true;
    }

    public Action(ActionType type, boolean removable) {
        this.type = type;
        this.removable = removable;
    }

    public ActionType getType() {
        return type;
    }

    public String getTypeName() {
        return type.getName();
    }

    public String getFirstAttribute() {
        return firstAttributeProperty.get();
    }

    public String getSecondAttribute() {
        return secondAttributeProperty.get();
    }

    public StringProperty getTypeProperty() {
        return type.nameProperty();
    }

    public StringProperty firstAttributeProperty() {
        return firstAttributeProperty;
    }

    public StringProperty secondAttributeProperty() {
        return secondAttributeProperty;
    }

    public boolean isRemovable() {
        return removable;
    }

    public List<String> validate() {
        List<String> errors = new ArrayList<>();

        if (getFirstAttribute().isEmpty() && !type.getFirstAttributeType().isOptional()) {
            errors.add("Invalid first attribute for action: " + type.getName());
        }

        if (getSecondAttribute().isEmpty() && !type.getSecondAttributeType().isOptional()) {
            errors.add("Invalid second attribute for action: " + type.getName());
        } else {
            if (type == ActionType.HOLD) {
                if (!isValidDate(getSecondAttribute())) {
                    errors.add("Invalid Hold Until Date must be formatted: " + DATE_FORMAT);
                }
            }
        }

        return errors;
    }

    private boolean isValidDate(String dateStr) {
        SimpleDateFormat formatter = new SimpleDateFormat(DATE_FORMAT);

        System.out.println("dateStr = " + dateStr);
        
        try {
            formatter.parse(dateStr);
        } catch (ParseException ex) {
            return false;
        }

        return true;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 79 * hash + Objects.hashCode(this.type);
        hash = 79 * hash + Objects.hashCode(this.firstAttributeProperty.get());
        hash = 79 * hash + Objects.hashCode(this.secondAttributeProperty.get());
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Action other = (Action) obj;
        if (this.type != other.type) {
            return false;
        }
        if (!Objects.equals(this.firstAttributeProperty.get(), other.firstAttributeProperty.get())) {
            return false;
        }
        if (!Objects.equals(this.secondAttributeProperty.get(), other.secondAttributeProperty.get())) {
            return false;
        }
        return true;
    }

    public enum ActionType {

        SOURCE("Source", ActionAttributeType.SCHEMA, ActionAttributeType.NONE),
        PROTECT("Protect", ActionAttributeType.NONE, ActionAttributeType.NONE),
        MODIFY("Modifiy", ActionAttributeType.SCHEMA, ActionAttributeType.NONE),
        RELEASE("Release", ActionAttributeType.TARGET_OPTIONAL, ActionAttributeType.NONE),
        SUPRESS("Supress", ActionAttributeType.TARGET, ActionAttributeType.NONE),
        VALIDATE("Validate", ActionAttributeType.SCHEMA, ActionAttributeType.NONE),
        HOLD("Hold", ActionAttributeType.HOLD_DATE_OPTIONAL, ActionAttributeType.TARGET_OPTIONAL);

        private final StringProperty nameProperty = new SimpleStringProperty();
        private final ActionAttributeType firstAttributeType;
        private final ActionAttributeType secondAttributeType;

        private ActionType(String name, ActionAttributeType firstAttributeType, ActionAttributeType secondAttributeType) {
            this.nameProperty.set(name);
            this.firstAttributeType = firstAttributeType;
            this.secondAttributeType = secondAttributeType;
        }

        public String getName() {
            return nameProperty.get();
        }

        public StringProperty nameProperty() {
            return nameProperty;
        }

        public ActionAttributeType getFirstAttributeType() {
            return firstAttributeType;
        }

        public ActionAttributeType getSecondAttributeType() {
            return secondAttributeType;
        }

        @Override
        public String toString() {
            return getName();
        }

    }

    public enum ActionAttributeType {

        SCHEMA("Schema", SubmissionElement.SchemaType.getNames()),
        SCHEMA_OPTIONAL("Schema(Optional)", SubmissionElement.SchemaType.getNames(), true),
        TARGET("Target", SubmissionElement.SchemaType.getNames()),
        TARGET_OPTIONAL("Target(Optional)", SubmissionElement.SchemaType.getNames(), true),
        HOLD_DATE("Hold Until Date - Ex: MM-DD-YYYY"),
        HOLD_DATE_OPTIONAL("Hold Until Date - Ex: MM-DD-YYYY (Optional)", new HashSet<String>(), true),
        NONE("", new HashSet<String>(), true);

        private final String promptText;
        private final Set<String> choices;
        private final boolean optional;

        private ActionAttributeType(String promptText) {
            this.promptText = promptText;
            this.choices = new HashSet<>();
            this.optional = false;
        }

        private ActionAttributeType(String promptText, Set<String> choices) {
            this.promptText = promptText;
            this.choices = choices;
            this.optional = false;
        }

        private ActionAttributeType(String promptText, Set<String> choices, boolean optional) {
            this.promptText = promptText;
            this.choices = choices;
            this.optional = optional;
        }

        public String getPromptText() {
            return promptText;
        }

        public Set<String> getChoices() {
            return Collections.unmodifiableSet(choices);
        }

        public boolean isOptional() {
            return optional;
        }
    }
}
