/*Copyright (c) 2015 Johns Hopkins Center for Inherited Disease Research

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.jhmi.cidr.SRA_Submission.objects;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 *
 * @author asanch15
 */
public class SRAFreeFormData {

    private final StringProperty title = new SimpleStringProperty("");
    private final StringProperty alias = new SimpleStringProperty("");
    private final StringProperty centerName = new SimpleStringProperty("");
    private final StringProperty referencGenome = new SimpleStringProperty("");
    private final StringProperty libraryProtocol = new SimpleStringProperty("");
//    private final StringProperty pipelineName = new SimpleStringProperty("");
//    private final StringProperty pipelineVersion = new SimpleStringProperty("");
    private final StringProperty designDescription = new SimpleStringProperty("");
    private final StringProperty libraryStrategy = new SimpleStringProperty("");
    private final StringProperty librarySource = new SimpleStringProperty("");
    private final StringProperty librarySelection = new SimpleStringProperty("");
    private final StringProperty libraryLayout = new SimpleStringProperty("");
    private final StringProperty outputFolderPath = new SimpleStringProperty("");
    private final StringProperty outputFileName = new SimpleStringProperty("");
    private final StringProperty checksumFilePath = new SimpleStringProperty("");
    private final StringProperty sampleIDColumn = new SimpleStringProperty("");
    private final StringProperty runGroupIDColumn = new SimpleStringProperty("");
    
    private final ObservableList<File> bamFolders = FXCollections.observableArrayList();
    private final ObservableList<File> qcReportFiles = FXCollections.observableArrayList();
    private final ObservableList<Contact> contacts = FXCollections.observableArrayList();
    private final ObservableList<Action> actions = FXCollections.observableArrayList();

    public StringProperty getTitle() {
        return title;
    }

    public StringProperty getChecksumFilePath() {
        return checksumFilePath;
    }
    
    /**
     * @return the alias
     */
    public StringProperty getAlias() {
        return alias;
    }

    /**
     * @return the centerName
     */
    public StringProperty getCenterName() {
        return centerName;
    }

    /**
     * @return the referencGenome
     */
    public StringProperty getReferencGenome() {
        return referencGenome;
    }

    /**
     * @return the designDescription
     */
    public StringProperty getLibraryProtocol() {
        return libraryProtocol;
    }

    public StringProperty getOutputFileName() {
        return outputFileName;
    }

//    /**
//     * @return the pipelineName
//     */
//    public StringProperty getPipelineName() {
//        return pipelineName;
//    }
//
//    /**
//     * @return the pipelineVersion
//     */
//    public StringProperty getPipelineVersion() {
//        return pipelineVersion;
//    }

    /**
     * @return the libraryProtocol
     */
    public StringProperty getDesignDescription() {
        return designDescription;
    }

    /**
     * @return the libraryStrategy
     */
    public StringProperty getLibraryStrategy() {
        return libraryStrategy;
    }

    /**
     * @return the librarySource
     */
    public StringProperty getLibrarySource() {
        return librarySource;
    }

    /**
     * @return the librarySelection
     */
    public StringProperty getLibrarySelection() {
        return librarySelection;
    }

    /**
     * @return the libraryLayout
     */
    public StringProperty getLibraryLayout() {
        return libraryLayout;
    }

    public StringProperty getRunGroupIDColumn() {
        return runGroupIDColumn;
    }

    public StringProperty getSampleIDColumn() {
        return sampleIDColumn;
    }
    
//    public StringProperty getQcReportPath() {
//        return qcReportPath;
//    }
//    
//    public StringProperty getBamFolderPath() {
//        return bamFolderPath;
//    }
    public ObservableList<File> getBamFolders() {
        return bamFolders;
    }

    public ObservableList<File> getQcReportFiles() {
        return qcReportFiles;
    }
    
    public StringProperty getOutputFolderPath() {
        return outputFolderPath;
    }

    public ObservableList<Contact> getContacts() {
        return contacts;
    }

    public ObservableList<Action> getActions() {
        return actions;
    }

    public List<String> validate(){
        List<String> errors = new ArrayList<>();
        
        if(title.get() == null || title.get().isEmpty()){
            errors.add("Invalid Project Title");
        }
        
        if(alias.get() == null || alias.get().isEmpty()){
            errors.add("Invalid Project Alias");
        }
        
        if(centerName.get() == null || centerName.get().isEmpty()){
            errors.add("Invalid Center Name");
        }
        
        if(referencGenome.get() == null || referencGenome.get().isEmpty()){
            errors.add("Invalid Reference Genome");
        }
        
        if(designDescription.get() == null || designDescription.get().isEmpty()){
            errors.add("Invalid Library Protocol");
        }
        
        if(libraryProtocol.get() == null || libraryProtocol.get().isEmpty()){
            errors.add("Invalid Design Description");
        }
        
        if(qcReportFiles.isEmpty()){
            errors.add("No QC Report(s) Selected");
        } else {
            for (File file : qcReportFiles) {
                if(file.isFile() == false || file.getName().endsWith(".csv") == false){
                    errors.add("Invalid QC Report: " + file.getAbsolutePath());
                }
            }
        }
        
        if(bamFolders.isEmpty()){
            errors.add("No BAM Folder(s) Selected");
        } else {
            for (File file : bamFolders) {
                if(file.isDirectory() == false){
                    errors.add("Invalid BAM Folder: " + file.getAbsolutePath());
                }
            }
        }
        
        if(sampleIDColumn.get() == null || sampleIDColumn.get().isEmpty()){
            errors.add("No Sample ID Column Selected");
        }
        
        if(runGroupIDColumn.get() == null || runGroupIDColumn.get().isEmpty()){
            errors.add("No Run Group ID Selected");
        }
        
//        if(qcReportPath.get() == null || qcReportPath.get().isEmpty()){
//            errors.add("Invalid QC Report File Path");
//        } else {
//            Path path = Paths.get(qcReportPath.get());
//            
//            if(!Files.exists(Paths.get(qcReportPath.get())) || 
//                    (com.google.common.io.Files.getFileExtension(path.getFileName().toString()).equals("cvs") )){
//                errors.add("Invalid QC Report File");
//            }
//        }
//        
//        if(bamFolderPath.get() == null || bamFolderPath.get().isEmpty()){
//            errors.add("Invalid BAM Folder Path");
//        } else {
//            Path path = Paths.get(bamFolderPath.get());
//            
//            if(!Files.exists(path)){
//                errors.add("Invalid BAM Folder");
//            }
//        }
        
        if(contacts.isEmpty()){
            errors.add("Need atleast one contact");
        }
        
        if(actions.isEmpty()){
            errors.add("Need atleast one action");
        }
        
        return errors;
    }
    
}
