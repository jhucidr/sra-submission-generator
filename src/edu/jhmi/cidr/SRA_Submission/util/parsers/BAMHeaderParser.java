/*Copyright (c) 2015 Johns Hopkins Center for Inherited Disease Research

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.jhmi.cidr.SRA_Submission.util.parsers;

import com.google.common.base.Joiner;
import com.google.common.base.Optional;
import com.google.common.collect.HashBasedTable;
import com.google.common.collect.Table;
import com.google.common.hash.HashCode;
import com.google.common.hash.HashFunction;
import com.google.common.hash.Hashing;
import com.google.common.io.Files;
import edu.jhmi.cidr.SRA_Submission.model.utils.StreamEater;
import edu.jhmi.cidr.SRA_Submission.util.parsers.BAMHeaderParser.BAMHeaderColumn;
import static edu.jhmi.cidr.lib.Constants.NEW_LINE;
import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author asanch15
 */
public class BAMHeaderParser {

    private final static String SAM_TOOLS_PATH = "SAM_TOOLS_WIN\\samtools.exe";
    private final static ChecksumAlgorithm CHECKSUM_ALGORITHM = ChecksumAlgorithm.MD5;
    private final static String COLON_DELIMITER = ":";
    private final static String TAB_DELIMITER = "\\t+";
    public final static String FILE_TYPE = "bam";
    private final static int NUM_RETRIES = 5;

    private final File bamFile;
    private final List<String> errors = new ArrayList<>();
    private final StringBuilder bamHeaderBuilder = new StringBuilder();
    private final Set<BAMHeaderColumn> missingColumns = new HashSet<>(Arrays.asList(BAMHeaderColumn.values()));

    public static void main(String[] args) throws Exception {
        BAMHeaderParser parser = new BAMHeaderParser(new File("BAM_File.bam"));

        System.out.println(parser.parse());
    }

    public BAMHeaderParser(File bamFile) {
        this.bamFile = bamFile;
    }

    public BAMHeaderData parse() {
        if (!Files.getFileExtension(bamFile.getName()).equalsIgnoreCase(FILE_TYPE)) {
            errors.add(bamFile.toString() + " is not a BAM file");
        }

        return parseBAMFile();
    }

    public static String getChecksumAlgorithmName() {
        return CHECKSUM_ALGORITHM.getName();
    }

    private String getSAMToolsCommand() {
        StringBuilder sb = new StringBuilder();

        sb.append(SAM_TOOLS_PATH).append(" view -H ").append(bamFile.getAbsolutePath());

        return sb.toString();
    }

    private BAMHeaderData parseBAMFile() {
        try {
            ApplicationMonitorStream appStream = new ApplicationMonitorStream();
            String samToolsCmd = getSAMToolsCommand();
            Process process;
            BAMHeaderData bamHeaderData;
            StreamEater outputStream;
            StreamEater errorStream;

        System.out.println("samToolsCmd = " + samToolsCmd);
            process = Runtime.getRuntime().exec(samToolsCmd);
            outputStream = new StreamEater(process.getInputStream(), appStream, StreamEater.Type.STDOUT, false, false);
            errorStream = new StreamEater(process.getErrorStream(), appStream, StreamEater.Type.STDERR, false, false);

            outputStream.start();
            errorStream.start();

            process.waitFor();
            //Just to make sure the process is actually done
            Thread.sleep(10);

            bamHeaderData = parseBAMHeaderString(bamHeaderBuilder.toString());

            bamHeaderData.setFile(bamFile);
            bamHeaderData.setFilename(bamFile.getName());

//        if (checksum.isPresent()) {
//            bamHeaderData.setChecksum(checksum.get());
//        } else {
//            bamHeaderData.setChecksum(getFileChecksumValue());
//        }
            return bamHeaderData;
        } catch (Exception ex) {
            errors.add(ex.getMessage());
            ex.printStackTrace();
        }

        return null;
    }

    private BAMHeaderData parseBAMHeaderString(String bamHeaderString) {
        BAMHeaderData bamHeaderData = new BAMHeaderData();
        Scanner scanner = new Scanner(bamHeaderString);

        while (scanner.hasNextLine()) {
            String line = scanner.nextLine();
            String[] rowItems = line.split(TAB_DELIMITER);

            if (rowItems.length > 1) {
                String key = rowItems[0];
                BAMHeaderRow row = BAMHeaderRow.getRowFromName(key);

                if (row != null) {
                    BAMData data = parseRow(row, rowItems);

                    if (data != null) {
                        bamHeaderData.addData(row, data);
                    }
                }
            }
        }

        if (missingColumns.isEmpty() == false) {
            errors.add(bamFile.getAbsolutePath() + " is missing header columns: " + Joiner.on(", ").join(missingColumns));
        }

        return bamHeaderData;
    }

    public List<String> getErrors() {
        return errors;
    }

    private BAMData parseRow(BAMHeaderRow rowKey, String[] rowItems) {
        BAMData data;

        switch (rowKey) {
            case RUN:
                BAMRunData runData = new BAMRunData();
                data = runData;
                break;
            case SOFTWARE:
                BAMSoftwareData softwareData = new BAMSoftwareData();
                data = softwareData;
                break;
            default:
                return null;
        }

        for (int i = 1; i < rowItems.length; i++) {
            String[] rowItemTokens = rowItems[i].split(COLON_DELIMITER);

            if (rowItemTokens.length > 1) {
                String columnKey = rowItemTokens[0];
                BAMHeaderColumn column = BAMHeaderColumn.getColumn(rowKey, columnKey);

                if (column != null) {
                    String value = rowItemTokens[1];

                    data.setValue(column, value);
                    missingColumns.remove(column);
                }

            }
        }

        return data;
    }

    public static String getFileChecksumValue(File file) throws Exception {
        HashCode hash = null;
        int retry = 0;

        while (retry < NUM_RETRIES) {
            try {
                hash = Files.hash(file, CHECKSUM_ALGORITHM.getHashFunction());
                break;
            } catch (IOException ex) {
                retry++;

                if (retry == NUM_RETRIES) {
                    throw ex;
                }
            }
        }

        if (hash == null) {
            throw new Exception("Error calculating checksum");
        }

        return hash.toString();
    }

    private class ApplicationMonitorStream extends OutputStream {

        @Override
        public void write(int b) throws IOException {
            synchronized (this) {
                bamHeaderBuilder.append(((char) b));
            }
        }
    }

    public static class BAMHeaderData {

        private File file;
        private String filename;
        private String checksum;
        private final List<BAMRunData> runs = new ArrayList<>();
        private final List<BAMSoftwareData> software = new ArrayList<>();

        public String getFilename() {
            return filename;
        }

        protected void setFilename(String filename) {
            this.filename = filename;
        }

        public String getChecksum() {
            return checksum;
        }

        public void setChecksum(String checksum) {
            this.checksum = checksum;
        }

        public List<BAMRunData> getRuns() {
            return runs;
        }

        public String getRunsString() {
            if (runs.isEmpty() == false) {
                return Joiner.on("\n").join(runs);
            }

            return "";
        }

        public List<BAMSoftwareData> getSoftware() {
            return software;
        }

        public String getSoftwareString() {
            if (software.isEmpty() == false) {
                return Joiner.on("\n").join(software);
            }

            return "";
        }

        protected void addRun(BAMRunData runData) {
            runs.add(runData);
        }

        protected void addSoftware(BAMSoftwareData softwareData) {
            software.add(softwareData);
        }

        public void setFile(File file) {
            this.file = file;
        }

        public File getFile() {
            return file;
        }

        void addData(BAMHeaderRow key, BAMData data) {
            switch (key) {
                case RUN:
                    addRun((BAMRunData) data);
                    break;
                case SOFTWARE:
                    addSoftware((BAMSoftwareData) data);
                    break;
            }
        }

        @Override
        public String toString() {
            StringBuilder sb = new StringBuilder();

            sb.append("File: ").append(filename).append(NEW_LINE)
                    .append("Checksum: ").append(checksum).append(NEW_LINE)
                    .append("Runs: ").append(NEW_LINE);

            for (BAMRunData run : runs) {
                sb.append(run);
            }

            sb.append("Software Info: ").append(NEW_LINE);
            for (BAMSoftwareData softwareData : software) {
                sb.append(softwareData);
            }

            return sb.toString();
        }

    }

    private interface BAMData {

        public void setValue(BAMHeaderColumn key, String value);
    }

    public class BAMRunData implements BAMData {

        private String id;
        private String platform;
        private String instrument;
//        private String runName;
        private String libraryName;
        private String experimentName;
        private String centerName;

        public String getId() {
            return id;
        }

        public String getPlatform() {
            return platform;
        }

        public String getInstrument() {
            return instrument;
        }

//        public String getRunName() {
//            return runName;
//        }
        public String getLibraryName() {
            return libraryName;
        }

        public String getExperimentName() {
            return experimentName;
        }

        public String getCenterName() {
            return centerName;
        }

        @Override
        public void setValue(BAMHeaderColumn key, String value) {
            switch (key) {
                case ID:
                    id = value;
                    break;
                case PLATFORM:
                    platform = value;
                    break;
                case INSTRUMENT_MODEL:
                    instrument = value;
                    break;
//                case RUN_NAME:
//                    runName = value;
//                    break;
                case LIBRARY_NAME:
                    libraryName = value;
                    break;
                case EXPERIMENT_NAME:
                    experimentName = value;
                    break;
                case CENTER_NAME:
                    centerName = value;
                    break;
            }
        }

        @Override
        public String toString() {
            StringBuilder sb = new StringBuilder();

            sb.append("ID: ").append(id).append(NEW_LINE)
                    .append("Platform: ").append(platform).append(NEW_LINE)
                    .append("Instrument: ").append(instrument).append(NEW_LINE)
                    //                    .append("Run Name: ").append(runName).append(NEW_LINE)
                    .append("Library Name: ").append(libraryName).append(NEW_LINE)
                    .append("Experiment Name: ").append(experimentName).append(NEW_LINE)
                    .append("Center Name: ").append(centerName).append(NEW_LINE);

            return sb.toString();
        }
    }

    public class BAMSoftwareData implements BAMData {

        private String name;
        private String version = "";

        public String getName() {
            return name;
        }

        public String getVersion() {
            return version;
        }

        @Override
        public void setValue(BAMHeaderColumn key, String value) {
            switch (key) {
                case SOFTWARE_NAME:
                    name = value;
                    break;
                case SOFTWARE_VERSION:
                    version = value;
                    break;
            }
        }

        @Override
        public String toString() {
            StringBuilder sb = new StringBuilder();

            sb.append(name).append(" Version:").append(version).append(NEW_LINE);

            return sb.toString();
        }
    }

    public enum BAMHeaderRow {

        RUN("@RG"),
        SOFTWARE("@PG");

        private static final Map<String, BAMHeaderRow> ENUM_MAP = new HashMap<>();

        static {
            for (BAMHeaderRow key : BAMHeaderRow.values()) {
                ENUM_MAP.put(key.getName(), key);
            }
        }

        private final String name;

        private BAMHeaderRow(String name) {
            this.name = name;
        }

        public String getName() {
            return name;
        }

        public static boolean containsRow(String rowName) {
            return ENUM_MAP.containsKey(rowName);
        }

        public static BAMHeaderRow getRowFromName(String rowName) {
            return ENUM_MAP.get(rowName);
        }
    }

    public enum BAMHeaderColumn {

        ID(BAMHeaderRow.RUN, "ID"),
        PLATFORM(BAMHeaderRow.RUN, "PL"),
        INSTRUMENT_MODEL(BAMHeaderRow.RUN, "DS"),
        //        RUN_NAME(BAMHeaderRow.RUN, "PU"),
        LIBRARY_NAME(BAMHeaderRow.RUN, "LB"),
        EXPERIMENT_NAME(BAMHeaderRow.RUN, "SM"),
        CENTER_NAME(BAMHeaderRow.RUN, "CN"),
        SOFTWARE_NAME(BAMHeaderRow.SOFTWARE, "ID"),
        SOFTWARE_VERSION(BAMHeaderRow.SOFTWARE, "VN");

        private static final Table<BAMHeaderRow, String, BAMHeaderColumn> ENUM_TABLE = HashBasedTable.create();

        static {
            for (BAMHeaderColumn column : BAMHeaderColumn.values()) {
                ENUM_TABLE.put(column.getRow(), column.getName(), column);
            }
        }

        private final BAMHeaderRow row;
        private final String name;

        private BAMHeaderColumn(BAMHeaderRow row, String name) {
            this.row = row;
            this.name = name;
        }

        public BAMHeaderRow getRow() {
            return row;
        }

        public String getName() {
            return name;
        }

        public static BAMHeaderColumn getColumn(BAMHeaderRow row, String name) {
            return ENUM_TABLE.get(row, name);
        }
    }

    public enum ChecksumAlgorithm {

        MD5("MD5", Hashing.md5());

        private final String name;
        private final HashFunction hashFunction;

        private ChecksumAlgorithm(String name, HashFunction hashFunction) {
            this.name = name;
            this.hashFunction = hashFunction;
        }

        public HashFunction getHashFunction() {
            return hashFunction;
        }

        public String getName() {
            return name;
        }
    }

}
