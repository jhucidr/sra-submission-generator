/*Copyright (c) 2015 Johns Hopkins Center for Inherited Disease Research

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.jhmi.cidr.SRA_Submission.util.parsers;

import com.google.common.base.Joiner;
import com.google.common.collect.Table;
import static edu.jhmi.cidr.lib.Constants.NEW_LINE;
import edu.jhmi.cidr.lib.Displayable;
import edu.jhmi.cidr.lib.Utils;
import edu.jhmi.cidr.lib.conveniencestore.fileutils.parseomatic.ParseOmatic;
import edu.jhmi.cidr.lib.conveniencestore.fileutils.parseomatic.interfaces.ParseOmaticColumn;
import edu.jhmi.cidr.lib.conveniencestore.fileutils.parseomatic.parseomaticstrategy.EnumColumnParser;
import java.io.File;
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Alice Sanchez
 */
public class QCReportParser {

    private final File qcReportFile;
    private final boolean useExperimentNameAsLocalID;
    private final List<String> errors = new ArrayList<>();

    public static void main(String[] args) {
        QCReportParser parser
                = new QCReportParser(
                        new File("C:\\Users\\asanch15\\Documents\\SRA_Submission\\E_Roden_PGxCCHMC_WGHum-SeqCustom_102212_8.QCreport.13November2013.csv"));

        for (QCReportRecord record : parser.parse()) {
            System.out.println(record);
        }

    }

    public QCReportParser(File qcReportFile) {
        this.qcReportFile = qcReportFile;
        useExperimentNameAsLocalID = false;
    }

    public QCReportParser(File qcReportFile, String localIDColumn, String runGroupColumn, String experimentNameColumn) {
        this.qcReportFile = qcReportFile;

        if (localIDColumn.isEmpty() == false) {
            Column.LOCAL_ID.setColumnName(localIDColumn);
        }

        if (experimentNameColumn.isEmpty() == false) {
            Column.EXPERIMENT_NAME.setColumnName(experimentNameColumn);
        }

        if (runGroupColumn.isEmpty() == false) {
            Column.RUN_NAME.setColumnName(runGroupColumn);
        }

        useExperimentNameAsLocalID = localIDColumn.equalsIgnoreCase(experimentNameColumn);
    }

    public List<QCReportRecord> parse() {
        List<Column> columns = new ArrayList<>(useExperimentNameAsLocalID ? EnumSet.range(Column.PROJECT, Column.INSERT_SDEV) : EnumSet.allOf(Column.class));
        List<QCReportRecord> records = new ArrayList<>();
        EnumColumnParser<Column> enumColumnParser;

        enumColumnParser = ParseOmatic.getEnumColumnParser(qcReportFile,
                1, ",",
                columns, columns);

        try {
            enumColumnParser.parse();
            Table<Integer, Column, String> data = enumColumnParser.getParsedContent();
            List<Column> missingColumns = enumColumnParser.getMissingRequiredColumns();

            if (missingColumns.isEmpty() == false) {
                errors.add("QC Report: " + qcReportFile.getName() + " has missing columns: " + Utils.join(", ", missingColumns));
            } else {
                for (Integer rowNum : data.rowKeySet()) {
                    QCReportRecord record = new QCReportRecord();
                    Map<Column, String> rowData = data.row(rowNum);
                    List<Column> missingColumnData = new ArrayList<>();

                    for (Column column : rowData.keySet()) {
                        String value = rowData.get(column);

                        if (value.isEmpty()) {
                            missingColumnData.add(column);
                        }

                        record.setValue(column, value);
                    }

                    if (useExperimentNameAsLocalID) {
                        record.setLocalIDToExperimentName();
                    }

                    if (missingColumnData.isEmpty()) {
                        records.add(record);
                    } else {
                        errors.add(qcReportFile.getName() + " Line: " + rowNum + " is missing data values for columns: " + Joiner.on(", ").join(missingColumnData));
                    }

                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            errors.add("Error parsing QC Report: " + qcReportFile);
        }

        return records;
    }

    public List<String> getErrors() {
        return errors;
    }

    public static class QCReportRecord {

        private String localID;
        private String project;
        private String experimentName;
        private String runName;
        private String libraryName;
        private String readOneLength;
        private String readTwoLength;
        private String insertSize;
        private String insertSDev;
        private String libraryPrepProtocol;
        private String libraryPrepDescription;

        private void setValue(Column column, String value) {
            switch (column) {
                case LOCAL_ID:
                    localID = value;
                    break;
                case PROJECT:
                    project = value;
                    break;
                case EXPERIMENT_NAME:
                    experimentName = value;
                    break;
                case RUN_NAME:
                    runName = value;
                    break;
                case LIBRARY_NAME:
                    libraryName = value;
                    break;
                case READ_1_LENGTH:
                    readOneLength = value;
                    break;
                case READ_2_LENGTH:
                    readTwoLength = value;
                    break;
                case INSERT_SIZE:
                    insertSize = value;
                    break;
                case INSERT_SDEV:
                    insertSDev = value;
                    break;
                default:
                    System.out.println("Unknown column: " + column);
            }
        }

        private void setLocalIDToExperimentName() {
            localID = experimentName;
        }

        public String getLocalID() {
            return localID;
        }

        public String getExperimentName() {
            return experimentName;
        }

        public String getProject() {
            return project;
        }

        public String getRunName() {
            return runName;
        }

        public String getLibraryName() {
            return libraryName;
        }

        public String getReadOneLength() {
            return readOneLength;
        }

        public String getReadTwoLength() {
            return readTwoLength;
        }

        public String getInsertSize() {
            return insertSize;
        }

        public String getInsertSDev() {
            return insertSDev;
        }

        public void setLibraryPrepProtocol(String libraryPrepProtocol) {
            this.libraryPrepProtocol = libraryPrepProtocol;
        }

        public String getLibraryPrepProtocol() {
            return libraryPrepProtocol;
        }

        public void setLibraryPrepDescription(String libraryPrepDescription) {
            this.libraryPrepDescription = libraryPrepDescription;
        }

        public String getLibraryPrepDescription() {
            return libraryPrepDescription;
        }

        @Override
        public String toString() {
            StringBuilder sb = new StringBuilder();

            sb.append("Local ID: ").append(localID).append(NEW_LINE)
                    .append("Experiment Name: ").append(experimentName).append(NEW_LINE)
                    .append("Run Name: ").append(runName).append(NEW_LINE)
                    .append("Library Name: ").append(libraryName).append(NEW_LINE)
                    .append("Read One Length: ").append(readOneLength).append(NEW_LINE)
                    .append("Read Two Length: ").append(readTwoLength).append(NEW_LINE)
                    .append("Mean Insert Size: ").append(insertSize).append(NEW_LINE)
                    .append("Insert Size SDev: ").append(insertSDev).append(NEW_LINE);

            return sb.toString();
        }
    }

    private enum Column implements ParseOmaticColumn, Displayable {

        LOCAL_ID("Local_Id"),
        PROJECT("Project"),
        EXPERIMENT_NAME("SM_tag"),
        RUN_NAME("RG_ID"),
        LIBRARY_NAME("Library_Name"),
        READ_1_LENGTH("MEAN_READ_LENGTH_R1"),
        READ_2_LENGTH("MEAN_READ_LENGTH_R2"),
        INSERT_SIZE("Mean_Insert_Size"),
        INSERT_SDEV("Standard_Deviation");

        private String columnName;

        private Column(String columnName) {
            this.columnName = columnName;
        }

        public void setColumnName(String columnName) {
            this.columnName = columnName;
        }

        @Override
        public String getDisplayName() {
            return columnName;
        }

        @Override
        public String getDisplayString() {
            return columnName;
        }

    }
}
