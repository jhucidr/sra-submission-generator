/*Copyright (c) 2015 Johns Hopkins Center for Inherited Disease Research

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package edu.jhmi.cidr.SRA_Submission.util.parsers;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Alice Sanchez
 */
public class MD5ChecksumFileParser {   
    private static final String BAM_FILE_EXT = ".bam";
    private final File checksumFile;
    private final Map<String, String> checksumMap = new HashMap<>();

    public static void main(String[] args) throws Exception {
        MD5ChecksumFileParser checksumFileParser = new MD5ChecksumFileParser(new File("C:\\Users\\asanch15\\Documents\\SRA_Submission\\checksums.md5"));
        
        checksumFileParser.parse();
        
        for (Map.Entry<String, String> entry : checksumFileParser.getChecksumMap().entrySet()) {
            String checksum = entry.getKey();
            String file = entry.getValue();
            
            System.out.println(file + ": " + checksum);
        }
    }
    
    public MD5ChecksumFileParser(File checksumFile) {
        this.checksumFile = checksumFile;
    }
    
    public String getChecksum(String fileName){
        return checksumMap.get(fileName);
    }

    public Map<String, String> getChecksumMap() {
        return checksumMap;
    }
    
    public void parse() throws Exception {
        try(BufferedReader br = new BufferedReader(new FileReader(checksumFile))){
            String line;
            
            while((line = br.readLine()) != null){
                parseLine(line);
            }
        }
    }
    
    private void parseLine(String line){
        String[] tokens = line.split("\\s+");

        if(tokens.length > 1){
            String checksum = tokens[0];
            String filePath = tokens[tokens.length - 1];
            String[] filePathTokens = filePath.split("/|\\\\");
            String fileName = filePathTokens[filePathTokens.length - 1];
            
            if(fileName.endsWith(BAM_FILE_EXT)){
                checksumMap.put(fileName, checksum);
            }
        }
    }
}
