/*Copyright (c) 2015 Johns Hopkins Center for Inherited Disease Research

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.jhmi.cidr.SRA_Submission.util.parsers;

import com.google.common.collect.Table;
import edu.jhmi.cidr.SRA_Submission.objects.LibraryProtocol;
import edu.jhmi.cidr.lib.conveniencestore.fileutils.parseomatic.interfaces.ParseOmaticColumn;
import edu.jhmi.cidr.lib.conveniencestore.fileutils.parseomatic.parseomaticstrategy.EnumColumnParser;
import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Alice Sanchez
 */
public class LibraryProtocolFileParser {

    private final File libProtocolFile;

    public LibraryProtocolFileParser(File libProtocolFile) {
        this.libProtocolFile = libProtocolFile;
    }

    public List<LibraryProtocol> parse() throws Exception {
        List<LibraryProtocol> protocols = new ArrayList<>();
        List<Column> columns = Arrays.asList(Column.values());
        EnumColumnParser<Column> parser = new EnumColumnParser<>(libProtocolFile, 1, ",", columns, columns);
        Table<Integer, Column, String> data;

        parser.parse();
        data = parser.getParsedContent();

        for (Integer row : data.rowKeySet()) {
            String name = null;
            String desc = null;

            for (Map.Entry<Column, String> entry : data.row(row).entrySet()) {
                Column column = entry.getKey();
                String value = entry.getValue();

                switch (column) {
                    case PROTOCOL:
                        name = value;
                        break;
                    case DESC:
                        desc = value;
                        break;
                }
            }

            if (name != null && desc != null) {
                protocols.add(new LibraryProtocol(name, desc));
            }
        }

        return protocols;
    }

    private enum Column implements ParseOmaticColumn {

        PROTOCOL("Library Prep Protocol"),
        DESC("Description for SRA submission");

        private final String name;

        private Column(String name) {
            this.name = name;
        }

        @Override
        public String getDisplayName() {
            return name;
        }

    }
}
