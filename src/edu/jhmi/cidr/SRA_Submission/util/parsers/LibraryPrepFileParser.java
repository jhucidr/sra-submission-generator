/*Copyright (c) 2015 Johns Hopkins Center for Inherited Disease Research

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.jhmi.cidr.SRA_Submission.util.parsers;

import com.google.common.collect.Table;
import edu.jhmi.cidr.SRA_Submission.objects.LibraryProtocol;
import edu.jhmi.cidr.lib.Displayable;
import edu.jhmi.cidr.lib.Utils;
import edu.jhmi.cidr.lib.conveniencestore.fileutils.parseomatic.ParseOmatic;
import edu.jhmi.cidr.lib.conveniencestore.fileutils.parseomatic.interfaces.ParseOmaticColumn;
import edu.jhmi.cidr.lib.conveniencestore.fileutils.parseomatic.parseomaticstrategy.EnumColumnParser;
import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Alice Sanchez
 */
public class LibraryPrepFileParser {

    private final File libraryPrepFile;
    private final Map<String, LibraryProtocol> libraryProtocolProjectMap = new HashMap<>();
    private final List<String> errors = new ArrayList<>();

    public LibraryPrepFileParser(File libraryPrepFile) {
        this.libraryPrepFile = libraryPrepFile;
    }

    public void parse() throws Exception {
        List<Column> columns = Arrays.asList(Column.values());
        EnumColumnParser<Column> enumColumnParser
                = ParseOmatic.getEnumColumnParser(libraryPrepFile,
                        1, ",",
                        columns, columns);
        Table<Integer, Column, String> parsedData;
        List<Column> missingColumns;

        enumColumnParser.parse();

        parsedData = enumColumnParser.getParsedContent();
        missingColumns = enumColumnParser.getMissingRequiredColumns();

        if (missingColumns.isEmpty() == false) {
            errors.add(libraryPrepFile.getName() + " is missing columns: " + Utils.join("; ", columns));
        } else {
            for (Integer row : parsedData.rowKeySet()) {
                Map<Column, String> data = parsedData.row(row);

                libraryProtocolProjectMap.put(data.get(Column.PROJECT), new LibraryProtocol(data.get(Column.LIB_PREP_PROTCOL), data.get(Column.DESCRIPTION)));
            }
        }
    }

    public List<String> getErrors() {
        return errors;
    }

    public Map<String, LibraryProtocol> getLibraryProtocolProjectMap() {
        return libraryProtocolProjectMap;
    }
    
    private enum Column implements ParseOmaticColumn, Displayable {

        PROJECT("Project"),
        LIB_PREP_PROTCOL("Library Prep Protocol"),
        DESCRIPTION("Description for SRA submission");

        private final String name;

        private Column(String name) {
            this.name = name;
        }

        @Override
        public String getDisplayName() {
            return name;
        }

        @Override
        public String getDisplayString() {
            return name;
        }

    }
}
