/*Copyright (c) 2015 Johns Hopkins Center for Inherited Disease Research

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.jhmi.cidr.SRA_Submission.util;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 *
 * @author asanch15
 */
public class SchemaDocumentationGenerator {

    private final String schemaUrlPath;
    private final String documentationFilePath;
    private static final String NEW_LINE = System.getProperty("line.separator");

    public SchemaDocumentationGenerator(String schemaUrlPath, String documentationFilePath) {
        if (documentationFilePath == null) {
            throw new NullPointerException("documentationFilePath must not be null");
        }
        if (schemaUrlPath == null) {
            throw new NullPointerException("schemaUrlPath must not be null");
        }
        this.schemaUrlPath = schemaUrlPath;
        this.documentationFilePath = documentationFilePath;
    }

    public void write() throws Exception {
        DocumentBuilderFactory fact = DocumentBuilderFactory.newInstance();
        DocumentBuilder db = fact.newDocumentBuilder();
        Document doc = db.parse(schemaUrlPath);
        String documentation = generateDocumentation(doc);
        writeDocumentation(documentation);
    }

    private String generateDocumentation(Document doc) {
        // parsing nodes within the schema node only allows us to ignore comments 
        // at the top of the schema file.
        return parseXmlNode(doc.getElementsByTagName("xs:schema").item(0), 0);
    }

    private String parseXmlNode(Node ele, int depth) {
        NodeList nodeList = ele.getChildNodes();

        StringBuilder docBuilder = new StringBuilder();
        for (int i = 0; i < nodeList.getLength(); i++) {
            Node childNode = nodeList.item(i);

            if (childNode.getNodeName().equals("xs:documentation")) {
                docBuilder.append(tabz(depth));
                docBuilder.append(newteringMethod(childNode.getTextContent()));
            } else {
                docBuilder.append(buildNodeAttributesDocumentation(childNode, depth));
            }
            
            docBuilder.append(parseXmlNode(childNode, depth + 1));
        }
        return docBuilder.toString();
    }

    private void writeDocumentation(String documentation) throws IOException {
        try (BufferedWriter out = new BufferedWriter(new FileWriter(documentationFilePath))) {
            out.write(documentation);
            out.flush();
        }
    }

    private String tabz(int depth) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < depth; i++) {
            sb.append("\t");
        }
        return sb.toString();
    }

    // removes extraneous new lines and white space
    private String newteringMethod(String text) {
        text = text.replaceAll("\r", "");
        String[] tokens = text.split("\n");
        StringBuilder newteredBuilder = new StringBuilder();
        for (String token : tokens) {
            newteredBuilder.append(token.trim());
        }
        newteredBuilder.append(NEW_LINE);
        return newteredBuilder.toString();
    }

    private String buildNodeAttributesDocumentation(Node childNode, int depth) {
        NamedNodeMap attributes = childNode.getAttributes();
        if (null != attributes) {
            for (int j = 0; j < attributes.getLength(); j++) {
                Node attribute = attributes.item(j);
                if (attribute.getNodeName().equals("name")
                        || ((childNode.getNodeName().equals("xs:enumeration") && attribute.getNodeName().equals("value")))) {
                    return buildAttributeDocumentation(attribute, depth);
                }
            }
        }
        // if none of the conditions are met, return nothing.
        return "";
    }

    private String buildAttributeDocumentation(Node attribute, int depth) {
        StringBuilder sb = new StringBuilder();
        sb.append(tabz(depth + 1)).append(attribute.getNodeValue()).append(NEW_LINE);
        return sb.toString();
    }
    
    public static void main(String[] args) throws Exception{
        SchemaDocumentationGenerator docGen = 
                new SchemaDocumentationGenerator("http://www.ncbi.nlm.nih.gov/viewvc/v1/trunk/sra/doc/SRA_1-5/SRA.analysis.xsd?view=co",
                "Analysis_Scheme_Documentation.txt");
        
        docGen.write();
    }
}