/*Copyright (c) 2015 Johns Hopkins Center for Inherited Disease Research

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.jhmi.cidr.SRA_Submission.util;

/**
 *
 * @author asanch15
 */
public class SRAConstants {

    /**
     * Context paths to JAXB created objects
     */
    public static final String SUBMISSION_CONTEXT_PATH = "edu.jhmi.cidr.SRA_Submission.model.XML.submission";
    public static final String RUN_CONTEXT_PATH = "edu.jhmi.cidr.SRA_Submission.model.XML.run";
    public static final String ANALYSIS_CONTEXT_PATH = "edu.jhmi.cidr.SRA_Submission.model.XML.analysis";
    public static final String EXPERIMENT_CONTEXT_PATH = "edu.jhmi.cidr.SRA_Submission.model.XML.experiment";
    public static final String SAMPLE_CONTEXT_PATH = "edu.jhmi.cidr.SRA_Submission.model.XML.sample";
    public static final String STUDY_CONTEXT_PATH = "edu.jhmi.cidr.SRA_Submission.model.XML.study";
    public static final String WRAPPER_CONTEXT_PATH = "edu.jhmi.cidr.SRA_Submission.model.XML.wrappers";
    /**
     * XML Stuff
     */
    public static final String SUBMISSION_SCHEMA_URL = "http://www.ncbi.nlm.nih.gov/viewvc/v1/trunk/sra/doc/SRA_1-5/SRA.submission.xsd?view=co";
    public static final String RUN_SCHEMA_URL = "http://www.ncbi.nlm.nih.gov/viewvc/v1/trunk/sra/doc/SRA_1-5/SRA.run.xsd?view=co";
    public static final String SAVE_DIR = ".";
    /**
     * SAM Tools Stuff
     */
    public static final String SAM_TOOLS_EXE_PATH = "O:\\asanch15\\SRA_Submission\\SAM_TOOLS_WIN\\samtools.exe";
}
