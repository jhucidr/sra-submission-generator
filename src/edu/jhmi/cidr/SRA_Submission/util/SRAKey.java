/*Copyright (c) 2015 Johns Hopkins Center for Inherited Disease Research

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.jhmi.cidr.SRA_Submission.util;

/**
 *
 * @author asanch15
 */
public enum SRAKey {

    CENTER_NAME("center_name", SRADataSource.PROP_FILE ,"CIDR"),
    LIBRARY_STRATEGY("library_strategy", SRADataSource.PROP_FILE),
    LIBRARY_SOURCE("library_source", SRADataSource.PROP_FILE),
    LIBRARY_SELECTION("library_selection", SRADataSource.PROP_FILE),
    LIBRARY_LAYOUT("library_layout", SRADataSource.PROP_FILE),
    LIBRARY_PROTOCOL("library_protocol", SRADataSource.SEQTRAQ),
    DESIGN_DESCRIPTION("design_description", SRADataSource.SEQTRAQ),
    PIPELINE_PROGRAM("pipeline_program", SRADataSource.PROP_FILE),
    PIPELINE_VERSION("pipeline_version", SRADataSource.PROP_FILE),
    FILE_TYPE("file_type", SRADataSource.PROP_FILE, "BAM"),
    FILE_CHECKSUM_METHOD("file_checksum_method", SRADataSource.PROP_FILE, "MD5"),
    READ_TYPE("read_type", SRADataSource.PROP_FILE),
    REF_GENOME("ref_genome", SRADataSource.PROP_FILE);

    private final String key;
    private final String defaultValue;
    private final SRADataSource source;

    private SRAKey(String key, SRADataSource source) {
        this.key = key;
        this.defaultValue = "";
        this.source = source;
    }

    private SRAKey(String key, SRADataSource source, String defaultValue) {
        this.key = key;
        this.defaultValue = defaultValue;
        this.source = source;
    }

    public String getKey() {
        return key;
    }

    public String getDefaultValue() {
        return defaultValue;
    }

    public SRADataSource getSource() {
        return source;
    }

    @Override
    public String toString() {
        return key;
    }

    public enum SRADataSource {

        PROP_FILE(),
        SEQTRAQ();
    }
}
